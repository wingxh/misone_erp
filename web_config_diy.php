<?php
return	array(
'MAIL_SMTP'=>'smtp.exmail.sina.com', /* SMTP服务器 */
'WEB_TITLE'=>'DD3000-进销存财', /* 公司名称 */
'PO2WH_CTRL_FLG'=>'Y', /* 采购单直接收货 */
'WEB_URL'=>'http://erp.dd3000.xin', /* 系统URL */
'MAIL_USER'=>'support@misone.org', /* 发件人 */
'SO2WH_CTRL_FLG'=>'Y', /* 销售单直接出货 */
'WEB_LOGO'=>'/Public/Images/erplogo.png', /* 公司LOGO */
'MAIL_PASSWORD'=>'support1***', /* 发件人认证码 */
'TODAY_REFRESH_TIME'=>'30', /* 看板自动刷新时间(分) */
'TODAY_FA_ACC'=>'1131,2121,2131,1151', /* 看板中要显示的科目(可用逗号分隔) */
'WHSWITCH_USE_NEWDATE'=>'Y', /* 调拨入库的商品使用新日期 */
'UPLOAD_PATH'=>'/Public/Upload/', /* 文件上传路径 */
'UPLOAD_ALLOW_EXTS'=>'jpg,gif,png,jpeg,bmp,rar,zip,7z,xls,doc,pdf,swf', /* 允许上传的类型 */
'UPLOAD_ALLOW_SIZE'=>'1024000', /* 允许的文件大小 */
'UPLOAD_RULE'=>'uniqid', /* 文件重命名方式 */
'LOG_RECORD'=>false, /* 后台日志记录 */
'AUTO_UPDATE_PRICE'=>'N', /* 自动更新存货价格 */
'SHOW_LINE_AMOUNT'=>'Y', /* 显示行小计金额 */
);
?>
