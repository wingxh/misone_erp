<?php

//设定项目配置
return	array(

'URL_ROUTER_ON' => true, //URL路由
'URL_MODEL'=>0, // 如果环境不支持PATHINFO 设置为0

/* SESSION 设定 */
'SESSION_EXPIRE'	=>	'300000',

/* 调试配置 */
'SHOW_PAGE_TRACE' => false, //是否要显示页面Trace信息


/* 编码设置 */
'TEMPLATE_CHARSET'      =>  'utf-8',        // 模板文件编码
'OUTPUT_CHARSET'        =>  'utf-8',        // 默认输出编码
'DB_CHARSET'            =>  'utf8',        // 数据库编码默认采用utf8

'TMPL_PARSE_STRING'  =>array(
    '__ADMIN__' => '/admin.php',
    '__VIP__'   => '/vip.php',
	'__WEB__'   => '/index.php',
),
'REGISTER_PASS'	=>	'0929!@',
'REGISTER_VER'	=>	'扬帆版',
'APP_PATH_DIY' => @''.dirname(__file__).'',

);


?>