<?php
header('Content-Type: text/html; charset=utf-8');

@set_time_limit(0); //设定执行超时时间
define('INSTALL_PATH', preg_replace("/[\/\\\\]{1,}/", '/', dirname(__FILE__)));
define('IN_INSTALL', TRUE); //发放登入牌


//提示已经安装
if(is_file(INSTALL_PATH.'/install_lock.txt') and @$_GET['s'] != md5('done'))
{
	require_once(INSTALL_PATH.'/templets/step_5.html');
	exit();
}

$s = !empty($_GET['s']) ? intval($_GET['s']) : 0;


switch($s)
{
	case 0: //协议说明
	require_once(INSTALL_PATH.'/templets/step_0.html'); 
	break;
	
	case 1: //环境检测
	$iswrite_array = array('/data/','/data/backup/','/data/update/','/include/conn.inc.php','/include/config.cache.php','/uploads/');
	$exists_array = array('is_writable','function_exists','mysql_connect');
	require_once(INSTALL_PATH.'/templets/step_1.html');
	break;

	case 2: //配置文件
	require_once(INSTALL_PATH.'/templets/step_2.html');
	break;
	
	case 3: //正在安装
	require_once(INSTALL_PATH.'/templets/step_3.html');
	break;

	case 15271: //检测数据库信息
	if(@mysql_connect($_GET['dbhost'], $_GET['dbuser'], $_GET['dbpw'])) echo 'true';
	else echo 'false';
	break;

	case md5('done'): //安装完成
	require_once(INSTALL_PATH.'/templets/step_4.html');
	Writef(INSTALL_PATH.'/install_lock.txt','程序已正确安装，重新安装请删除本文件');
	break;

	default: //协议说明
	require_once(INSTALL_PATH.'/templets/step_0.html');
	break;
}

//读取文件内容
function Readf($file)
{
	if(file_exists($file) and is_readable($file))
	{
		if(function_exists('file_get_contents'))
		{
			$string = file_get_contents($file);
		}
		else
		{
			$fp = fopen($file, 'r');
			while(!feof($fp))
			{
				$string = fgets($fp, 1024);
			}
			fclose($fp);
		}
		return $string;
	}
}

//写入文件内容
function Writef($file,$string,$mode='w')
{
	if(function_exists('file_put_contents'))
	{
		file_put_contents($file, $string);
        //echo 'write001';
	}
	else
	{
		$fp = fopen($file,$mode);
		fwrite($fp, $string);
		fclose($fp);
        //echo 'write002';
	}
	return true;
}

//测试可写性
function ck_iswrite($file)
{
	if(is_writable($file))
	{
		echo '<span class="install_true">可写</span>';
	}
	else
	{
		echo '<span class="install_false">不可写</span>';
		$GLOBALS['isnext'] = 'N';
	}
}

//测试函数是否存在
function funexists($func)
{
	if(function_exists($func))
	{
		echo '<span class="install_true">支持</span>';
	}
	else
	{
		echo '<span class="install_false">不支持</span>';
		$GLOBALS['isnext'] = 'N';
	}
}

//测试函数是否存在，返回建议
function funadvice($func)
{
	if(function_exists($func))
	{
		echo '<span style="color:#999;">无</span>';
	}
	else
	{
		echo '<span style="color:red">建议安装</span>';
		$GLOBALS['isnext'] = 'N';
	}
}
?>