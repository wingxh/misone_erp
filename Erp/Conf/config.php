<?php
if (!defined('THINK_PATH')) exit();
//载入网站配置
$web_config	=	require 'web_config.php';

//载入网站配置
$web_config_diy	=	require 'web_config_diy.php';

//载入数据库配置
$db_config = require 'db_config.php';

//RBAC
$array	=	array(
	'USER_AUTH_ON'			=>true,
	'USER_AUTH_TYPE'		=>1,		// 默认认证类型 1 登录认证 2 实时认证
	'USER_AUTH_KEY'			=>'authId',	// 用户认证SESSION标记
	'USER_AUTH_TIT'         =>'authTitle', //当前登陆的用户名
    'ADMIN_AUTH_KEY'		=>'administrator',  //加入管理员标识
	'USER_AUTH_MODEL'		=>'user',	// 默认验证数据表模型  会去找数据库中的新建的think_user(用户表)
	'AUTH_PWD_ENCODER'		=>'md5',	// 用户认证密码加密方式
	'USER_AUTH_GATEWAY'		=>'/Public/login',	// 默认认证网关   登录成功和失败 都经过这个文件
	'RBAC_ERROR_PAGE'       =>'/Public/access_error',
	'SYS_REGISTER'          =>'/Public/register',
	'SYS_BE_MODIFY'         =>'/Public/be_modify',
	'NOT_AUTH_MODULE'		=>'Public',		// 默认无需认证模块
	'REQUIRE_AUTH_MODULE'	=>'',		// 默认需要认证模块
	'NOT_AUTH_ACTION'		=>'',		// 默认无需认证操作
	'REQUIRE_AUTH_ACTION'	=>'',		// 默认需要认证操作
    'GUEST_AUTH_ON'         => false,    // 是否开启游客授权访问
    'GUEST_AUTH_ID'         =>0,     // 游客的用户ID   think_access表的id

	'RBAC_ROLE_TABLE'		=>'misone_role',
	'RBAC_USER_TABLE'		=>'misone_role_user',
	'RBAC_ACCESS_TABLE' 	=>'misone_access',
	'RBAC_NODE_TABLE'		=>'misone_node',
	
	'OFFICIAL_NAME' => 'misone',
	'OFFICIAL_URL'  => 'http://erp.dd3000.xin',
	'OFFICIAL_HELP' => 'http://erp.dd3000.xin/index.php/Help',
);

//return $array;

//合并输出配置
return array_merge($db_config,$web_config,$web_config_diy,$array);
?>