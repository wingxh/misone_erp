<?php

class ItsAction extends Action
{
    public function index()
	{
		header("Content-Type:text/html; charset=utf-8");
		echo "Hello,".time();
    }
	
	public function upd_stk_price_pur()
	{
		header("Content-Type:text/html; charset=utf-8");
		if(I('check')==md5('misone-erp'))
		{
			$tmp_stk_id = cookie('tmp_stk_price_pur_stk_id');
			$model = M('Stkmas');
			$_list = $model->field('id,price_pur')->where(array('price_pur'=>0,'id'=>array('gt',(int)$tmp_stk_id)))->order('id asc')->limit(100)->select();
			$_fail = 0;
			$_mas_id = 0;
			if($_list)
			{
				foreach($_list as $row)
				{
					$unit_price = (double)(M('Stkdetail')->where(array('refcode'='PO','stk_id'=>$row['id'],'dr_amount'=>array('gt',0),'dr_qty'=>array('gt',0)))->order('id desc')->getField('dr_amount/dr_qty');
					if($unit_price)
					{
						$model->where('id'=>$row['id'])->data(array('price_pur'=>$unit_price))->save();
					}
					else
					{
						$_fail++;
					}
					$_mas_id = $row['id'];
				}
				cookie('tmp_stk_price_pur_stk_id',$_mas_id);
			}
			echo date('Y-m-d H:i:s').' 本次共读取资料：'.count($_list).'，失败：'.$_fail;
		}
		else
		{
			echo md5('misone-erp');
		}
    }

}
?>