<?php
class  PublicAction  extends  Action
{

	function index()
	{
		$this->login();     //访问默认方法 跳转到  login()方法
	}
	
	function  login()
	{ 
		check_system();
		$this->display();    //指定模板
	}

	public function checkLogin()
	{ 
		if(empty($_POST['username'])) {  
			$this->error('帐号错误！');   
		}elseif (empty($_POST['password'])){  
			$this->error('密码必须！');
		}
		
		
        $map    =   array();	
		$map['username']	= $_POST['username'];
		$map["status_flg"]	=	'Y';
       /* $map["status"]	=	array('gt',0);
		if($_SESSION['verify'] != md5($_POST['verify'])) {
			$this->error('验证码错误！');
		}  状态码   是否激活  可以暂时关闭*/
		
		import ( 'ORG.Util.RBAC' );            //载入rbac类
        $authInfo = RBAC::authenticate($map);  //使用rbac类的authenticate方法 直接找 数组$map 里的全部信息  然后全部放到  $authInfo数组里面
        //使用用户名、密码和状态的方式进行认证
        if(false == $authInfo) {   //如果不存在  就说明不存在 
            $this->error('帐号不存在或已禁用！');
        }else {
            if($authInfo['password'] != md5($_POST['password'])) { 
            	$this->error('密码错误！');
            }
            $_SESSION[C('USER_AUTH_KEY')]	=	$authInfo['id'];
			$_SESSION[C('USER_AUTH_TIT')]   =   $authInfo['username'];
     
            if($authInfo['role']=='admin') {  
            	$_SESSION['administrator']		=	true;   
            }
            RBAC::saveAccessList();
			
			$User = M("User"); // 实例化User对象
			// 要修改的数据对象属性赋值
			$data['id'] = $authInfo['id'];
			$data['logintime'] = time();
			$data['loginip'] = get_client_ip();
			$User->save($data); // 根据条件保存修改的数据
			
			//var_dump($authInfo);
			$this->assign("jumpUrl",U('Index/index'));
			$this->success('登录成功！');

		}
	}
	
	public function logout()
    {
        if(isset($_SESSION[C('USER_AUTH_KEY')])) {    //注销session 信息
			unset($_SESSION[C('USER_AUTH_KEY')]);
			unset($_SESSION);
			session_destroy();
            $this->assign("jumpUrl",U('Public/login'));
            $this->success('登出成功！');
        }else {
            $this->error('已经登出！');
        }
     }
	 
	public function register()
    {
		vendor('MisoneAction.misone#action');
		$mac = new GetMacAddr(PHP_OS);
		$reginfo = $mac->mac_addr;
		$this->assign("request_code",$reginfo);
		$this->display();
	}
	
	public function register_save()
    {
		$req = $_POST['request_code'];
		$reg = $_POST['register_code'];
		if( $reg == md5($req.md5(C('REGISTER_PASS')).md5(C('REGISTER_VER')))  )
		{
			$data['html']=$reg;
			$upd = M('Test')->data($data)->where('code=\''.md5('check_system').'\'')->save();
			if($upd)
			{
				$this->assign("jumpUrl",U('Index/index'));
				$this->success('注册成功');
			}
			else
			{
				//$this->assign("jumpUrl",U('Public/register'));
				$this->error('保存失败');
			}
		}
		else
		{
			//$this->assign("jumpUrl",U('Public/register'));
			$this->error('注册码无效');
		}
		
	}
	
	

}

?>