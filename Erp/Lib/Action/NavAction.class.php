<?php

class NavAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 导航分类数据处理 */
	
	public function class_list()
	{
		$datalist = M("Navclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function class_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		
		$this->get_class_list();
		$this->assign("classid",$cid);
		$orderid=M("Navclass")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function class_add_save()
	{
		$model = D("Navclass");
		$this->assign("jumpUrl",U('Nav/class_list'));
		if ($model->create())
		{			
			$classid=$model->add();
			
			$data = array('parentstr'=>$this->get_parentstr($classid,$_POST['parentid']));
			$model->where("id=$classid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Navclass");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$this->get_class_list();
		
		$this->display();
    }
	public function class_edit_save()
	{
		$model = D("Navclass");
		$this->assign("jumpUrl",U('Nav/class_list'));
		if ($model->create())
		{			
			//var_dump($model->status_flg);exit;
			$model->save(); // 保存数据
			if($_POST['status_flg']=="N")
			{
				//当禁用类别时，自动禁用下级所有类别
				$data = array('status_flg'=>'N');
				$model->where("parentid=".$_POST['id']." or parentstr like '%,".$_POST['id'].",%'")->setField($data);
			}
			$this->set_parentstr($_POST['id'],$_POST['parentid']);
			$this->success("保存成功!");
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_del()
	{
		$model = M("Navclass");
		$this->assign("jumpUrl",U('Nav/class_list'));
		
		$data1 = array('parentid'=>0,'parentstr'=>'0,'); //将下级类别的父类设置为0
		$data2 = array('classid'=>0,'parentid'=>0,'parentstr'=>'0,'); //将对应的文章类别设置为0
		foreach (explode(',',$_GET["id"]) as $s)
		{
			if($s)
			{
				$model->where("parentid=$s or parentstr like '%,$s,%'")->setField($data);
				M("Navlist")->where("classid=$s  or parentstr like '%,$s,%'")->setField($data);
			}
		}		
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功: 此类别的所有下级类别及归属的内容都已转为顶级（未归类）!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	
	/* 导航数据处理 */
	
	public function data_list()
	{
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		//$datalist = D("Navlist")->relation(true)->where("classid=$cid or parentstr like '%,$cid,%'") -> order("classid asc,orderid asc")->select();
		
		$datalist = D("Navlist")->relation(true)
					-> field("id,classid,parentid,parentstr,title,note,picurl,linkurl,orderid,posttime,status_flg,concat(parentstr,'',id) as bpath") 
					-> where("classid=$cid or parentstr like '%,$cid,%'")
					-> order("bpath asc")
					-> select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function data_add()
	{		
		$cid=$_GET['cid'];
		$pid=$_GET['pid'];
		if(!$cid) $cid=0;
		if(!$pid) $pid=0;
		$this->get_class_list();
		$this->get_list_list();
		$this->assign("classid",$cid);
		$this->assign("parentid",$pid);
		$orderid=M("Navlist")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function data_add_save()
	{
		$model = M("Navlist");
		$this->assign("jumpUrl",U('Nav/data_list'));
		if ($model->create())
		{			
			$dataid=$model->add();
			
			$data = array('parentstr'=>$this->get_list_parentstr($dataid,$_POST['parentid']));
			$model->where("id=$dataid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Navlist");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->get_class_list();
		$this->get_list_list();
		$this->display();
    }
	public function data_edit_save()
	{
		$model = M("Navlist");
		$this->assign("jumpUrl",U('Nav/data_list'));
		if ($model->create())
		{
			$model->save(); // 保存数据
			
			if($_POST['status_flg']=="N")
			{
				//当禁用类别时，自动禁用下级所有类别
				$data = array('status_flg'=>'N');
				$model->where("parentid=".$_POST['id']." or parentstr like '%,".$_POST['id'].",%'")->setField($data);
			}
			$this->set_list_parentstr($_POST['id'],$_POST['parentid']);
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del()
	{
		$model = M("Navlist");
		$this->assign("jumpUrl",U('Nav/data_list'));		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function get_parentstr($cid,$pid=0)
	{
		if($pid==0)
		{
			return ',0,';
		}
		else
		{
			return M("Navclass")->where("id=$pid")->getField("parentstr").$pid.",";
		}
	}
	
	public function set_parentstr($cid,$pid=0,$pstr='')
	{
		
		//获取当前parentstr
		if($pstr == '')
		{
			$parstr = $this->get_parentstr($cid,$pid);
		}
		else
		{
			$parstr = $pstr.$pid.',';
		}
		
		//更新类别表parentstr
		M("Navclass")->where("id=$cid")->setField("parentstr",$parstr);
			
		//更新信息表（当前级别）
		//$data = array('parentid'=>$pid,'parentstr'=>$parstr);
		//M("Navlist")->where("classid=$cid")->setField($data);

		//获取当前ID下所有子ID
		$arrNav = M("Navclass")->where("parentid=$cid")->select();
		foreach($arrNav as $row)
		{
			//传递下级参数,继续更新
			$this->set_parentstr($row['id'], $row['parentid'], $parstr);
		}

	}
	
	public function get_class_list()
	{
		$datalist = M("Navclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") ->where("status_flg='Y'") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
	}
	
	/* Navlist属导航，LIST也需要支持多级 */
	public function get_list_parentstr($cid,$pid=0)
	{
		if($pid==0)
		{
			return ',0,';
		}
		else
		{
			return M("Navlist")->where("id=$pid")->getField("parentstr").$pid.",";
		}
	}
	
	public function set_list_parentstr($id,$pid=0,$pstr='')
	{
		
		//获取当前parentstr
		if($pstr == '')
		{
			$parstr = $this->get_list_parentstr($id,$pid);
		}
		else
		{
			$parstr = $pstr.$pid.',';
		}
			
		//更新信息表（当前级别）
		$data = array('parentid'=>$pid,'parentstr'=>$parstr);
		M("Navlist")->where("id=$id")->setField($data);


		//获取当前ID下所有子ID
		$arrNav = M("Navlist")->where("parentid=$id")->select();
		foreach($arrNav as $row)
		{
			//传递下级参数,继续更新
			$this->set_list_parentstr($row['id'], $row['parentid'], $parstr);
		}

	}
	
	public function get_list_list($listname='listlist')
	{
		$datalist = M("Navlist")->field("id,parentid,parentstr,title,orderid,status_flg,concat(parentstr,'',id) as bpath") ->where("status_flg='Y'") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign($listname,$datalist);
	}


}
?>