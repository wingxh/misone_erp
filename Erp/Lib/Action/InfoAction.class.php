<?php

class InfoAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 分类数据处理 */
	
	public function class_list()
	{
		$datalist = M("Infoclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function class_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		
		$this->get_class_list();
		$this->assign("classid",$cid);
		$orderid=M("Infoclass")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function class_add_save()
	{
		$model = D("Infoclass");
		$this->assign("jumpUrl",U('Info/class_list'));
		if ($model->create())
		{			
			$classid=$model->add();
			
			$data = array('parentstr'=>$this->get_parentstr($classid,$_POST['parentid']));
			$model->where("id=$classid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Infoclass");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$this->get_class_list();
		
		$this->display();
    }
	public function class_edit_save()
	{
		$model = D("Infoclass");
		$this->assign("jumpUrl",U('Info/class_list'));
		if ($model->create())
		{			
			//var_dump($model->status_flg);exit;
			$model->save(); // 保存数据
			if($_POST['status_flg']=="N")
			{
				//当禁用类别时，自动禁用下级所有类别
				$data = array('status_flg'=>'N');
				$model->where("parentid=".$_POST['id']." or parentstr like '%,".$_POST['id'].",%'")->setField($data);
			}
			$this->set_parentstr($_POST['id'],$_POST['parentid']);
			$this->success("保存成功!");
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_del()
	{
		$model = M("Infoclass");
		$this->assign("jumpUrl",U('Info/class_list'));
		
		$data1 = array('parentid'=>0,'parentstr'=>'0,'); //将下级类别的父类设置为0
		$data2 = array('classid'=>0,'parentid'=>0,'parentstr'=>'0,'); //将对应的文章类别设置为0
		foreach (explode(',',$_GET["id"]) as $s)
		{
			if($s)
			{
				$model->where("parentid=$s or parentstr like '%,$s,%'")->setField($data1);
				M("Infolist")->where("classid=$s  or parentstr like '%,$s,%'")->setField($data2);
			}
		}		
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功: 此类别的所有下级类别及归属的内容都已转为顶级（未归类）!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	
	/* 数据处理 */
	
	public function data_list()
	{
		$cid = $_REQUEST['cid'];
		if(!$cid) $cid=0;
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("Infolist");
		$strwhere="(classid=$cid or parentstr like '%,$cid,%') and status_flg in ('Y','N') and (title like '%$keywords%' or keywords like '%$keywords%' or description like '%$keywords%' or content like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&cid=".urlencode($cid);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->relation(true)->where($strwhere) -> order("classid asc,orderid asc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->get_class_list();
		$this->get_class_list('classlist2');
		$this->assign('cid',$cid);
		$this->assign('keywords',$keywords);
		
		$this->display();
    }
	public function data_list_rec()
	{
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("Infolist");
		$strwhere="(classid=$cid or parentstr like '%,$cid,%') and status_flg in ('D')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->relation(true)->where($strwhere) -> order("classid asc,orderid asc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		$this->display();
    }
	public function data_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		$this->get_class_list();
		$this->assign("classid",$cid);
		$orderid=M("Infolist")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->assign("posttime",date("Y-m-d H:i:s"));

		$this->display();
    }
	public function data_add_save()
	{
		if(trim($_POST['title'])=="")
		{
			$this->error("必须输入标题");
			exit;
		}
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		if ($model->create())
		{			
			$model->flag1 = $_POST['flag1']=="Y"?"Y":"N";
			$model->flag2 = $_POST['flag2']=="Y"?"Y":"N";
			$model->flag3 = $_POST['flag3']=="Y"?"Y":"N";
			$model->flag4 = $_POST['flag4']=="Y"?"Y":"N";
			$model->flag5 = $_POST['flag5']=="Y"?"Y":"N";
			$model->posttime=strtotime($_POST['posttime']);
			$dataid=$model->add();			
			
			$classinfo=M("Infoclass")->find($_POST['classid']);
			$data = array('parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
			$model->where("id=$dataid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Infolist");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->get_class_list();
		$this->display();
    }
	public function data_edit_save()
	{
		if(trim($_POST['title'])=="")
		{
			$this->error("必须输入标题");
			exit;
		}
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		if ($model->create())
		{
			$model->flag1 = $_POST['flag1']=="Y"?"Y":"N";
			$model->flag2 = $_POST['flag2']=="Y"?"Y":"N";
			$model->flag3 = $_POST['flag3']=="Y"?"Y":"N";
			$model->flag4 = $_POST['flag4']=="Y"?"Y":"N";
			$model->flag5 = $_POST['flag5']=="Y"?"Y":"N";
			$model->posttime=strtotime($_POST['posttime']);
			$model->save(); // 保存数据
			
			$classinfo=M("Infoclass")->find($_POST['classid']);
			$data = array('parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
			$model->where("id=".$_POST['id'])->setField($data);
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del()
	{
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		$data = array('status_flg'=>'D');
		$model->where("id in (".$_GET["id"].")")->setField($data); // 仅标记为删除,【回收站】功能
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_todo()
	{
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list_rec'));
		$model->where("id in (".$_GET["id"].")")->delete();
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_undo()
	{
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list_rec'));
		$data = array('status_flg'=>'Y');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("还原成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_y()
	{
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		$data = array('status_flg'=>'Y');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_n()
	{
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		$data = array('status_flg'=>'N');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_move_byid()
	{
		if($_GET['classid']=='' or $_GET['id']=='')
		{
			$this->error("参数错误");
			exit;
		}
		//var_dump($_GET['classid'],$_GET['id']);
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		
		$classinfo=M("Infoclass")->find($_GET['classid']);
		//var_dump($classinfo);exit;
		$data = array('classid'=>$_GET['classid'],'parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("转移成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_move_byclass()
	{
		if($_GET['classid']==$_GET['oldclassid'])
		{
			$this->error("原类别与新类别相同，不需要转移");
			exit;
		}
		if($_GET['classid']=='' or $_GET['oldclassid']=='')
		{
			$this->error("类别参数错误");
			exit;
		}
		
		$model = M("Infolist");
		$this->assign("jumpUrl",U('Info/data_list'));
		
		$classinfo=M("Infoclass")->find($_GET['classid']);
		$data = array('classid'=>$_GET['classid'],'parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
		$model->where("classid=".$_GET["oldclassid"]." or parentstr like '%,".$_GET["oldclassid"].",%'")->setField($data);
		if ($model)
		{
			$this->success("按类别批量转移成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	public function get_parentstr($cid,$pid=0)
	{
		if($pid==0)
		{
			return ',0,';
		}
		else
		{
			return M("Infoclass")->where("id=$pid")->getField("parentstr").$pid.",";
		}
	}
	
	public function set_parentstr($cid,$pid=0,$pstr='')
	{
		
		//获取当前parentstr
		if($pstr == '')
		{
			$parstr = $this->get_parentstr($cid,$pid);
		}
		else
		{
			$parstr = $pstr.$pid.',';
		}
		
		//更新类别表parentstr
		M("Infoclass")->where("id=$cid")->setField("parentstr",$parstr);
			
		//更新信息表（当前级别）
		$data = array('parentid'=>$pid,'parentstr'=>$parstr);
		M("Infolist")->where("classid=$cid")->setField($data);


		//获取当前ID下所有子ID
		$arrNav = M("Infoclass")->where("parentid=$cid")->select();
		foreach($arrNav as $row)
		{

			/*
			//更新类别表parentstr
			M("Infoclass")->where("id=$row['id']")->setField("parentstr",$parstr.$cid.",");

			//更新信息表parentstr
			//更新子信息表
			$data = array('parentid'=>$row['parentid'],'parentstr'=>$parstr.$cid.",");
			M("Infolist")->where("classid=$row['id']")->setField($data);
			*/

			//传递下级参数,继续更新
			$this->set_parentstr($row['id'], $row['parentid'], $parstr);
		}

	}
	
	public function get_class_list($classlist_name='classlist')
	{
		$classlist = M("Infoclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") ->where("status_flg='Y'") -> order("bpath asc")->select();
		foreach($classlist as $key=>$value)
		{
			$classlist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign($classlist_name,$classlist);
	}


}
?>