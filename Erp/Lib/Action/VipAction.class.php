<?php

class VipAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }	
	
	
	/* 数据处理 */
	
	public function data_list()
	{
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=M("Vip");
		$strwhere="status_flg in ('Y','N') and (vip_c like '%$keywords%' or title like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere)->order("point desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('keywords',$keywords);
		
		$this->display();
    }
	
	public function data_add()
	{
		$this->display();
    }
	public function data_add_save()
	{
		if(trim($_POST['vip_c'])=="")
		{
			$this->error("必须输入会员号码");
			exit;
		}
		
		$check_vip_c = $this->check_vip_c($_POST['vip_c'],0);
		if($check_vip_c)
		{
			$this->error("会员".$_POST['vip_c']."已存在");
			exit;
		}
		$_POST['mobile'] = $_POST['vip_c'];
		
		$model = M("Vip");
		$this->assign("jumpUrl",U('Vip/data_list'));
		if ($model->create())
		{			
			$get_point = (int)C('REGISTER_VIP_GET_POINT');
			$model->created = time();
			$model->updated = time();
			$model->status_flg = "Y";
			$model->point = $get_point;
			$dataid=$model->add();
			
			if($get_point)
			{
				$vip_log = array();
				$vip_log['vip_c'] = $_POST['vip_c'];
				$vip_log['act_point'] = $get_point;
				$vip_log['act_uid'] = (int)$_SESSION[C('USER_AUTH_KEY')];
				$vip_log['remark'] = '后台创建会员送积分';
				$vip_log['created'] = time();
				$vip_log['status_flg'] = 'Y';
				M('VipLog')->data($vip_log)->add();
			}
			
			$this->success("创建成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Vip");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->display();
    }
	public function data_edit_save()
	{
		if(trim($_POST['vip_c'])=="")
		{
			$this->error("必须输入会员号码");
			exit;
		}
		
		$check_vip_c = $this->check_vip_c($_POST['vip_c'],$_POST['id']);
		if($check_vip_c)
		{
			$this->error("会员".$_POST['vip_c']."已存在");
			exit;
		}
		$_POST['mobile'] = $_POST['vip_c'];
		
		$model = M("Vip");
		$this->assign("jumpUrl",U('Vip/data_list'));
		if ($model->create())
		{
			$model->updated = time();
			$model->save(); // 保存数据
			
			if((int)$_POST['act_point'])
			{
				$vip_log = array();
				$vip_log['vip_c'] = $_POST['vip_c'];
				$vip_log['act_point'] = (int)$_POST['act_point'];
				$vip_log['act_uid'] = (int)$_SESSION[C('USER_AUTH_KEY')];
				$vip_log['remark'] = '后台调整会员积分~'.$_POST['act_remark'];
				$vip_log['created'] = time();
				$vip_log['status_flg'] = 'Y';
				M('VipLog')->data($vip_log)->add();
			}
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function data_status_to_y()
	{
		$model = M("Vip");
		$this->assign("jumpUrl",U('Vip/data_list'));
		$data = array('status_flg'=>'Y','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_n()
	{
		$model = M("Vip");
		$this->assign("jumpUrl",U('Vip/data_list'));
		$data = array('status_flg'=>'N','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	private function check_vip_c($vip_c,$vip_id)
	{
		$model = M("Vip");
		return $model->where(array('vip_c'=>$vip_c,'id'=>array('neq'=>(int)$vip_id)))->find();
	}
	
	public function vip_log_list()
	{
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=M("VipLog");
		$strwhere="status_flg in ('Y') and (vip_c like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere)->order("id asc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('keywords',$keywords);
		
		$this->display();
    }

	public function point_to_card()
	{
		$map = array();
        $map["where"]["vip_c"] = $_GET["vip_c"];

        $model= M("Vip");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$point2card_rate = (double)C('POINT2CARD_RATE') * 100;
		$this->assign("point2card_rate",$point2card_rate);
		
		$this->display();
	}
	
	public function point_to_card_save()
	{		
		
		if($_POST['vip_c'] && $_POST['card_c'] && (int)$_POST['act_point'] && (double)$_POST['act_amount'])
		{
			$curr_point = M("Vip")->where(array('vip_c'=>$_POST['vip_c']))->getField('point');
			if((int)$curr_point < (int)$_POST['act_point'])
			{
				$this->error('使用的积分不能大于可用积分');
			}
			else
			{
				$model_vip = M("Vip");
				$model_vip_log = M("VipLog");
				$model_shopping_card = M("ShoppingCard");
				$model_shopping_card_log = M("ShoppingCardLog");
				$model_vip->startTrans();
				
				//$vip_point_flg = $model_vip->where("vip_c='".$_POST['vip_c']."'")->setDec('point',(int)$_POST['act_point']); // 用户减积分
				$vip_point_data['point'] = array('exp','point-'.(int)$_POST['act_point']);// 用户减积分
				$vip_point_data['updated'] = time();
				$vip_point_flg = $model_vip->where("vip_c='".$_POST['vip_c']."'")->data($vip_point_data)->save();
				
				//$shopping_card_flg = $model_shopping_card->where("card_c='".$_POST['card_c']."'")->setInc('open_amount',(double)$_POST['act_amount']); // 购物卡加金额
				$shopping_card_data['open_amount'] = array('exp','open_amount+'.(double)$_POST['act_amount']);
				$shopping_card_data['updated'] = time();
				$shopping_card_flg = $model_shopping_card->where("card_c='".$_POST['card_c']."'")->data($shopping_card_data)->save();
				
				$vip_log = array();
				$vip_log['vip_c'] = $_POST['vip_c'];
				$vip_log['act_point'] = (int)$_POST['act_point'] * -1;
				$vip_log['act_uid'] = (int)$_SESSION[C('USER_AUTH_KEY')];
				$vip_log['remark'] = '积分兑换购物卡#卡号#'.$_POST['card_c'].'#金额#'.$_POST['act_amount'];
				$vip_log['created'] = time();
				$vip_log['status_flg'] = 'Y';
				$vip_log_flg = $model_vip_log->data($vip_log)->add();
				
				$shopping_card_log = array();
				$shopping_card_log['card_c'] = $_POST['card_c'];
				$shopping_card_log['act_amount'] = (double)$_POST['act_amount'];
				$shopping_card_log['act_uid'] = (int)$_SESSION[C('USER_AUTH_KEY')];
				$shopping_card_log['remark'] = '积分兑换购物卡#会员#'.$_POST['vip_c'].'#积分#'.$_POST['act_point'];
				$shopping_card_log['created'] = time();
				$shopping_card_log['status_flg'] = 'Y';
				$shopping_card_log_flg = $model_shopping_card_log->data($shopping_card_log)->add();
				
				if($vip_point_flg && $shopping_card_flg && $vip_log_flg && $shopping_card_log_flg)
				{
					$model_vip->commit();
					$this->assign("jumpUrl",U('Vip/data_list'));
					$this->success('兑换成功');
				}
				else
				{
					$model_vip->rollback();
					$this->error('写数据失败');
				}
			}
		}
		else
		{
			$this->error('参数错误');
		}
	}

}
?>