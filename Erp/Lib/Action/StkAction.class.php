<?php

class StkAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 分类数据处理 */
	
	public function class_list()
	{
		$datalist = M("Stkclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function class_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		
		$this->get_class_list();
		$this->assign("classid",$cid);
		$orderid=M("Stkclass")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function class_add_save()
	{
		$model = D("Stkclass");
		$this->assign("jumpUrl",U('Stk/class_list'));
		if ($model->create())
		{			
			$classid=$model->add();
			
			$data = array('parentstr'=>$this->get_parentstr($classid,$_POST['parentid']));
			$model->where("id=$classid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Stkclass");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$this->get_class_list();
		
		$this->display();
    }
	public function class_edit_save()
	{
		$model = D("Stkclass");
		$this->assign("jumpUrl",U('Stk/class_list'));
		if ($model->create())
		{			
			//var_dump($model->status_flg);exit;
			$model->save(); // 保存数据
			if($_POST['status_flg']=="N")
			{
				//当禁用类别时，自动禁用下级所有类别
				$data = array('status_flg'=>'N');
				$model->where("parentid=".$_POST['id']." or parentstr like '%,".$_POST['id'].",%'")->setField($data);
			}
			$this->set_parentstr($_POST['id'],$_POST['parentid']);
			$this->success("保存成功!");
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_del()
	{
		$model = M("Stkclass");
		$this->assign("jumpUrl",U('Stk/class_list'));
		
		$data1 = array('parentid'=>0,'parentstr'=>'0,'); //将下级类别的父类设置为0
		$data2 = array('classid'=>0,'parentid'=>0,'parentstr'=>'0,'); //将对应的文章类别设置为0
		foreach (explode(',',$_GET["id"]) as $s)
		{
			if($s)
			{
				$model->where("parentid=$s or parentstr like '%,$s,%'")->setField($data1);
				M("Stkmas")->where("classid=$s  or parentstr like '%,$s,%'")->setField($data2);
			}
		}		
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功: 此类别的所有下级类别及归属的内容都已转为顶级（未归类）!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	/* 品牌数据处理 */
	
	public function brand_list()
	{
		$datalist = M("Stkbrand")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function brand_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;

		$this->assign("classid",$cid);
		$orderid=M("Stkbrand")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function brand_add_save()
	{
		$model = D("Stkbrand");
		$this->assign("jumpUrl",U('Stk/brand_list'));
		if ($model->create())
		{			
			$classid=$model->add();			
			$this->success("创建成功!");			
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function brand_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Stkbrand");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$this->display();
    }
	public function brand_edit_save()
	{
		$model = D("Stkbrand");
		$this->assign("jumpUrl",U('Stk/brand_list'));
		if ($model->create())
		{			
			$model->save(); // 保存数据
			$this->success("保存成功!");
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function brand_del()
	{
		$model = M("Stkbrand");
		$this->assign("jumpUrl",U('Stk/brand_list'));
		
		$data = array('brandid'=>0); //将对应的商品品牌设置为0
		M("Stkmas")->where("brandid in (".$_GET["id"].")")->setField($data);
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	
	/* 数据处理 */
	
	public function data_list()
	{
		$cid = $_REQUEST['cid'];
		if(!$cid) $cid=0;
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("Stkmas");
		$strwhere="(classid=$cid or parentstr like '%,$cid,%') and status_flg in ('Y','N') and (stk_c like '%$keywords%' or title like '%$keywords%' or keywords like '%$keywords%' or description like '%$keywords%' or content like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&cid=".urlencode($cid);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->relation(true)->where($strwhere) -> order("orderid desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->get_class_list();
		$this->get_class_list('classlist2');
		$this->assign('cid',$cid);
		$this->assign('keywords',$keywords);
		
		$this->display();
    }
    public function data_print()
	{
		$cid = $_REQUEST['cid'];
		if(!$cid) $cid=0;
		$keywords = $_REQUEST['keywords'];
		
		$model=D("Stkmas");
		$strwhere="(classid=$cid or parentstr like '%,$cid,%') and status_flg in ('Y','N') and (stk_c like '%$keywords%' or title like '%$keywords%' or keywords like '%$keywords%' or description like '%$keywords%' or content like '%$keywords%')";
				
		
		$datalist = $model->relation(true)->where($strwhere) -> order("orderid desc")->select();
		$this->assign("datalist",$datalist);
		
		$this->display();
    }
	public function data_list_rec()
	{
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("Stkmas");
		$strwhere="(classid=$cid or parentstr like '%,$cid,%') and status_flg in ('D')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->relation(true)->where($strwhere) -> order("orderid desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		$this->display();
    }
	public function data_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		$this->get_class_list();
		$this->get_brand_list();
		$this->assign("classid",$cid);
		$orderid=M("Stkmas")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->assign("posttime",date("Y-m-d H:i:s"));

		$this->display();
    }
	public function data_add_save()
	{
		if(trim($_POST['title'])=="")
		{
			$this->error("必须输入商品名称");
			exit;
		}
		if($this->check_code_double($_POST['stk_c'],$_POST['id']))
		{
			$this->error("条码编号已存在");
			exit;
		}
		
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		if ($model->create())
		{			
			$model->updated = time();
			$model->flag1 = $_POST['flag1']=="Y"?"Y":"N";
			$model->flag2 = $_POST['flag2']=="Y"?"Y":"N";
			$model->flag3 = $_POST['flag3']=="Y"?"Y":"N";
			$model->flag4 = $_POST['flag4']=="Y"?"Y":"N";
			$model->flag5 = $_POST['flag5']=="Y"?"Y":"N";
			$model->posttime=strtotime($_POST['posttime']);
			$model->diyinfo=serialize($_POST['diyinfo']);            
            $func = A("Func");
            $model->keywords = $func->pinyin1($_POST['title']);

			$dataid=$model->add();			
			
			$classinfo=M("Stkclass")->find($_POST['classid']);
			$data = array('parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
			$model->where("id=$dataid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Stkmas");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->get_class_list();
		$this->get_brand_list();
		$this->display();
    }
	public function data_edit_save()
	{
		if(trim($_POST['title'])=="")
		{
			$this->error("必须输入商品名称");
			exit;
		}
		if($this->check_code_double($_POST['stk_c'],$_POST['id']))
		{
			$this->error("条码编号已存在");
			exit;
		}
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		if ($model->create())
		{
			$model->updated = time();
			$model->flag1 = $_POST['flag1']=="Y"?"Y":"N";
			$model->flag2 = $_POST['flag2']=="Y"?"Y":"N";
			$model->flag3 = $_POST['flag3']=="Y"?"Y":"N";
			$model->flag4 = $_POST['flag4']=="Y"?"Y":"N";
			$model->flag5 = $_POST['flag5']=="Y"?"Y":"N";
			$model->posttime=strtotime($_POST['posttime']);
			$model->diyinfo=serialize($_POST['diyinfo']);
            $func = A("Func");
            $model->keywords = $func->pinyin1($_POST['title']);
            
			$model->save(); // 保存数据
			
			$classinfo=M("Stkclass")->find($_POST['classid']);
			$data = array('parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
			$model->where("id=".$_POST['id'])->setField($data);
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del()
	{
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		$data = array('status_flg'=>'D');
		$model->where("id in (".$_GET["id"].")")->setField($data); // 仅标记为删除,【回收站】功能
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_todo()
	{
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list_rec'));
		$model->where("id in (".$_GET["id"].")")->delete();
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_undo()
	{
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list_rec'));
		$data = array('status_flg'=>'Y','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("还原成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_y()
	{
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		$data = array('status_flg'=>'Y','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_n()
	{
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		$data = array('status_flg'=>'N','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_move_byid()
	{
		if($_GET['classid']=='' or $_GET['id']=='')
		{
			$this->error("参数错误");
			exit;
		}
		//var_dump($_GET['classid'],$_GET['id']);
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		
		$classinfo=M("Stkclass")->find($_GET['classid']);
		//var_dump($classinfo);exit;
		$data = array('classid'=>$_GET['classid'],'parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr'],'updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("转移成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_move_byclass()
	{
		if($_GET['classid']==$_GET['oldclassid'])
		{
			$this->error("原类别与新类别相同，不需要转移");
			exit;
		}
		if($_GET['classid']=='' or $_GET['oldclassid']=='')
		{
			$this->error("类别参数错误");
			exit;
		}
		
		$model = M("Stkmas");
		$this->assign("jumpUrl",U('Stk/data_list'));
		
		$classinfo=M("Stkclass")->find($_GET['classid']);
		$data = array('classid'=>$_GET['classid'],'parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr'],'updated'=>time());
		$model->where("classid=".$_GET["oldclassid"]." or parentstr like '%,".$_GET["oldclassid"].",%'")->save($data);
		if ($model)
		{
			$this->success("按类别批量转移成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	public function get_parentstr($cid,$pid=0)
	{
		if($pid==0)
		{
			return ',0,';
		}
		else
		{
			return M("Stkclass")->where("id=$pid")->getField("parentstr").$pid.",";
		}
	}
	
	public function set_parentstr($cid,$pid=0,$pstr='')
	{
		
		//获取当前parentstr
		if($pstr == '')
		{
			$parstr = $this->get_parentstr($cid,$pid);
		}
		else
		{
			$parstr = $pstr.$pid.',';
		}
		
		//更新类别表parentstr
		M("Stkclass")->where("id=$cid")->setField("parentstr",$parstr);
			
		//更新商品表（当前级别）
		$data = array('parentid'=>$pid,'parentstr'=>$parstr);
		M("Stkmas")->where("classid=$cid")->setField($data);


		//获取当前ID下所有子ID
		$arrNav = M("Stkclass")->where("parentid=$cid")->select();
		foreach($arrNav as $row)
		{
			//传递下级参数,继续更新
			$this->set_parentstr($row['id'], $row['parentid'], $parstr);
		}

	}
	
	public function get_class_list($classlist_name='classlist')
	{
		$classlist = M("Stkclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") ->where("status_flg='Y'") -> order("bpath asc")->select();
		foreach($classlist as $key=>$value)
		{
			$classlist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign($classlist_name,$classlist);
	}
	
	public function get_brand_list($brandlist_name='brandlist')
	{
		$brandlist = M("Stkbrand")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") ->where("status_flg='Y'") -> order("bpath asc")->select();
		foreach($brandlist as $key=>$value)
		{
			$brandlist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign($brandlist_name,$brandlist);
	}
	
	public function stkmas_popup()
	{
		$objid=$_REQUEST['objid'];
		$objcode=$_REQUEST['objcode'];
		$objtitle=$_REQUEST['objtitle'];
		$objuom=$_REQUEST['objuom'];
		$objpurprice=$_REQUEST['objpurprice'];
		$objsalesprice=$_REQUEST['objsalesprice'];
        $objspec=$_REQUEST['objspec'];
        $objmadein=$_REQUEST['objmadein'];
        $callback=$_REQUEST['callback'];
        $objdetail=$_REQUEST['objdetail'];		
        
		$this->assign("objid",$objid);
		$this->assign("objcode",$objcode);
		$this->assign("objtitle",$objtitle);
		$this->assign("objuom",$objuom);
		$this->assign("objpurprice",$objpurprice);
		$this->assign("objsalesprice",$objsalesprice);
        $this->assign("objspec",$objspec);
        $this->assign("objmadein",$objmadein);
        $this->assign("callback",$callback);
        $this->assign("objdetail",$objdetail);		
		//explode(separator,string,limit)
		
		$cid = (int)$_REQUEST['cid'];
		$keepwindow=(int)$_REQUEST['keepwindow'];		
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$itemno=(int)$_REQUEST['itemno'];
		if($itemno<=0)
		{
			$objArr = explode('[',$objid,2);
			$itemno = (int)$objArr[1];
		}
		
		$model=M("Stkmas");
		$strwhere="(classid=$cid or parentstr like '%,$cid,%') and status_flg in ('Y') and (stk_c like '%$keywords%' or title like '%$keywords%' or keywords like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&cid=".urlencode($cid);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$Page->parameter   .=   "&objid=".urlencode($objid);
		$Page->parameter   .=   "&objcode=".urlencode($objcode);
		$Page->parameter   .=   "&objtitle=".urlencode($objtitle);
		$Page->parameter   .=   "&objuom=".urlencode($objuom);
		$Page->parameter   .=   "&objpurprice=".urlencode($objpurprice);
		$Page->parameter   .=   "&objsalesprice=".urlencode($objsalesprice);
        $Page->parameter   .=   "&objspec=".urlencode($objspec);
        $Page->parameter   .=   "&objmadein=".urlencode($objmadein);
        $Page->parameter   .=   "&callback=".urlencode($callback);
        $Page->parameter   .=   "&objdetail=".urlencode($objdetail);
		$Page->parameter   .=   "&keepwindow=".urlencode($keepwindow);
		$Page->parameter   .=   "&itemno=".urlencode($itemno);
		
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("orderid asc,id asc")->limit($Page->firstRow.','.$Page->listRows)->select();
        if($objdetail)
        {
            $model_stk_detail=D("StkdetailView");
            foreach($datalist as $k=>$v)
            {
                unset($strfield);unset($strgroup);unset($strwhere);unset($detail_list);
                $strfield="wh_title,batch_no,expiry_date,approval_no,spec,madein,sum(open_qty) as open_qty";
                $strgroup="wh_title,batch_no,expiry_date,approval_no,spec,madein";
                $strwhere="Stkdetail.open_qty>0";
                $strwhere.=" and Stkmas.id = ".$v["id"];		
                $detail_list = $model_stk_detail->field($strfield)->where($strwhere)->group($strgroup)->select();
                $datalist[$k]['stk_detail']=$detail_list;
            }
        }
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->get_class_list();
		$this->assign('cid',$cid);
		$this->assign('keywords',$keywords);
		$this->assign('keepwindow',$keepwindow);
		$this->assign('itemno',$itemno);
		
		$this->display();
    }
	
	private function check_code_double($stk_c,$stk_id)
	{
		if($stk_c=='')
		{
			return false;
		}
		else if((int)C('IGNORE_STK_CODE_DOUBLE'))
		{
			return false;
		}
		$model = M("Stkmas");
		return (int)($model->where(array('stk_c'=>$stk_c,'id'=>array('NEQ',(int)$stk_id)))->getField('id'));
	}

}
?>