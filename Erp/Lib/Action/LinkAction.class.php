<?php

class LinkAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 分类数据处理 */
	
	public function class_list()
	{
		$datalist = M("Linkclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function class_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		
		$this->get_class_list();
		$this->assign("classid",$cid);
		$orderid=M("Linkclass")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function class_add_save()
	{
		$model = D("Linkclass");
		$this->assign("jumpUrl",U('Link/class_list'));
		if ($model->create())
		{			
			$classid=$model->add();
			
			$data = array('parentstr'=>$this->get_parentstr($classid,$_POST['parentid']));
			$model->where("id=$classid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Linkclass");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$this->get_class_list();
		
		$this->display();
    }
	public function class_edit_save()
	{
		$model = D("Linkclass");
		$this->assign("jumpUrl",U('Link/class_list'));
		if ($model->create())
		{			
			//var_dump($model->status_flg);exit;
			$model->save(); // 保存数据
			if($_POST['status_flg']=="N")
			{
				//当禁用类别时，自动禁用下级所有类别
				$data = array('status_flg'=>'N');
				$model->where("parentid=".$_POST['id']." or parentstr like '%,".$_POST['id'].",%'")->setField($data);
			}
			$this->set_parentstr($_POST['id'],$_POST['parentid']);
			$this->success("保存成功!");
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function class_del()
	{
		$model = M("Linkclass");
		$this->assign("jumpUrl",U('Link/class_list'));
		
		$data1 = array('parentid'=>0,'parentstr'=>'0,'); //将下级类别的父类设置为0
		$data2 = array('classid'=>0,'parentid'=>0,'parentstr'=>'0,'); //将对应的文章类别设置为0
		foreach (explode(',',$_GET["id"]) as $s)
		{
			if($s)
			{
				$model->where("parentid=$s or parentstr like '%,$s,%'")->setField($data);
				M("Linklist")->where("classid=$s  or parentstr like '%,$s,%'")->setField($data);
			}
		}		
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功: 此类别的所有下级类别及归属的内容都已转为顶级（未归类）!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	
	
	/* 数据处理 */
	
	public function data_list()
	{
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		$datalist = D("Linklist")->relation(true)->where("classid=$cid or parentstr like '%,$cid,%'") -> order("classid asc,orderid asc")->select();
		//var_dump($datalist);
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function data_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		$this->get_class_list();
		$this->assign("classid",$cid);
		$orderid=M("Linklist")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function data_add_save()
	{
		$model = M("Linklist");
		$this->assign("jumpUrl",U('Link/data_list'));
		if ($model->create())
		{			
			$dataid=$model->add();
			
			$classinfo=M("Linkclass")->find($_POST['classid']);
			$data = array('parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
			$model->where("id=$dataid")->setField($data);
			
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Linklist");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->get_class_list();
		$this->display();
    }
	public function data_edit_save()
	{
		$model = M("Linklist");
		$this->assign("jumpUrl",U('Link/data_list'));
		if ($model->create())
		{
			$model->save(); // 保存数据
			
			$classinfo=M("Linkclass")->find($_POST['classid']);
			$data = array('parentid'=>$classinfo['parentid'],'parentstr'=>$classinfo['parentstr']);
			$model->where("id=".$_POST['id'])->setField($data);
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del()
	{
		$model = M("Linklist");
		$this->assign("jumpUrl",U('Link/data_list'));		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function get_parentstr($cid,$pid=0)
	{
		if($pid==0)
		{
			return ',0,';
		}
		else
		{
			return M("Linkclass")->where("id=$pid")->getField("parentstr").$pid.",";
		}
	}
	
	public function set_parentstr($cid,$pid=0,$pstr='')
	{
		
		//获取当前parentstr
		if($pstr == '')
		{
			$parstr = $this->get_parentstr($cid,$pid);
		}
		else
		{
			$parstr = $pstr.$pid.',';
		}
		
		//更新类别表parentstr
		M("Linkclass")->where("id=$cid")->setField("parentstr",$parstr);
			
		//更新信息表（当前级别）
		$data = array('parentid'=>$pid,'parentstr'=>$parstr);
		M("Linklist")->where("classid=$cid")->setField($data);


		//获取当前ID下所有子ID
		$arrNav = M("Linkclass")->where("parentid=$cid")->select();
		foreach($arrNav as $row)
		{

			/*
			//更新类别表parentstr
			M("Linkclass")->where("id=$row['id']")->setField("parentstr",$parstr.$cid.",");

			//更新信息表parentstr
			//更新子信息表
			$data = array('parentid'=>$row['parentid'],'parentstr'=>$parstr.$cid.",");
			M("Linklist")->where("classid=$row['id']")->setField($data);
			*/

			//传递下级参数,继续更新
			$this->set_parentstr($row['id'], $row['parentid'], $parstr);
		}

	}
	
	public function get_class_list()
	{
		$datalist = M("Linkclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") ->where("status_flg='Y'") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("datalist",$datalist);
	}


}
?>