<?php

class WarehouseAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 仓库基础数据处理 */
	
	public function whmas_list()
	{
		$datalist = M("Whmas")->where("status_flg in ('Y','N')") -> order("status_flg asc,orderid asc")->select();
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function whmas_add()
	{		
		$orderid=M("Whmas")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function whmas_add_save()
	{
		$model = D("Whmas");
		$this->assign("jumpUrl",U('Warehouse/whmas_list'));
		if ($model->create())
		{			
			$model->posttime=time();
			$model->diyinfo=serialize($_POST['diyinfo']);
			$classid=$model->add();
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function whmas_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Whmas");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);		
		$this->display();
    }
	public function whmas_edit_save()
	{
		$model = D("Whmas");
		$this->assign("jumpUrl",U('Warehouse/whmas_list'));
		if ($model->create())
		{
			$model->diyinfo=serialize($_POST['diyinfo']);
			$model->save(); // 保存数据
			$this->success("保存成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function whmas_del()
	{
		$model = M("Whmas");
		$this->assign("jumpUrl",U('Warehouse/whmas_list'));
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function whmas_status_to_y()
	{
		$model = M("Whmas");
		$this->assign("jumpUrl",U('Warehouse/whmas_list'));
		$data = array('status_flg'=>'Y');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function whmas_status_to_n()
	{
		$model = M("Whmas");
		$this->assign("jumpUrl",U('Warehouse/whmas_list'));
		$data = array('status_flg'=>'N');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function whmas_popup()
	{
		$objid=$_GET['objid'];
		$objtitle=$_GET['objtitle'];
        $callback=$_GET['callback'];
        
		$this->assign("objid",$objid);
		$this->assign("objtitle",$objtitle);
        $this->assign("callback",$callback);
		
		$datalist = M("Whmas")->where("status_flg in ('Y')") -> order("orderid asc")->select();
		$this->assign("datalist",$datalist);
		$this->display();
    }
	
	
	/* 出入库类型处理 */
	
	public function iotype_list()
	{
		$datalist = M("Iotype")->where("status_flg in ('Y','N')") -> order("status_flg asc,flag asc,orderid asc")->select();
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function iotype_add()
	{		
		$orderid=M("Iotype")->getField("max(orderid)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function iotype_add_save()
	{
		$model = D("Iotype");
		$this->assign("jumpUrl",U('Warehouse/iotype_list'));
		if ($model->create())
		{			
			$model->posttime=time();
			$model->diyinfo=serialize($_POST['diyinfo']);
			$classid=$model->add();
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function iotype_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Iotype");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);		
		$this->display();
    }
	public function iotype_edit_save()
	{
		$model = D("Iotype");
		$this->assign("jumpUrl",U('Warehouse/iotype_list'));
		if ($model->create())
		{
			$model->diyinfo=serialize($_POST['diyinfo']);
			$model->save(); // 保存数据
			$this->success("保存成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function iotype_del()
	{
		$model = M("Iotype");
		$this->assign("jumpUrl",U('Warehouse/iotype_list'));
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			$this->success("删除成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function iotype_status_to_y()
	{
		$model = M("Iotype");
		$this->assign("jumpUrl",U('Warehouse/iotype_list'));
		$data = array('status_flg'=>'Y');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function iotype_status_to_n()
	{
		$model = M("Iotype");
		$this->assign("jumpUrl",U('Warehouse/iotype_list'));
		$data = array('status_flg'=>'N');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function iotype_popup()
	{
		$flg=$_GET['flg'];
		$objid=$_GET['objid'];
		$objtitle=$_GET['objtitle'];
		$callback=$_GET['callback'];
		$this->assign("objid",$objid);
		$this->assign("objtitle",$objtitle);
		$this->assign("callback",$callback);
		
		$datalist = M("Iotype")->where("status_flg in ('Y') and flag='$flg'") -> order("orderid asc")->select();
		$this->assign("datalist",$datalist);
		$this->display();
    }
	
	
	//库存明细
	public function stk_detail()
	{
		$model=D("StkdetailView");
		
		$wh_id = $_REQUEST['wh_id'];
		$keywords = $_REQUEST['keywords'];
        $cid = $_REQUEST['cid'];
		
		$this->assign('wh_id',$wh_id);
		$this->assign('keywords',$keywords);
        $this->assign('cid',$cid);
		
		import("ORG.Util.Page"); // 导入分页类
		
		$strwhere="Stkdetail.open_qty>0";
		if($wh_id){$strwhere.=" and Stkdetail.wh_id =$wh_id";}
		if($keywords){$strwhere.=" and (Stkmas.stk_c like '%$keywords%' or Stkmas.title like '%$keywords%')";}
        if($cid){$strwhere.=" and (Stkmas.classid = $cid or Stkmas.parentstr like '%,$cid,%')";}
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&wh_id=".urlencode($wh_id);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
        $Page->parameter   .=   "&cid=".urlencode($cid);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("Stkmas.stk_c asc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$whlist = M('Whmas') -> order("orderid asc")->select();
		$this->assign("whlist",$whlist);
		
		$stk_total=$model->where($strwhere)->field('sum(open_qty) as open_qty,sum(open_amount) as open_amount')->find();
		$this->assign('stk_total',$stk_total);
        
        $stk_class_list = M("Stkclass")->field("id,parentid,parentstr,classname,orderid,status_flg,concat(parentstr,'',id) as bpath") -> order("bpath asc")->select();
		foreach($stk_class_list as $key=>$value)
		{
			$stk_class_list[$key]['count'] = count(explode(',',$value['parentstr']));
		}
		$this->assign("stk_class_list",$stk_class_list);

		$this->display();
    }
    
    public function stk_detail_print()
	{
		$model=D("StkdetailView");
		
		$wh_id = $_REQUEST['wh_id'];
		$keywords = $_REQUEST['keywords'];
        $cid = $_REQUEST['cid'];
		
		$this->assign('wh_id',$wh_id);
		$this->assign('keywords',$keywords);
        $this->assign('cid',$cid);
		
		$strwhere="Stkdetail.open_qty>0";
		if($wh_id){$strwhere.=" and Stkdetail.wh_id =$wh_id";}
		if($keywords){$strwhere.=" and (Stkmas.stk_c like '%$keywords%' or Stkmas.title like '%$keywords%')";}
        if($cid){$strwhere.=" and (Stkmas.classid = $cid or Stkmas.parentstr like '%,$cid,%')";}
		
				
		$datalist = $model->where($strwhere) -> order("Stkmas.stk_c asc")->select();
		$this->assign("datalist",$datalist);
		
		$whlist = M('Whmas') -> order("orderid asc")->select();
		$this->assign("whlist",$whlist);
		
		$stk_total=$model->where($strwhere)->field('sum(open_qty) as open_qty,sum(open_amount) as open_amount')->find();
		$this->assign('stk_total',$stk_total);        

		$this->display();
    }
	
	//库存查询
	public function stk_search()
	{
		$model=D("StkdetailView");
		
		$wh_id = $_REQUEST['wh_id'];
		$keywords = $_REQUEST['keywords'];
		
		$this->assign('wh_id',$wh_id);
		$this->assign('keywords',$keywords);
		
		import("ORG.Util.Page"); // 导入分页类
		
		$strfield="wh_title,stk_title,stk_code,stk_uom,batch_no,expiry_date,approval_no,spec,madein,sum(open_qty) as open_qty";
		$strgroup="wh_title,stk_title,stk_code,stk_uom,batch_no,expiry_date,approval_no,spec,madein";
		$strwhere="Stkdetail.open_qty>0";		
		if($wh_id){$strwhere.=" and Stkdetail.wh_id =$wh_id";}
		if($keywords){$strwhere.=" and (Stkmas.stk_c like '%$keywords%' or Stkmas.title like '%$keywords%')";}
		
		//$count      = $model->field($strfield)->where($strwhere)->group($strgroup)->count(); // 查询满足要求的总记录数		
		$subQuery = $model->field($strfield)->where($strwhere)->group($strgroup)->select(false);
		$count      = $model->table($subQuery.' a')->count(); // 查询满足要求的总记录数
		
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&wh_id=".urlencode($wh_id);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->field($strfield)
					->where($strwhere) -> order("Stkmas.stk_c asc")->limit($Page->firstRow.','.$Page->listRows)
					->group($strgroup) -> select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$whlist = M('Whmas') -> order("orderid asc")->select();
		$this->assign("whlist",$whlist);
		
		$stk_total=$model->where($strwhere)->field('sum(open_qty) as open_qty,sum(open_amount) as open_amount')->find();
		$this->assign('stk_total',$stk_total);

		$this->display();
    }
    
    public function stk_search_print()
	{
		$model=D("StkdetailView");
		
		$wh_id = $_REQUEST['wh_id'];
		$keywords = $_REQUEST['keywords'];
		
		$this->assign('wh_id',$wh_id);
		$this->assign('keywords',$keywords);
		
		$strfield="wh_title,stk_title,stk_code,stk_uom,batch_no,expiry_date,approval_no,spec,madein,sum(open_qty) as open_qty";
		$strgroup="wh_title,stk_title,stk_code,stk_uom,batch_no,expiry_date,approval_no,spec,madein";
		$strwhere="Stkdetail.open_qty>0";		
		if($wh_id){$strwhere.=" and Stkdetail.wh_id =$wh_id";}
		if($keywords){$strwhere.=" and (Stkmas.stk_c like '%$keywords%' or Stkmas.title like '%$keywords%')";}
		
		$datalist = $model->field($strfield)
					->where($strwhere) -> order("Stkmas.stk_c asc")
					->group($strgroup) -> select();
		$this->assign("datalist",$datalist);
		
		$stk_total=$model->where($strwhere)->field('sum(open_qty) as open_qty,sum(open_amount) as open_amount')->find();
		$this->assign('stk_total',$stk_total);

		$this->display();
    }
	
	public function stk_track()
	{
		$model=D("StkdetailView");
		
		$strwhere='1=1';
        $strwhere.=" and refcode='".$_GET['refcode']."'";
		$strwhere.=" and refid='".$_GET['refid']."'";
		
		$datalist = $model->where($strwhere) -> order("id asc")->select();
		$this->assign("datalist",$datalist);
		
		$stk_total=$model->where($strwhere)->field('sum(dr_qty) as dr_qty,sum(dr_amount) as dr_amount,sum(cr_qty) as cr_qty,sum(cr_amount) as cr_amount')->find();
		$this->assign('stk_total',$stk_total);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	
	//库存下限报警
	public function stk_qtylow()
	{
		$model=D("StkmasView");
		
		$keywords = $_REQUEST['keywords'];
		$this->assign('keywords',$keywords);
		
		import("ORG.Util.Page"); // 导入分页类
		
		$strfield="title,stk_c,uom,qty_min,sum(IFNULL(open_qty,0)) as open_qty";
		$strgroup="title,stk_c,uom,qty_min";
		$strwhere="qty_min>0 and IFNULL(open_qty,0) < qty_min";
		if($keywords){$strwhere.=" and (Stkmas.stk_c like '%$keywords%' or Stkmas.title like '%$keywords%')";}
		
	
		$subQuery = $model->field($strfield)->group($strgroup)->select(false);
		$count    = M()->table($subQuery.' a')->where($strwhere)->count(); // 查询满足要求的总记录数
		
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = M()->table($subQuery.' a')->where($strwhere)->limit($Page->firstRow.','.$Page->listRows)-> select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出

		$this->display();
    }
	//库存上限报警
	public function stk_qtyhigh()
	{
		$model=D("StkmasView");
		
		$keywords = $_REQUEST['keywords'];
		$this->assign('keywords',$keywords);
		
		import("ORG.Util.Page"); // 导入分页类
		
		$strfield="title,stk_c,uom,qty_max,sum(open_qty) as open_qty";
		$strgroup="title,stk_c,uom,qty_max";
		$strwhere="qty_max>0 and IFNULL(open_qty,0) > qty_max";
		if($keywords){$strwhere.=" and (Stkmas.stk_c like '%$keywords%' or Stkmas.title like '%$keywords%')";}
		
	
		$subQuery = $model->field($strfield)->group($strgroup)->select(false);
		$count    = M()->table($subQuery.' a')->where($strwhere)->count(); // 查询满足要求的总记录数
		
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = M()->table($subQuery.' a')->where($strwhere)->limit($Page->firstRow.','.$Page->listRows)-> select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出

		$this->display();
    }
    
    public function get_batch()
	{
		header("Content-Type:text/html; charset=utf-8");
        
        $model=D("StkdetailView");
		
		$wh_id = $_REQUEST['wh_id'];
		$stk_id = $_REQUEST['stk_id'];
		if($wh_id && $stk_id)
		{
            $strfield="batch_no,expiry_date,approval_no,spec,madein,sum(open_qty) as open_qty";
            $strgroup="batch_no,expiry_date,approval_no,spec,madein";
            $strwhere="Stkdetail.open_qty>0";		
            if($wh_id){$strwhere.=" and Stkdetail.wh_id = $wh_id";}
            if($stk_id){$strwhere.=" and Stkmas.id = $stk_id";}
		
            $datalist = $model->field($strfield)->where($strwhere)->group($strgroup)->select();
            if($datalist)
			{
				foreach($datalist as $k=>$v)
                {
                    if($v['expiry_date'])
                    {
                        $datalist[$k]['expiry_date2']=date('Y-m-d',$v['expiry_date']);
                    }else{
                        $datalist[$k]['expiry_date2']='';
                    }
                }
                $result['msg']='ok';
				$result['data']=$datalist;
			}
			else
			{
				$result['msg']='没有库存';
				$result['data']='';
			}
		}
        else
        {
				$result['msg']='参数错误';
				$result['data']='';
		}		
		//var_dump($result);echo $model->getLastSql();exit;
		$jsonencode = json_encode($result);
		echo $jsonencode;
    }




}
?>