<?php

class StkioAction extends CommonAction
{
    public function index()
	{
		$this->stkin_list();
    }
	
	/* 其他入库单 */
	
	public function stkin_list()
	{
	
		$s_status_flg = $_REQUEST['s_status_flg'];
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("StkiomasView");
		$strwhere="Stkiomas.mas_code = 'STKIN'";
		if($s_status_flg){$strwhere.=" and Stkiomas.status_flg like '$s_status_flg'";}
		if($keywords){$strwhere.=" and (Stkiomas.mas_no like '%$keywords%' or Stkiomas.remark like '%$keywords%' or Iotype.title like '%$keywords%')";}	
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&s_status_flg=".urlencode($s_status_flg);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('s_status_flg',$s_status_flg);
		$this->assign('keywords',$keywords);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	
	public function stkin_add()
	{		
		$whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate",date('Y-m-d'));
		$this->display();
    }
	
	public function stkin_add_save()
	{		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();	
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->mas_no = stkio_masno_create($_POST['mas_code'],strtotime($_POST['fadate']));
			$model_mas->createtime=time();
			$model_mas->creater=$_SESSION[C('USER_AUTH_KEY')];
			$newid=$model_mas->add(); //保存主数据
			
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			//保存行明细
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
		
			if($newid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/stkin_edit?id='.$newid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	
	public function stkin_edit()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("StkiomasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("StkioitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
        
        $ids='0';
        foreach($mydata_item as $item)
        {
            if($item['stk_id'])
            {
                $ids.=','.$item['stk_id'];
            }
        }
        
        $model_stk = M('Stkmas');
        $stk_info = $model_stk->field('id,spec,madein')->where('id in ('.$ids.')')->select();
        
        foreach($mydata_item as $k1=>$v1)
        {
            foreach($stk_info as $k2=>$v2)
            {
                if($v1['stk_id']==$v2['id'])
                {
                    $mydata_item[$k1]['stk_spec']=$v2['spec'];
                    $mydata_item[$k1]['stk_madein']=$v2['madein'];
                }
            }
        }
        
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
        
        $whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate_write_off",date('Y-m-d'));
		
		$this->display();
    }
	
	public function stkin_edit_save()
	{		
		if(!$this->doc_check_status($_REQUEST['id'],"'A','E'"))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$spmasid = $_POST['id'];
			$model_mas->save();
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($spmasid && $delid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/stkin_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	public function stkin_save_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		if(!fiscalyp_check_common($_REQUEST['fadate'],2))
		{
			$this->error('会计期间已关闭'.$_REQUEST['fadate']);
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			//主记录
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->posttime=time();
			$model_mas->poster=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->status_flg='P';
			$spmasid=$_POST['id'];
			$model_mas->save();
			
			//明细记录
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			$amount_stk=0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
					
					$data_detail[$itmp]['fadate']=strtotime($_POST['fadate']);
					$data_detail[$itmp]['refcode']=$_POST['mas_code'];
					$data_detail[$itmp]['refid']=$spmasid;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['dr_qty']=$item['qty'];
					$data_detail[$itmp]['dr_amount']=$data[$itmp]['amount'];
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$data_detail[$itmp]['dr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_stk+=$data_detail[$itmp]['dr_amount'];
					
					$itmp += 1;
				}
			}
			$item_flg = $model_item->addAll($data);
			
			//生成存货明细账
			$detail_flg = M('Stkdetail')->addAll($data_detail);
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=strtotime($_POST['fadate']);
			$data_vou_mas['refcode']=$_POST['mas_code'];
			$data_vou_mas['refid']=$spmasid;
			$data_vou_mas['vouno']=vouno_create($data_vou_mas['fadate']);
			$data_vou_mas['remark']='从'.$_POST['mas_code'].'#'.$_POST['mas_no'].'#'.$_POST['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where('code like \'STKIO-%\'')->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			if($facc['STKIO-FEE']['acc_code'])
			{
				$data_vou_item[0]['masid']=$vouid;
				$data_vou_item[0]['acc_code']=$facc['STKIO-FEE']['acc_code'];
				$data_vou_item[0]['acc_title']=$facc['STKIO-FEE']['acc_title'];
				$data_vou_item[0]['customer_id']=0;
				$data_vou_item[0]['supplier_id']=0;
				$data_vou_item[0]['curr_code']=$currmas['code'];
				$data_vou_item[0]['curr_rate']=$currmas['rate'];
				$data_vou_item[0]['curr_dr']=0;
				$data_vou_item[0]['dr']=0;
				$data_vou_item[0]['curr_cr']=get_round($amount_stk);
				$data_vou_item[0]['cr']=get_round($amount_stk);
				$data_vou_item[0]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[0]['open_amount']=get_round($amount_stk);
				$data_vou_item[0]['refid']=0;
			}
			if($facc['STKIO-STK']['acc_code'])
			{
				$data_vou_item[1]['masid']=$vouid;
				$data_vou_item[1]['acc_code']=$facc['STKIO-STK']['acc_code'];
				$data_vou_item[1]['acc_title']=$facc['STKIO-STK']['acc_title'];
				$data_vou_item[1]['customer_id']=0;
				$data_vou_item[1]['supplier_id']=0;
				$data_vou_item[1]['curr_code']=$currmas['code'];
				$data_vou_item[1]['curr_rate']=$currmas['rate'];
				$data_vou_item[1]['curr_dr']=get_round($amount_stk);
				$data_vou_item[1]['dr']=get_round($amount_stk);
				$data_vou_item[1]['curr_cr']=0;
				$data_vou_item[1]['cr']=0;
				$data_vou_item[1]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[1]['open_amount']=get_round($amount_stk);
				$data_vou_item[1]['refid']=0;
			}
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}			
			
			if($spmasid && $delid && $item_flg && $detail_flg && $vouid && $vou_item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/stkin_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
	}
	
	public function stkin_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		$ori_id = $_REQUEST['id'];
		$ori_mas = $model_mas->find($ori_id);
		$ori_item = $model_item->where('masid='.$ori_id)->select();
		
		if(!fiscalyp_check_common($ori_mas['fadate'],2))
		{
			$this->error('会计期间已关闭');
			exit;
		}
		
		$data['id']=$_REQUEST['id'];
		$data['status_flg']='P';
		$data['posttime']=time();
		$data['poster']=$_SESSION[C('USER_AUTH_KEY')];
		
		$spmasid=$model_mas->save($data);		
		
		$itmp = 0;
			$amount_stk=0;
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data_detail[$itmp]['fadate']=$ori_mas['fadate'];
					$data_detail[$itmp]['refcode']=$ori_mas['mas_code'];
					$data_detail[$itmp]['refid']=$ori_id;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['dr_qty']=$item['qty'];
					$data_detail[$itmp]['dr_amount']=$item['amount'];
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$data_detail[$itmp]['dr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_stk+=$data_detail[$itmp]['dr_amount'];
					
					$itmp += 1;
				}
			}
			
			//生成存货明细账
			$detail_flg = M('Stkdetail')->addAll($data_detail);
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=$ori_mas['fadate'];
			$data_vou_mas['refcode']=$ori_mas['mas_code'];
			$data_vou_mas['refid']=$ori_id;
			$data_vou_mas['vouno']=vouno_create($ori_mas['fadate']);
			$data_vou_mas['remark']='从'.$ori_mas['mas_code'].'#'.$ori_mas['mas_no'].'#'.$ori_mas['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where('code like \'STKIO-%\'')->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			if($facc['STKIO-FEE']['acc_code'])
			{
				$data_vou_item[0]['masid']=$vouid;
				$data_vou_item[0]['acc_code']=$facc['STKIO-FEE']['acc_code'];
				$data_vou_item[0]['acc_title']=$facc['STKIO-FEE']['acc_title'];
				$data_vou_item[0]['customer_id']=0;
				$data_vou_item[0]['supplier_id']=0;
				$data_vou_item[0]['curr_code']=$currmas['code'];
				$data_vou_item[0]['curr_rate']=$currmas['rate'];
				$data_vou_item[0]['curr_dr']=0;
				$data_vou_item[0]['dr']=0;
				$data_vou_item[0]['curr_cr']=get_round($amount_stk);
				$data_vou_item[0]['cr']=get_round($amount_stk);
				$data_vou_item[0]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[0]['open_amount']=get_round($amount_stk);
				$data_vou_item[0]['refid']=0;
			}
			if($facc['STKIO-STK']['acc_code'])
			{
				$data_vou_item[1]['masid']=$vouid;
				$data_vou_item[1]['acc_code']=$facc['STKIO-STK']['acc_code'];
				$data_vou_item[1]['acc_title']=$facc['STKIO-STK']['acc_title'];
				$data_vou_item[1]['customer_id']=0;
				$data_vou_item[1]['supplier_id']=0;
				$data_vou_item[1]['curr_code']=$currmas['code'];
				$data_vou_item[1]['curr_rate']=$currmas['rate'];
				$data_vou_item[1]['curr_dr']=get_round($amount_stk);
				$data_vou_item[1]['dr']=get_round($amount_stk);
				$data_vou_item[1]['curr_cr']=0;
				$data_vou_item[1]['cr']=0;
				$data_vou_item[1]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[1]['open_amount']=get_round($amount_stk);
				$data_vou_item[1]['refid']=0;
			}
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}
		
		if ($spmasid && $detail_flg && $vouid && $vou_item_flg)
		{
			$model_mas->commit();
			$this->assign("jumpUrl",U('Stkio/stkin_edit?id='.$_REQUEST['id']));
			$this->success("操作已完成!");				
		}
		else
		{
			$model_mas->rollback();
			$this->error("操作时发生错误!");
		}
    }
	
	
	/* 其他出库单 */
	
	public function stkout_list()
	{
	
		$s_status_flg = $_REQUEST['s_status_flg'];
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("StkiomasView");
		$strwhere="Stkiomas.mas_code = 'STKOUT'";
		if($s_status_flg){$strwhere.=" and Stkiomas.status_flg like '$s_status_flg'";}
		if($keywords){$strwhere.=" and (Stkiomas.mas_no like '%$keywords%' or Stkiomas.remark like '%$keywords%' or Iotype.title like '%$keywords%')";}
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&s_status_flg=".urlencode($s_status_flg);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('s_status_flg',$s_status_flg);
		$this->assign('keywords',$keywords);
		
		$this->display();
    }
	
	public function stkout_add()
	{		
		$whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate",date('Y-m-d'));
		$this->display();
    }
	
	public function stkout_add_save()
	{		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();	
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->mas_no = stkio_masno_create($_POST['mas_code'],strtotime($_POST['fadate']));
			$model_mas->createtime=time();
			$model_mas->creater=$_SESSION[C('USER_AUTH_KEY')];
			$newid=$model_mas->add(); //保存主数据
			
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			//保存行明细
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
		
			if($newid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/stkout_edit?id='.$newid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	
	public function stkout_edit()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("StkiomasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("StkioitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		$this->assign("fadate_write_off",date('Y-m-d'));
		
		$this->display();
    }
	
	public function stkout_edit_save()
	{		
		if(!$this->doc_check_status($_REQUEST['id'],"'A','E'"))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$spmasid = $_POST['id'];
			$model_mas->save();
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($spmasid && $delid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/stkout_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	public function stkout_save_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		if(!fiscalyp_check_common($_REQUEST['fadate'],2))
		{
			$this->error('会计期间已关闭'.$_REQUEST['fadate']);
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			//主记录
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->posttime=time();
			$model_mas->poster=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->status_flg='P';
			$spmasid=$_POST['id'];
			$model_mas->save();
			
			//明细记录
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			$amount_stk=0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
					
					$data_detail[$itmp]['fadate']=strtotime($_POST['fadate']);
					$data_detail[$itmp]['refcode']=$_POST['mas_code'];
					$data_detail[$itmp]['refid']=$spmasid;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['cr_qty']=$item['qty'];
					$data_detail[$itmp]['cr_amount']=$data[$itmp]['amount'];
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$data_detail[$itmp]['cr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_stk+=$data_detail[$itmp]['cr_amount'];
					
					$itmp += 1;
				}
			}
			$item_flg = $model_item->addAll($data);
			
			//生成存货明细账
			$detail_flg = deduct_stk($data_detail);
			if($detail_flg['flg']===false)
			{
				$model_mas->rollback();
				$this->error($detail_flg['msg']);
				exit;
			}
			$amount_stk = $detail_flg['amount_stk'];
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=strtotime($_POST['fadate']);
			$data_vou_mas['refcode']=$_POST['mas_code'];
			$data_vou_mas['refid']=$spmasid;
			$data_vou_mas['vouno']=vouno_create($data_vou_mas['fadate']);
			$data_vou_mas['remark']='从'.$_POST['mas_code'].'#'.$_POST['mas_no'].'#'.$_POST['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where('code like \'STKIO-%\'')->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			if($facc['STKIO-FEE']['acc_code'])
			{
				$data_vou_item[0]['masid']=$vouid;
				$data_vou_item[0]['acc_code']=$facc['STKIO-FEE']['acc_code'];
				$data_vou_item[0]['acc_title']=$facc['STKIO-FEE']['acc_title'];
				$data_vou_item[0]['customer_id']=0;
				$data_vou_item[0]['supplier_id']=0;
				$data_vou_item[0]['curr_code']=$currmas['code'];
				$data_vou_item[0]['curr_rate']=$currmas['rate'];
				$data_vou_item[0]['curr_dr']=get_round($amount_stk);
				$data_vou_item[0]['dr']=get_round($amount_stk);
				$data_vou_item[0]['curr_cr']=0;
				$data_vou_item[0]['cr']=0;
				$data_vou_item[0]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[0]['open_amount']=get_round($amount_stk);
				$data_vou_item[0]['refid']=0;
			}
			if($facc['STKIO-STK']['acc_code'])
			{
				$data_vou_item[1]['masid']=$vouid;
				$data_vou_item[1]['acc_code']=$facc['STKIO-STK']['acc_code'];
				$data_vou_item[1]['acc_title']=$facc['STKIO-STK']['acc_title'];
				$data_vou_item[1]['customer_id']=0;
				$data_vou_item[1]['supplier_id']=0;
				$data_vou_item[1]['curr_code']=$currmas['code'];
				$data_vou_item[1]['curr_rate']=$currmas['rate'];
				$data_vou_item[1]['curr_dr']=0;
				$data_vou_item[1]['dr']=0;
				$data_vou_item[1]['curr_cr']=get_round($amount_stk);
				$data_vou_item[1]['cr']=get_round($amount_stk);
				$data_vou_item[1]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[1]['open_amount']=get_round($amount_stk);
				$data_vou_item[1]['refid']=0;
			}
			
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}			
			
			if($spmasid && $delid && $item_flg && $detail_flg && $vouid && $vou_item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/stkout_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
	}
	
	public function stkout_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		$ori_id = $_REQUEST['id'];
		$ori_mas = $model_mas->find($ori_id);
		$ori_item = $model_item->where('masid='.$ori_id)->select();
		
		if(!fiscalyp_check_common($ori_mas['fadate'],2))
		{
			$this->error('会计期间已关闭');
			exit;
		}
		
		$data['id']=$_REQUEST['id'];
		$data['status_flg']='P';
		$data['posttime']=time();
		$data['poster']=$_SESSION[C('USER_AUTH_KEY')];
		
		$spmasid=$model_mas->save($data);		
		
		$itmp = 0;
			$amount_stk=0;
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data_detail[$itmp]['fadate']=$ori_mas['fadate'];
					$data_detail[$itmp]['refcode']=$ori_mas['mas_code'];
					$data_detail[$itmp]['refid']=$ori_id;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['cr_qty']=$item['qty'];
					$data_detail[$itmp]['cr_amount']=$item['amount'];
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$data_detail[$itmp]['cr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_stk+=$data_detail[$itmp]['cr_amount'];
					
					$itmp += 1;
				}
			}
			
			//生成存货明细账
			$detail_flg = deduct_stk($data_detail);
			if($detail_flg['flg']===false)
			{
				$model_mas->rollback();
				$this->error($detail_flg['msg']);
				exit;
			}
			$amount_stk = $detail_flg['amount_stk'];
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=$ori_mas['fadate'];
			$data_vou_mas['refcode']=$ori_mas['mas_code'];
			$data_vou_mas['refid']=$ori_id;
			$data_vou_mas['vouno']=vouno_create($ori_mas['fadate']);
			$data_vou_mas['remark']='从'.$ori_mas['mas_code'].'#'.$ori_mas['mas_no'].'#'.$ori_mas['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where('code like \'STKIO-%\'')->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			if($facc['STKIO-FEE']['acc_code'])
			{
				$data_vou_item[0]['masid']=$vouid;
				$data_vou_item[0]['acc_code']=$facc['STKIO-FEE']['acc_code'];
				$data_vou_item[0]['acc_title']=$facc['STKIO-FEE']['acc_title'];
				$data_vou_item[0]['customer_id']=0;
				$data_vou_item[0]['supplier_id']=0;
				$data_vou_item[0]['curr_code']=$currmas['code'];
				$data_vou_item[0]['curr_rate']=$currmas['rate'];
				$data_vou_item[0]['curr_dr']=get_round($amount_stk);
				$data_vou_item[0]['dr']=get_round($amount_stk);
				$data_vou_item[0]['curr_cr']=0;
				$data_vou_item[0]['cr']=0;
				$data_vou_item[0]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[0]['open_amount']=get_round($amount_stk);
				$data_vou_item[0]['refid']=0;
			}
			if($facc['STKIO-STK']['acc_code'])
			{
				$data_vou_item[1]['masid']=$vouid;
				$data_vou_item[1]['acc_code']=$facc['STKIO-STK']['acc_code'];
				$data_vou_item[1]['acc_title']=$facc['STKIO-STK']['acc_title'];
				$data_vou_item[1]['customer_id']=0;
				$data_vou_item[1]['supplier_id']=0;
				$data_vou_item[1]['curr_code']=$currmas['code'];
				$data_vou_item[1]['curr_rate']=$currmas['rate'];
				$data_vou_item[1]['curr_dr']=0;
				$data_vou_item[1]['dr']=0;
				$data_vou_item[1]['curr_cr']=get_round($amount_stk);
				$data_vou_item[1]['cr']=get_round($amount_stk);
				$data_vou_item[1]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[1]['open_amount']=get_round($amount_stk);
				$data_vou_item[1]['refid']=0;
			}

			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}
		
		if ($spmasid && $detail_flg && $vouid && $vou_item_flg)
		{
			$model_mas->commit();
			$this->assign("jumpUrl",U('Stkio/stkout_edit?id='.$_REQUEST['id']));
			$this->success("操作已完成!");				
		}
		else
		{
			$model_mas->rollback();
			$this->error("操作时发生错误!");
		}
    }
	
	/* 仓库调拨单 */
	
	public function whswitch_list()
	{
	
		$s_status_flg = $_REQUEST['s_status_flg'];
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("StkiomasView");
		$strwhere="Stkiomas.mas_code = 'WHSWITCH'";
		if($s_status_flg){$strwhere.=" and Stkiomas.status_flg like '$s_status_flg'";}
		if($keywords){$strwhere.=" and (Stkiomas.mas_no like '%$keywords%' or Stkiomas.remark like '%$keywords%' or Whmas2.title like '%$keywords%')";}	
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数
		$Page->parameter   .=   "&s_status_flg=".urlencode($s_status_flg);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('s_status_flg',$s_status_flg);
		$this->assign('keywords',$keywords);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	
	public function whswitch_add()
	{		
		$whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate",date('Y-m-d'));
		$this->display();
    }
	
	public function whswitch_add_save()
	{		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();	
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->mas_no = stkio_masno_create($_POST['mas_code'],strtotime($_POST['fadate']));
			$model_mas->createtime=time();
			$model_mas->creater=$_SESSION[C('USER_AUTH_KEY')];
			$newid=$model_mas->add(); //保存主数据
			
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			//保存行明细
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
		
			if($newid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/whswitch_edit?id='.$newid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	
	public function whswitch_edit()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("StkiomasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("StkioitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		$this->assign("fadate_write_off",date('Y-m-d'));
		
		$this->display();
    }
	
	public function whswitch_edit_save()
	{		
		if(!$this->doc_check_status($_REQUEST['id'],"'A','E'"))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$spmasid = $_POST['id'];
			$model_mas->save();
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($spmasid && $delid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/whswitch_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	public function whswitch_save_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		if(!fiscalyp_check_common($_REQUEST['fadate'],2))
		{
			$this->error('会计期间已关闭'.$_REQUEST['fadate']);
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			//主记录
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->posttime=time();
			$model_mas->poster=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->status_flg='P';
			$spmasid=$_POST['id'];
			$model_mas->save();
			
			//明细记录
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['amount']=(double)$item['qty']*(double)$item['price'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
					
					$data_detail[$itmp]['fadate']=strtotime($_POST['fadate']);
					$data_detail[$itmp]['refcode']=$_POST['mas_code'];
					$data_detail[$itmp]['refid']=$spmasid;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['cr_qty']=$item['qty'];
					$data_detail[$itmp]['cr_amount']=$data[$itmp]['amount'];
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$data_detail[$itmp]['cr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$itmp += 1;
				}
			}
			$item_flg = $model_item->addAll($data);
			
			//生成存货明细账（先出库）
			$detail_flg = deduct_stk($data_detail);
			if($detail_flg['flg']===false)
			{
				$model_mas->rollback();
				$this->error($detail_flg['msg']);
				exit;
			}
			
			//生成存货明细账（后入库）
			$itmp = 0;
			unset($data_detail);
			foreach($detail_flg['dtl'] as $item)
			{
					$data_detail[$itmp]['fadate']=C('WHSWITCH_USE_NEWDATE')=='Y'?strtotime($_POST['fadate']):$item['fadate'];
					$data_detail[$itmp]['refcode']=$_POST['mas_code'];
					$data_detail[$itmp]['refid']=$spmasid;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$_POST['wh2_id']; //入库到新的仓库
					$data_detail[$itmp]['dr_qty']=$item['qty'];
					$data_detail[$itmp]['dr_amount']=$item['amount'];
					$data_detail[$itmp]['cr_qty']=0;
					$data_detail[$itmp]['cr_amount']=0;
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$item['amount'];
					$data_detail[$itmp]['detail_id']=$item['detail_id'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$itmp += 1;
			}
			$detail_in = M('Stkdetail')->addAll($data_detail);
			
			
			if($spmasid && $delid && $item_flg && $detail_flg && $detail_in)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Stkio/whswitch_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
	}
	
	public function whswitch_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Stkiomas");
		$model_item = D("Stkioitem");
		$model_mas->startTrans();
		
		$ori_id = $_REQUEST['id'];
		$ori_mas = $model_mas->find($ori_id);
		$ori_item = $model_item->where('masid='.$ori_id)->select();
		
		if(!fiscalyp_check_common($ori_mas['fadate'],2))
		{
			$this->error('会计期间已关闭');
			exit;
		}
		
		$data['id']=$_REQUEST['id'];
		$data['status_flg']='P';
		$data['posttime']=time();
		$data['poster']=$_SESSION[C('USER_AUTH_KEY')];
		
		$spmasid=$model_mas->save($data);
		
		$itmp = 0;
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data_detail[$itmp]['fadate']=$ori_mas['fadate'];
					$data_detail[$itmp]['refcode']=$ori_mas['mas_code'];
					$data_detail[$itmp]['refid']=$ori_id;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['cr_qty']=$item['qty'];
					$data_detail[$itmp]['cr_amount']=$item['amount'];
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$data_detail[$itmp]['cr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$itmp += 1;
				}
			}
			
			//生成存货明细账
			$detail_flg = deduct_stk($data_detail);
			if($detail_flg['flg']===false)
			{
				$model_mas->rollback();
				$this->error($detail_flg['msg']);
				exit;
			}
			
			//生成存货明细账（后入库）
			$itmp = 0;
			unset($data_detail);			
			foreach($detail_flg['dtl'] as $item)
			{
					$data_detail[$itmp]['fadate']=C('WHSWITCH_USE_NEWDATE')=='Y'?$ori_mas['fadate']:$item['fadate'];
					$data_detail[$itmp]['refcode']=$ori_mas['mas_code'];
					$data_detail[$itmp]['refid']=$ori_mas['id'];
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$ori_mas['wh2_id']; //入库到新的仓库
					$data_detail[$itmp]['dr_qty']=$item['qty'];
					$data_detail[$itmp]['dr_amount']=$item['amount'];
					$data_detail[$itmp]['cr_qty']=0;
					$data_detail[$itmp]['cr_amount']=0;
					$data_detail[$itmp]['open_qty']=$item['qty'];
					$data_detail[$itmp]['open_amount']=$item['amount'];
					$data_detail[$itmp]['detail_id']=$item['detail_id'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$itmp += 1;
			}
			$detail_in = M('Stkdetail')->addAll($data_detail);			
			
		
		if ($spmasid && $detail_flg && $detail_in)
		{
			$model_mas->commit();
			$this->assign("jumpUrl",U('Stkio/whswitch_edit?id='.$_REQUEST['id']));
			$this->success("操作已完成!");				
		}
		else
		{
			$model_mas->rollback();
			$this->error("操作时发生错误!");
		}
    }
	
	//公用
	
	public function doc_check_status($spmasid,$doc_status="'A'")
	{
		$model_mas = M("Stkiomas");
		$where = "id in (".$spmasid.") and status_flg in (".$doc_status.")";
		$chk = $model_mas->where($where)->find();
		if($chk)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function doc_status_to_a()
	{
		if($this->doc_check_status($_GET["id"],"'P'"))
		{
			$this->error('单据已过账');
			exit;
		}
		
		$model = M("Stkiomas");
		//$this->assign("jumpUrl",U('Stkio/stkin_list'));
		$data = array('status_flg'=>'A');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function doc_status_to_c()
	{
		if($this->doc_check_status($_GET["id"],"'P'"))
		{
			$this->error('单据已过账');
			exit;
		}
		
		$model = M("Stkiomas");
		//$this->assign("jumpUrl",U('Stkio/stkin_list'));
		$data = array('status_flg'=>'C');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("取消成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function stkio_history()
	{
	
		$mas_code = $_REQUEST['mas_code'];
		$wh_title = $_REQUEST['wh_title'];
		$mas_fadate1 = $_REQUEST['mas_fadate1'];
		$mas_fadate2 = $_REQUEST['mas_fadate2'];
		$brand_title = $_REQUEST['brand_title'];
		$class_title = $_REQUEST['class_title'];
		$stk_title = $_REQUEST['stk_title'];
		
		if(!$mas_fadate1) $mas_fadate1 = date('Y-m-d');
		if(!$mas_fadate2) $mas_fadate2 = date('Y-m-d');
		
		$this->assign('mas_code',$mas_code);
		$this->assign('wh_title',$wh_title);		
		$this->assign('mas_fadate1',$mas_fadate1);
		$this->assign('mas_fadate2',$mas_fadate2);
		$this->assign('brand_title',$brand_title);
		$this->assign('class_title',$class_title);
		$this->assign('stk_title',$stk_title);
		

		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("StkdetailView");
		$strwhere="1=1";
		if($mas_code){$strwhere.=" and Stkdetail.refcode = '$mas_code'";}
		if($wh_title){$strwhere.=" and Whmas.title like '%$wh_title%'";}
		if($mas_fadate1){$strwhere.=" and PERIOD_DIFF(FROM_UNIXTIME(Stkdetail.fadate, '%Y%m%d'),'".date('Ymd',strtotime($mas_fadate1))."') >= 0";}
		if($mas_fadate2){$strwhere.=" and PERIOD_DIFF(FROM_UNIXTIME(Stkdetail.fadate, '%Y%m%d'),'".date('Ymd',strtotime($mas_fadate2))."') <= 0";}
		if($brand_title){$strwhere.=" and Stkbrand.classname like '%$brand_title%'";}
		if($class_title){$strwhere.=" and Stkclass.classname like '%$class_title%'";}
		if($stk_title){$strwhere.=" and (Stkmas.title like '%$stk_title%' or Stkmas.stk_c like '%$stk_title%')";}
		
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&mas_code=".urlencode($mas_code);   //传递查询条件
		$Page->parameter   .=   "&wh_title=".urlencode($wh_title);
		$Page->parameter   .=   "&mas_fadate1=".urlencode($mas_fadate1);
		$Page->parameter   .=   "&mas_fadate2=".urlencode($mas_fadate2);
		$Page->parameter   .=   "&brand_title=".urlencode($brand_title);
		$Page->parameter   .=   "&class_title=".urlencode($class_title);
		$Page->parameter   .=   "&stk_title=".urlencode($stk_title);
		
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("Stkdetail.fadate desc,Stkdetail.id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出

		$stk_total=$model->where($strwhere)->field('sum(dr_qty) as dr_qty,sum(dr_amount) as dr_amount,sum(cr_qty) as cr_qty,sum(cr_amount) as cr_amount')->find();
		$this->assign('stk_total',$stk_total);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	
    public function stkin_print()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("StkiomasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("StkioitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
        
        $ids='0';
        foreach($mydata_item as $item)
        {
            if($item['stk_id'])
            {
                $ids.=','.$item['stk_id'];
            }
        }
        
        $model_stk = M('Stkmas');
        $stk_info = $model_stk->field('id,spec,madein')->where('id in ('.$ids.')')->select();
        
        foreach($mydata_item as $k1=>$v1)
        {
            foreach($stk_info as $k2=>$v2)
            {
                if($v1['stk_id']==$v2['id'])
                {
                    $mydata_item[$k1]['stk_spec']=$v2['spec'];
                    $mydata_item[$k1]['stk_madein']=$v2['madein'];
                }
            }
        }
        
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
        
        //$whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		//$this->assign("whmas",$whmas);
		
		//$this->assign("fadate_write_off",date('Y-m-d'));
        
        $this->assign("web_title",C('WEB_TITLE'));
		
		$this->display();
    }
    
    public function stkout_print()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("StkiomasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("StkioitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		//$this->assign("fadate_write_off",date('Y-m-d'));
        
        $this->assign("web_title",C('WEB_TITLE'));
		
		$this->display();
    }





}
?>