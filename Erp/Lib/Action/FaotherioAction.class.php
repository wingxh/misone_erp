<?php

class FaotherioAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }	
	
	
	/* 数据处理 */
	
	public function data_list()
	{
		$mas_code = $_REQUEST['mas_code'];
		$keywords = $_REQUEST['keywords'];
		$status_flg = $_REQUEST['status_flg'];
		$fadate1 = $_REQUEST['fadate1'];
		$fadate2 = $_REQUEST['fadate2'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=M("Faotherio");
		$strwhere="(mas_code like '%$mas_code%' and comment like '%$keywords%')";
		if($status_flg) $strwhere.=" and status_flg='$status_flg'";
		if($fadate1) $strwhere.=" and fadate>=".strtotime($fadate1);
		if($fadate2) $strwhere.=" and fadate<=".strtotime($fadate2);
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$Page->parameter   .=   "&mas_code=".urlencode($mas_code);
		$Page->parameter   .=   "&status_flg=".urlencode($status_flg);
		$Page->parameter   .=   "&fadate1=".urlencode($fadate1);
		$Page->parameter   .=   "&fadate2=".urlencode($fadate2);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere)->order("fadate desc,id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		//var_dump($model->getLastSql());
		
		$this->assign('keywords',$keywords);
		$this->assign('mas_code',$mas_code);
		$this->assign('status_flg',$status_flg);
		$this->assign('fadate1',$fadate1);
		$this->assign('fadate2',$fadate2);
		
		$bank_list = M('Faccmas')->where('status_flg="Y" and diyinfo like \'%"bank";s:1:"Y"%\'')->getField('code,title',true);
		$this->assign('bank_list',$bank_list);
		
		$status_list = array();
		$status_list[] = array('code'=>'A');
		$status_list[] = array('code'=>'C');
		$status_list[] = array('code'=>'P');
		$status_list[] = array('code'=>'N');
		$this->assign('status_list',$status_list);
		
		$this->display();
    }
	
	public function data_add()
	{
		$this->assign('mas_code',$_REQUEST['mas_code']);
		
		$bank_list = M('Faccmas')->field('code,title')->where('status_flg="Y" and diyinfo like \'%"bank";s:1:"Y"%\'')->select();
		$this->assign('bank_list',$bank_list);
		
		//var_dump(M('Faccmas')->getLastSql());exit;
		
		$this->display();
    }
	public function data_add_save()
	{
		if(trim($_POST['mas_code'])=="")
		{
			$this->error("没有识别是收入还是支出");
			exit;
		}
		if(trim($_POST['comment'])=="")
		{
			$this->error("请描述这笔收支");
			exit;
		}
		
		if($_POST['fadate'])
		{
			$_POST['fadate'] = strtotime($_POST['fadate']);
		}
		else
		{
			$_POST['fadate'] = time();
		}
		
		$model = M("Faotherio");
		$this->assign("jumpUrl",U('Faotherio/data_list'));
		if ($model->create())
		{
			$model->created = time();
			$model->updated = time();
			$model->status_flg = "A";
			$dataid=$model->add();			
			
			$this->success("创建成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Faotherio");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$bank_list = M('Faccmas')->field('code,title')->where('status_flg="Y" and diyinfo like \'%"bank";s:1:"Y"%\'')->select();
		$this->assign('bank_list',$bank_list);
		
		$this->display();
    }
	public function data_edit_save()
	{
		if(trim($_POST['mas_code'])=="")
		{
			$this->error("没有识别是收入还是支出");
			exit;
		}
		if(trim($_POST['comment'])=="")
		{
			$this->error("请描述这笔收支");
			exit;
		}
		
		if($_POST['fadate'])
		{
			$_POST['fadate'] = strtotime($_POST['fadate']);
		}
		else
		{
			$_POST['fadate'] = time();
		}
		
		$model = M("Faotherio");
		
		$check_status = $model->where(array('id'=>$_POST['id']))->getField('status_flg');
		if($check_status=='P' || $check_status=='N')
		{
			unset($_POST['fadate']);
			unset($_POST['comment']);
			unset($_POST['status_flg']);
			unset($_POST['amount']);
			unset($_POST['bank']);
		}		
		
		$this->assign("jumpUrl",U('Faotherio/data_list'));
		if ($model->create())
		{
			$model->updated = time();
			$model->save(); // 保存数据
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function data_status_to_a()
	{
		$model = M("Faotherio");
		$this->assign("jumpUrl",U('Faotherio/data_list'));
		$data = array('status_flg'=>'A','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function data_status_to_c()
	{
		$model = M("Faotherio");
		$this->assign("jumpUrl",U('Faotherio/data_list'));
		$data = array('status_flg'=>'C','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function data_status_to_n()
	{
		$model = M("Faotherio");
		$this->assign("jumpUrl",U('Faotherio/data_list'));
		$data = array('status_flg'=>'N','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("存档成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function data_post()
	{
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = M("Faotherio");
		$model_mas->startTrans();
		
		$ori_id = $_REQUEST['id'];
		$ori_mas = $model_mas->find($ori_id);
		
		if(!fiscalyp_check_common($ori_mas['fadate']))
		{
			$this->error('会计期间已关闭');
			exit;
		}
		
		$data['id']=$_REQUEST['id'];
		$data['status_flg']='P';
		$data['updated']=time();
		
		$spmasid=$model_mas->save($data);		
					
		//生成总账
		$model_vou_mas = M('Voumas');
		$model_vou_item= M('Vouitem');
		
		$data_vou_mas['fadate']=$ori_mas['fadate'];
		$data_vou_mas['refcode']=$ori_mas['mas_code'];
		$data_vou_mas['refid']=$ori_id;
		$data_vou_mas['vouno']=vouno_create($ori_mas['fadate']);
		$data_vou_mas['remark']='从'.$ori_mas['mas_code'].'#'.$ori_mas['id'].'#'.$ori_mas['comment'];
		$data_vou_mas['createtime']=time();
		$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
		$data_vou_mas['posttime']=time();
		$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
		$data_vou_mas['status_flg']='P';
		$vouid=$model_vou_mas->data($data_vou_mas)->add();
		
		$faccdef = D('Faccdefault')->relation(true)->where("code in ('".$ori_mas['mas_code']."')")->select();
		foreach($faccdef as $faccrow)
		{
			$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
			$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
		}
		$facc_bank = M('Faccmas')->field('code,title')->where("code='".$ori_mas['bank']."'")->find();
							
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$itmp = 0;
		
		if($facc_bank && $facc[$ori_mas['mas_code']]['acc_code'])
		{
			
			
			if($ori_mas['mas_code'] == 'INCOME')
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc_bank['code'];
				$data_vou_item[$itmp]['acc_title']=$facc_bank['title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['dr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += get_round((double)$data_vou_item[$itmp]['curr_dr']);
				$total_dr      += get_round((double)$data_vou_item[$itmp]['dr']);
				$total_curr_cr += get_round((double)$data_vou_item[$itmp]['curr_cr']);
				$total_cr      += get_round((double)$data_vou_item[$itmp]['cr']);
			
				$itmp+=1;
				
				
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc[$ori_mas['mas_code']]['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc[$ori_mas['mas_code']]['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['cr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += get_round((double)$data_vou_item[$itmp]['curr_dr']);
				$total_dr      += get_round((double)$data_vou_item[$itmp]['dr']);
				$total_curr_cr += get_round((double)$data_vou_item[$itmp]['curr_cr']);
				$total_cr      += get_round((double)$data_vou_item[$itmp]['cr']);
			
				$itmp+=1;				
			}
			else if($ori_mas['mas_code'] == 'PAYOUT')
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc[$ori_mas['mas_code']]['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc[$ori_mas['mas_code']]['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['dr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += get_round((double)$data_vou_item[$itmp]['curr_dr']);
				$total_dr      += get_round((double)$data_vou_item[$itmp]['dr']);
				$total_curr_cr += get_round((double)$data_vou_item[$itmp]['curr_cr']);
				$total_cr      += get_round((double)$data_vou_item[$itmp]['cr']);
			
				$itmp+=1;
				
				
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc_bank['code'];
				$data_vou_item[$itmp]['acc_title']=$facc_bank['title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['cr']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['open_amount']=get_round($ori_mas['amount']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += get_round((double)$data_vou_item[$itmp]['curr_dr']);
				$total_dr      += get_round((double)$data_vou_item[$itmp]['dr']);
				$total_curr_cr += get_round((double)$data_vou_item[$itmp]['curr_cr']);
				$total_cr      += get_round((double)$data_vou_item[$itmp]['cr']);
			
				$itmp+=1;
			}
		}
		
		if($total_curr_dr != $total_curr_cr || $total_dr != $total_cr)
		{
			$model_mas->rollback();
			$this->error('借贷不平衡，请检查您的默认科目设置是否正确');
			exit;
		}
		if($data_vou_item)
		{
			$vou_item_flg=$model_vou_item->addAll($data_vou_item);
		}
		
		if ($spmasid && $vouid && $vou_item_flg)
		{
			$model_mas->commit();
			//$this->assign("jumpUrl",U('Faotherio/data_edit?id='.$_REQUEST['id']));
			$this->assign("jumpUrl",U('Faotherio/data_list'));
			$this->success("过账成功!");				
		}
		else
		{
			$model_mas->rollback();
			$this->error("操作时发生错误!".$spmasid ."--". $vouid ."--". $vou_item_flg);
		}
		
	}
	
	public function doc_check_status($spmasid,$doc_status="'A'")
	{
		$model_mas = M("Faotherio");
		$where = "id in (".$spmasid.") and status_flg in (".$doc_status.")";
		$chk = $model_mas->where($where)->find();
		if($chk)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
?>