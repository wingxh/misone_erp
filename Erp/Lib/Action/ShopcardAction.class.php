<?php

class ShopcardAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }	
	
	
	/* 数据处理 */
	
	public function data_list()
	{
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=M("ShoppingCard");
		$strwhere="status_flg in ('Y','N') and (card_c like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere)->order("open_amount desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('keywords',$keywords);
		
		$this->display();
    }
	
	public function data_add()
	{
		$this->display();
    }
	public function data_add_save()
	{
		if(trim($_POST['card_c'])=="")
		{
			$this->error("必须输入购物卡号码");
			exit;
		}
		
		$check_card_c = $this->check_card_c($_POST['card_c'],0);
		if($check_card_c)
		{
			$this->error("购物卡".$_POST['card_c']."已存在");
			exit;
		}
		
		$model = M("ShoppingCard");
		$this->assign("jumpUrl",U('Shopcard/data_list'));
		if ($model->create())
		{
			$model->created = time();
			$model->updated = time();
			$model->status_flg = "Y";
			$dataid=$model->add();
			
			if($_POST['open_amount'])
			{
				$card_log = array();
				$card_log['card_c'] = $_POST['card_c'];
				$card_log['act_amount'] = $_POST['open_amount'];
				$card_log['act_uid'] = (int)$_SESSION[C('USER_AUTH_KEY')];
				$card_log['remark'] = '后台设置初始金额';
				$card_log['created'] = time();
				$card_log['status_flg'] = 'Y';
				M('ShoppingCardLog')->data($card_log)->add();
			}
			
			$this->success("创建成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("ShoppingCard");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->display();
    }
	public function data_edit_save()
	{
		if(trim($_POST['card_c'])=="")
		{
			$this->error("必须输入购物卡号码");
			exit;
		}
		
		$check_card_c = $this->check_card_c($_POST['card_c'],$_POST['id']);
		if($check_card_c)
		{
			$this->error("购物卡".$_POST['card_c']."已存在");
			exit;
		}
		
		$model = M("ShoppingCard");
		$this->assign("jumpUrl",U('Shopcard/data_list'));
		if ($model->create())
		{
			$model->updated = time();
			$model->save(); // 保存数据
			
			if((int)$_POST['act_amount'])
			{
				$card_log = array();
				$card_log['card_c'] = $_POST['card_c'];
				$card_log['act_amount'] = (int)$_POST['act_amount'];
				$card_log['act_uid'] = (int)$_SESSION[C('USER_AUTH_KEY')];
				$card_log['remark'] = '后台调整金额~'.$_POST['act_remark'];
				$card_log['created'] = time();
				$card_log['status_flg'] = 'Y';
				M('ShoppingCardLog')->data($card_log)->add();
			}
			
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function data_status_to_y()
	{
		$model = M("ShoppingCard");
		$this->assign("jumpUrl",U('Shopcard/data_list'));
		$data = array('status_flg'=>'Y','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_n()
	{
		$model = M("ShoppingCard");
		$this->assign("jumpUrl",U('Shopcard/data_list'));
		$data = array('status_flg'=>'N','updated'=>time());
		$model->where("id in (".$_GET["id"].")")->save($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	private function check_card_c($card_c,$card_id)
	{
		$model = M("ShoppingCard");
		return $model->where(array('card_c'=>$card_c,'id'=>array('neq'=>(int)$card_id)))->find();
	}
	
	public function card_log_list()
	{
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=M("ShoppingCardLog");
		$strwhere="status_flg in ('Y') and (card_c like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere)->order("id asc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('keywords',$keywords);
		
		$this->display();
    }

	public function get_open_amount()
	{
		header("Content-Type:text/html; charset=utf-8");
        
        $model=M("ShoppingCard");
		
		$card_c = $_REQUEST['card_c'];
		if($card_c)
		{
            $strfield="id,open_amount,status_flg";	
            $strwhere="card_c = '$card_c'";
		
            $datalist = $model->field($strfield)->where($strwhere)->find();
            if($datalist)
			{
                if($datalist['status_flg']=='Y')
				{
					$result['msg']='ok';
					$result['data']=$datalist;
				}
				else
				{
					$result['msg']='购物卡已被禁用';
					$result['data']='';
				}
			}
			else
			{
				$result['msg']='购物卡不存在';
				$result['data']='';
			}
		}
        else
        {
				$result['msg']='参数错误';
				$result['data']='';
		}		
		//var_dump($result);echo $model->getLastSql();exit;
		$jsonencode = json_encode($result);
		echo $jsonencode;
    }

}
?>