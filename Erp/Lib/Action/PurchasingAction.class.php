<?php

class PurchasingAction extends CommonAction
{
    public function index()
	{
		$this->po_list();
    }
	
	/* 采购单 */
	
	public function po_list()
	{
	
		check_system();
		$s_status_flg = $_REQUEST['s_status_flg'];
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("SpmasView");
		$strwhere="Spmas.mas_code = 'PO'";
		if($s_status_flg){$strwhere.=" and Spmas.status_flg like '$s_status_flg'";}
		if($keywords){$strwhere.=" and (Spmas.mas_no like '%$keywords%' or Spmas.remark like '%$keywords%' or Supplier.title like '%$keywords%')";}	
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&s_status_flg=".urlencode($s_status_flg);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('s_status_flg',$s_status_flg);
		$this->assign('keywords',$keywords);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	
	public function po_add()
	{		
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$this->assign("currmas",$currmas);
		
		$whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate",date('Y-m-d'));
		$this->display();
    }
	
	public function po_add_save()
	{		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();	
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->mas_no = sp_masno_create($_POST['mas_code'],strtotime($_POST['fadate']));
			$model_mas->createtime=time();
			$model_mas->creater=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->manager=$_POST['manager']==''?$_SESSION[C('USER_AUTH_KEY')]:$_POST['manager'];
			$newid=$model_mas->add(); //保存主数据
			
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100;
					$data[$itmp]['amount2']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100*(1+(float)$item['tax_rate']/100);
					$data[$itmp]['qty_processed']=$item['qty'];//C('PO2WH_CTRL_FLG')=='Y'?$item['qty']:0;
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			//保存行明细
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
		
			if($newid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Purchasing/po_edit?id='.$newid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	
	public function po_edit()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("SpmasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
        
        //var_dump($model->getLastSql());exit;
		
		$model_item= D("SpitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
        
        $ids='0';
        foreach($mydata_item as $item)
        {
            if($item['stk_id'])
            {
                $ids.=','.$item['stk_id'];
            }
        }
        
        $model_stk = M('Stkmas');
        $stk_info = $model_stk->field('id,spec,madein')->where('id in ('.$ids.')')->select();
        
        foreach($mydata_item as $k1=>$v1)
        {
            foreach($stk_info as $k2=>$v2)
            {
                if($v1['stk_id']==$v2['id'])
                {
                    $mydata_item[$k1]['stk_spec']=$v2['spec'];
                    $mydata_item[$k1]['stk_madein']=$v2['madein'];
                }
            }
        }
        //var_dump($mydata_item);exit;
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$this->assign("currmas",$currmas);
        
        $whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate_write_off",date('Y-m-d'));
		
		$this->display();
    }
	
	public function po_edit_save()
	{		
		if(!$this->doc_check_status($_REQUEST['id'],"'A','E'"))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$spmasid = $_POST['id'];
			$model_mas->save();
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100;
					$data[$itmp]['amount2']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100*(1+(float)$item['tax_rate']/100);
					$data[$itmp]['qty_processed']=$item['qty'];//C('PO2WH_CTRL_FLG')=='Y'?$item['qty']:0;
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($spmasid && $delid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Purchasing/po_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	public function po_save_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		if(!fiscalyp_check_common($_REQUEST['fadate'],2))
		{
			$this->error('会计期间已关闭'.$_REQUEST['fadate']);
			exit;
		}
		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			//主记录
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->posttime=time();
			$model_mas->poster=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->status_flg='P';
			$model_mas->check_amount=(double)$_POST['check_amount'] < 0 ? 0 : (double)$_POST['check_amount'];
			$spmasid=$_POST['id'];
			$model_mas->save();
			
			//明细记录
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			$amount_ar =0;
			$amount_tax=0;
			$amount_stk=0;
			$amount_fee=0;
			$amount_check      = (double)$_POST['check_amount'] < 0 ? 0 : (double)$_POST['check_amount']; //本次付款金额
			$amount_check_over = 0; //付款溢出金额
			
			//是否为管控物料，如果不是，则不生成实际的库存数量和金额
			$stk_ids = 0;
			foreach($_POST['spitem'] as $item)
			{
				$stk_ids .= ','.$item['stk_id'];
			}
			$stk_flg1 = M('Stkmas')->field('id,flag1')->where('id in ('.$stk_ids.')')->select();
			if($stk_flg1)
			{
				foreach($stk_flg1 as $stk)
				{
					$stk_flag[$stk['stk_id']]['stk_id']=$stk['stk_id'];
					$stk_flag[$stk['stk_id']]['flag1']=$stk['flag1'];
				}
				unset($stk_flg1);
			}
			
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100;
					$data[$itmp]['amount2']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100*(1+(float)$item['tax_rate']/100);
					$data[$itmp]['qty_processed']=$item['qty'];//C('PO2WH_CTRL_FLG')=='Y'?$item['qty']:0;
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
					
					$data_detail[$itmp]['fadate']=strtotime($_POST['fadate']);
					$data_detail[$itmp]['refcode']=$_POST['mas_code'];
					$data_detail[$itmp]['refid']=$spmasid;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['dr_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['dr_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data[$itmp]['amount1']*(float)$_POST['curr_rate'];
					$data_detail[$itmp]['open_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['open_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data_detail[$itmp]['dr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_ar +=$data[$itmp]['amount2'];
					$amount_tax+=$data[$itmp]['amount1']*(float)$_POST['curr_rate']*(float)$item['tax_rate']/100;
					$amount_stk+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data[$itmp]['amount1']*(float)$_POST['curr_rate'];
					$amount_fee+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? $data[$itmp]['amount1']*(float)$_POST['curr_rate'] : 0;
					
					$itmp += 1;
				}
			}
			$item_flg = $model_item->addAll($data);
			
			//生成存货明细账
			$detail_flg = M('Stkdetail')->addAll($data_detail);
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=strtotime($_POST['fadate']);
			$data_vou_mas['refcode']=$_POST['mas_code'];
			$data_vou_mas['refid']=$spmasid;
			$data_vou_mas['vouno']=vouno_create($data_vou_mas['fadate']);
			$data_vou_mas['remark']='从'.$_POST['mas_code'].'#'.$_POST['mas_no'].'#'.$_POST['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where("code in ('SINV-AP','SINV-TAX','SINV-STK','SINV-FEE','BANK','PREPAID')")->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();			
			
			$itmp = 0;
			$amount_check_over = get_round($amount_ar) - get_round($amount_check); //如果为负数，表示有预付
			$total_curr_dr = 0;
			$total_dr      = 0;
			$total_curr_cr = 0;
			$total_cr      = 0;
			
			if($amount_check && $facc['BANK']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['BANK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['BANK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$_POST['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$_POST['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_check);
				$data_vou_item[$itmp]['cr']=get_round($amount_check*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
			
				$itmp+=1;
			}
			if($amount_check && $facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$_POST['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$_POST['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$_POST['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_check);
				$data_vou_item[$itmp]['dr']=get_round($amount_check*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1 : 0);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1*(float)$_POST['curr_rate'] : 0);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$_POST['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$_POST['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$_POST['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_ar);
				$data_vou_item[$itmp]['cr']=get_round($amount_ar*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-STK']['acc_code'] && $amount_stk)
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-STK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-STK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_stk);
				$data_vou_item[$itmp]['dr']=get_round($amount_stk);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-FEE']['acc_code'] && $amount_fee) //如果有非管控商品，则生成到费用类科目，而不是库存商品
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-FEE']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-FEE']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_fee);
				$data_vou_item[$itmp]['dr']=get_round($amount_fee);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-TAX']['acc_code'] && $amount_tax) //如果是0税，不生成税的分录
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-TAX']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-TAX']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_tax);
				$data_vou_item[$itmp]['dr']=get_round($amount_tax);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($total_curr_dr != $total_curr_cr || $total_dr != $total_cr)
			{
				$model_mas->rollback();
				$this->error('借贷不平衡，请检查您的默认科目设置是否正确');
				exit;
			}
			
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}			
			
			if($spmasid && $delid && $item_flg && $detail_flg && $vouid && $vou_item_flg)
			{
				$model_mas->commit();
                //bof update stk_mas(price)
                if( C('AUTO_UPDATE_PRICE')=='Y' )
                {
                    $model_stkmas = M("Stkmas");
                    foreach($data as $row)
                    {
                        unset($data_stkmas);
                        $data_stkmas['id']=$row['stk_id'];
                        $data_stkmas['price_pur']=$row['price'];
						$data_stkmas['updated']=time();
                        $model_stkmas -> save($data_stkmas);
                    }
                }
                //eof update stk_mas(price)
				$this->assign("jumpUrl",U('Purchasing/po_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
	}
	
	public function po_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();
		
		$ori_id = $_REQUEST['id'];
		$ori_mas = $model_mas->find($ori_id);
		$ori_item = $model_item->where('masid='.$ori_id)->select();
		
		if(!fiscalyp_check_common($ori_mas['fadate'],2))
		{
			$this->error('会计期间已关闭');
			exit;
		}
		
		$data['id']=$_REQUEST['id'];
		$data['status_flg']='P';
		$data['posttime']=time();
		$data['poster']=$_SESSION[C('USER_AUTH_KEY')];
		
		$spmasid=$model_mas->save($data);		
		
		$itmp = 0;
			$amount_ar =0;
			$amount_tax=0;
			$amount_stk=0;
			$amount_fee=0;
			$amount_check      = (double)$ori_mas['check_amount'] < 0 ? 0 : (double)$ori_mas['check_amount']; //本次付款金额
			$amount_check_over = 0; //付款溢出金额
			
			//是否为管控物料，如果不是，则不生成实际的库存数量和金额
			$stk_ids = 0;
			foreach($ori_item as $item)
			{
				$stk_ids .= ','.$item['stk_id'];
			}
			$stk_flg1 = M('Stkmas')->field('id,flag1')->where('id in ('.$stk_ids.')')->select();

			if($stk_flg1)
			{
				foreach($stk_flg1 as $stk)
				{
					$stk_flag[$stk['id']]['stk_id']=$stk['id'];
					$stk_flag[$stk['id']]['flag1']=$stk['flag1'];
				}
				unset($stk_flg1);
			}
			
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['stk_id']=$item['stk_id'];
                    $data[$itmp]['price']=$item['price'];
                    
                    $data_detail[$itmp]['fadate']=$ori_mas['fadate'];
					$data_detail[$itmp]['refcode']=$ori_mas['mas_code'];
					$data_detail[$itmp]['refid']=$ori_id;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['dr_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['dr_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : (double)$item['amount1']*(float)$ori_mas['curr_rate'];
					$data_detail[$itmp]['open_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['open_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data_detail[$itmp]['dr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_ar +=$item['amount2'];
					$amount_tax+=(double)$item['amount1']*(float)$ori_mas['curr_rate']*(float)$item['tax_rate']/100;
					$amount_stk+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : (double)$item['amount1']*(float)$ori_mas['curr_rate'];
					$amount_fee+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? (double)$item['amount1']*(float)$ori_mas['curr_rate'] : 0;
					
					$itmp += 1;
				}
			}
			
			//生成存货明细账
			$detail_flg = M('Stkdetail')->addAll($data_detail);
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=$ori_mas['fadate'];
			$data_vou_mas['refcode']=$ori_mas['mas_code'];
			$data_vou_mas['refid']=$ori_id;
			$data_vou_mas['vouno']=vouno_create($ori_mas['fadate']);
			$data_vou_mas['remark']='从'.$ori_mas['mas_code'].'#'.$ori_mas['mas_no'].'#'.$ori_mas['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where("code in ('SINV-AP','SINV-TAX','SINV-STK','SINV-FEE','BANK','PREPAID')")->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			//var_dump($facc);var_dump($facc['SINV-AP']['acc_code']);$model_mas->rollback();exit;			
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			$itmp=0;
			$amount_check_over = get_round($amount_ar) - get_round($amount_check); //如果为负数，表示有预付
			$total_curr_dr = 0;
			$total_dr      = 0;
			$total_curr_cr = 0;
			$total_cr      = 0;			
			
			if($amount_check && $facc['BANK']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['BANK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['BANK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$ori_mas['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$ori_mas['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_check);
				$data_vou_item[$itmp]['cr']=get_round($amount_check*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
			
				$itmp+=1;
			}
			if($amount_check && $facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$ori_mas['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$ori_mas['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$ori_mas['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_check);
				$data_vou_item[$itmp]['dr']=get_round($amount_check*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1 : 0);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1*(float)$ori_mas['curr_rate'] : 0);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$ori_mas['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$ori_mas['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$ori_mas['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_ar);
				$data_vou_item[$itmp]['cr']=get_round($amount_ar*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-STK']['acc_code'] && $amount_stk)
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-STK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-STK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_stk);
				$data_vou_item[$itmp]['dr']=get_round($amount_stk);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-FEE']['acc_code'] && $amount_fee) //如果有非管控商品，则生成到费用类科目，而不是库存商品
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-FEE']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-FEE']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_fee);
				$data_vou_item[$itmp]['dr']=get_round($amount_fee);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-TAX']['acc_code'] && $amount_tax) //如果是0税，不生成税的分录
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-TAX']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-TAX']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_tax);
				$data_vou_item[$itmp]['dr']=get_round($amount_tax);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			
			if($total_curr_dr != $total_curr_cr || $total_dr != $total_cr)
			{
				$model_mas->rollback();
				$this->error('借贷不平衡，请检查您的默认科目设置是否正确');
				exit;
			}
			
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}
		
		if ($spmasid && $detail_flg && $vouid && $vou_item_flg)
		{
			$model_mas->commit();
            //bof update stk_mas(price)
                if( C('AUTO_UPDATE_PRICE')=='Y' )
                {
                    $model_stkmas = M("Stkmas");
                    foreach($data as $row)
                    {
                        unset($data_stkmas);
                        $data_stkmas['id']=$row['stk_id'];
                        $data_stkmas['price_pur']=$row['price'];
						$data_stkmas['updated']=time();
                        $model_stkmas -> save($data_stkmas);
                    }
                }
            //eof update stk_mas(price)
			$this->assign("jumpUrl",U('Purchasing/po_edit?id='.$_REQUEST['id']));
			$this->success("操作已完成!");				
		}
		else
		{
			$model_mas->rollback();
			$this->error("操作时发生错误!");
		}
    }
	
	public function po_write_off()
	{			
		if(!$this->doc_check_status($_REQUEST['id'],"'P'"))
		{
			$this->error('单据状态要求为【已过账】');
			exit;
		}
		
		$model_mas = M("Spmas");
		$model_item = M("Spitem");
		$model_mas->startTrans();
		
		$ori_id = $_GET['id'];
		$ori_mas = $model_mas->find($ori_id);
		
		if ($ori_mas)
		{			
			$data_mas['fadate'] = strtotime($_GET['fadate']);
			$data_mas['mas_code'] = 'RNS';
			$data_mas['mas_no'] = sp_masno_create('RNS',strtotime($_GET['fadate']));
			$data_mas['acc_id'] = $ori_mas['acc_id'];
			$data_mas['curr_code'] = $ori_mas['curr_code'];
			$data_mas['curr_rate'] = $ori_mas['curr_rate'];
			$data_mas['remark'] = '从PO#'.$ori_mas['mas_no'].'#'.$ori_mas['remark'];
			$data_mas['createtime'] = time();
			$data_mas['creater'] = $_SESSION[C('USER_AUTH_KEY')];
			$data_mas['status_flg'] = 'A';
			
			//var_dump($data_mas);

			$newid = $model_mas->data($data_mas)->add();
			$ori_item = $model_item->where('masid='.$ori_id)->order('id asc')->select();
			$itmp = 0;
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=$item['amount1'];
					$data[$itmp]['amount2']=$item['amount2'];
					$data[$itmp]['qty_processed']=$item['qty'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date'];
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			
			//var_dump($data);
			
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($newid && $item_flg)
			{
				$model_mas->commit();
				//$this->assign("jumpUrl",U('Purchasing/rns_edit?id='.$newid));
				header("Content-Type:text/html; charset=utf-8");
				echo '<script type="text/javascript">parent.addTab("采购退货单-'.$newid.'","'.U('Purchasing/rns_edit?id='.$newid).'");</script>';
				$this->assign("jumpUrl",U('Purchasing/po_edit?id='.$ori_id));
				$this->success("操作已完成!");
			}
			else
			{
				//$this->assign("waitSecond",999);
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error('原始单据数据读取错误');
		}
    }
    
    public function po_print()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];
		
		M('Spmas')->where('id='.(int)$_GET["id"])->setInc('print_num');

        $model= D("SpmasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("SpitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$this->assign("currmas",$currmas);
		
		$this->assign("web_title",C('WEB_TITLE'));
		
		$this->display();
    }
	
	public function po_copy()
	{		
		$model_mas = M("Spmas");
		$model_item = M("Spitem");
		$model_mas->startTrans();
		
		$ori_id = $_GET['id'];
		$ori_mas = $model_mas->find($ori_id);
		
		if ($ori_mas)
		{			
			$data_mas['fadate'] = strtotime($_GET['fadate']);
			$data_mas['mas_code'] = 'PO';
			$data_mas['mas_no'] = sp_masno_create('PO',strtotime($_GET['fadate']));
			$data_mas['acc_id'] = $ori_mas['acc_id'];
			$data_mas['curr_code'] = $ori_mas['curr_code'];
			$data_mas['curr_rate'] = $ori_mas['curr_rate'];
			$data_mas['remark'] = '从PO#'.$ori_mas['mas_no'].'#'.$ori_mas['remark'];
			$data_mas['createtime'] = time();
			$data_mas['creater'] = $_SESSION[C('USER_AUTH_KEY')];
			$data_mas['status_flg'] = 'A';
			
			//var_dump($data_mas);

			$newid = $model_mas->data($data_mas)->add();
			$ori_item = $model_item->where('masid='.$ori_id)->order('id asc')->select();
			$itmp = 0;
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=$item['amount1'];
					$data[$itmp]['amount2']=$item['amount2'];
					$data[$itmp]['qty_processed']=$item['qty'];
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date'];
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			
			//var_dump($data);
			
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($newid && $item_flg)
			{
				$model_mas->commit();
				header("Content-Type:text/html; charset=utf-8");
				echo '<script type="text/javascript">parent.addTab("采购单-'.$newid.'","'.U('Purchasing/po_edit?id='.$newid).'");</script>';
				$this->assign("jumpUrl",U('Purchasing/po_edit?id='.$ori_id));
				//$this->assign("jumpUrl",U('Purchasing/po_list'));
				$this->success("操作已完成!");
			}
			else
			{
				//$this->assign("waitSecond",999);
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error('原始单据数据读取错误');
		}
    }
	
	
	/* 采购退货单 */
	
	public function rns_list()
	{
	
		$s_status_flg = $_REQUEST['s_status_flg'];
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("SpmasView");
		$strwhere="Spmas.mas_code = 'RNS'";
		if($s_status_flg){$strwhere.=" and Spmas.status_flg like '$s_status_flg'";}
		if($keywords){$strwhere.=" and (Spmas.mas_no like '%$keywords%' or Spmas.remark like '%$keywords%' or Supplier.title like '%$keywords%')";}
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&s_status_flg=".urlencode($s_status_flg);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('s_status_flg',$s_status_flg);
		$this->assign('keywords',$keywords);
		
		$this->display();
    }
	
	public function rns_add()
	{		
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$this->assign("currmas",$currmas);
		
		$whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate",date('Y-m-d'));
		$this->display();
    }
	
	public function rns_add_save()
	{		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();	
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->mas_no = sp_masno_create($_POST['mas_code'],strtotime($_POST['fadate']));
			$model_mas->createtime=time();
			$model_mas->creater=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->manager=$_POST['manager']==''?$_SESSION[C('USER_AUTH_KEY')]:$_POST['manager'];
			$newid=$model_mas->add(); //保存主数据
			
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$newid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100;
					$data[$itmp]['amount2']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100*(1+(float)$item['tax_rate']/100);
					$data[$itmp]['qty_processed']=$item['qty'];//C('PO2WH_CTRL_FLG')=='Y'?$item['qty']:0;
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			//保存行明细
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
		
			if($newid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Purchasing/rns_edit?id='.$newid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	
	public function rns_edit()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= D("SpmasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("SpitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$this->assign("currmas",$currmas);
        
        $whmas = M('Whmas')->where("orderid='0' and status_flg='Y'")->find();
		$this->assign("whmas",$whmas);
		
		$this->assign("fadate_write_off",date('Y-m-d'));
		
		$this->display();
    }
	
	public function rns_edit_save()
	{		
		if(!$this->doc_check_status($_REQUEST['id'],"'A','E'"))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			$model_mas->fadate=strtotime($_POST['fadate']);
			$spmasid = $_POST['id'];
			$model_mas->save();
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100;
					$data[$itmp]['amount2']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100*(1+(float)$item['tax_rate']/100);
					$data[$itmp]['qty_processed']=$item['qty'];//C('PO2WH_CTRL_FLG')=='Y'?$item['qty']:0;
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
                    
					$itmp += 1;
				}
			}
			$item_flg = true;
			if($data)
			{
				$item_flg = $model_item->addAll($data);
			}
			
			if($spmasid && $delid && $item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Purchasing/rns_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
    }
	public function rns_save_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		if(!fiscalyp_check_common($_REQUEST['fadate'],2))
		{
			$this->error('会计期间已关闭'.$_REQUEST['fadate']);
			exit;
		}
		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();
		
		if ($model_mas->create())
		{			
			//主记录
			$model_mas->fadate=strtotime($_POST['fadate']);
			$model_mas->posttime=time();
			$model_mas->poster=$_SESSION[C('USER_AUTH_KEY')];
			$model_mas->status_flg='P';
			$model_mas->check_amount=(double)$_POST['check_amount'] < 0 ? 0 : (double)$_POST['check_amount']; //本次退款金额
			$spmasid=$_POST['id'];
			$model_mas->save();
			
			//明细记录
			$delid=$model_item->where('masid='.$spmasid)->delete();
			$itmp = 0;
			$amount_ar =0;
			$amount_tax=0;
			$amount_stk=0;
			$amount_fee=0;
			$amount_check      = (double)$_POST['check_amount'] < 0 ? 0 : (double)$_POST['check_amount']; //本次退款金额
			$amount_check_over = 0; //付款溢出金额
			
			//是否为管控物料，如果不是，则不生成实际的库存数量和金额
			$stk_ids = 0;
			foreach($_POST['spitem'] as $item)
			{
				$stk_ids .= ','.$item['stk_id'];
			}
			$stk_flg1 = M('Stkmas')->field('id,flag1')->where('id in ('.$stk_ids.')')->select();

			if($stk_flg1)
			{
				foreach($stk_flg1 as $stk)
				{
					$stk_flag[$stk['id']]['stk_id']=$stk['id'];
					$stk_flag[$stk['id']]['flag1']=$stk['flag1'];
				}
				unset($stk_flg1);
			}
			
			foreach($_POST['spitem'] as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data[$itmp]['masid']=$spmasid;
					$data[$itmp]['stk_id']=$item['stk_id'];
					$data[$itmp]['wh_id']=$item['wh_id'];
					$data[$itmp]['qty']=$item['qty'];
					$data[$itmp]['price']=$item['price'];
					$data[$itmp]['tax_rate']=$item['tax_rate'];
					$data[$itmp]['disc_rate']=$item['disc_rate'];
					$data[$itmp]['amount1']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100;
					$data[$itmp]['amount2']=(double)$item['qty']*(double)$item['price']*(float)$item['disc_rate']/100*(1+(float)$item['tax_rate']/100);
					$data[$itmp]['qty_processed']=$item['qty'];//C('PO2WH_CTRL_FLG')=='Y'?$item['qty']:0;
					$data[$itmp]['status_flg']='Y';
					$data[$itmp]['remark']=$item['remark'];
                    
                    $data[$itmp]['batch_no']=$item['batch_no'];
                    $data[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data[$itmp]['approval_no']=$item['approval_no'];
                    $data[$itmp]['spec']=$item['spec'];
                    $data[$itmp]['madein']=$item['madein'];
					
					$data_detail[$itmp]['fadate']=strtotime($_POST['fadate']);
					$data_detail[$itmp]['refcode']=$_POST['mas_code'];
					$data_detail[$itmp]['refid']=$spmasid;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['cr_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['cr_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data[$itmp]['amount1']*(float)$_POST['curr_rate'];
					$data_detail[$itmp]['open_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['open_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data_detail[$itmp]['cr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date']==''?0:strtotime($item['expiry_date']);
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_ar +=$data[$itmp]['amount2'];
					$amount_tax+=(double)$data[$itmp]['amount1']*(float)$_POST['curr_rate']*(float)$item['tax_rate']/100;
					$amount_stk+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : (double)$data[$itmp]['amount1']*(float)$_POST['curr_rate'];
					$amount_fee+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? (double)$data[$itmp]['amount1']*(float)$_POST['curr_rate'] : 0;
					
					$itmp += 1;
				}
			}
			$item_flg = $model_item->addAll($data);
			
			//生成存货明细账
			$detail_flg = deduct_stk($data_detail);
			if($detail_flg['flg']===false)
			{
				$model_mas->rollback();
				$this->error($detail_flg['msg']);
				exit;
			}
			$amount_diff = (double)$detail_flg['amount_stk'] - $amount_stk; //退货成本差异
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=strtotime($_POST['fadate']);
			$data_vou_mas['refcode']=$_POST['mas_code'];
			$data_vou_mas['refid']=$spmasid;
			$data_vou_mas['vouno']=vouno_create($data_vou_mas['fadate']);
			$data_vou_mas['remark']='从'.$_POST['mas_code'].'#'.$_POST['mas_no'].'#'.$_POST['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where("code in ('SINV-AP','SINV-TAX','SINV-STK','SINV-FEE','BANK','PREPAID','SINV-DIFF')")->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			$itmp=0;
			$amount_check_over = get_round($amount_ar) - get_round($amount_check); //如果为负数，表示有多退
			$total_curr_dr = 0;
			$total_dr      = 0;
			$total_curr_cr = 0;
			$total_cr      = 0;	
			
			if($amount_check && $facc['BANK']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['BANK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['BANK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$_POST['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$_POST['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_check);
				$data_vou_item[$itmp]['dr']=get_round($amount_check*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
			
				$itmp+=1;
			}
			if($amount_check && $facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$_POST['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$_POST['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$_POST['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_check);
				$data_vou_item[$itmp]['cr']=get_round($amount_check*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1 : 0);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1*(float)$_POST['curr_rate'] : 0);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$_POST['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$_POST['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$_POST['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_ar);
				$data_vou_item[$itmp]['dr']=get_round($amount_ar*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($facc['SINV-STK']['acc_code'] && $amount_stk)
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-STK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-STK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_stk);
				$data_vou_item[$itmp]['cr']=get_round($amount_stk);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($facc['SINV-FEE']['acc_code'] && $amount_fee) //如果有非管控商品，则生成费用分录，而不是存货科目
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-FEE']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-FEE']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_fee);
				$data_vou_item[$itmp]['cr']=get_round($amount_fee);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($facc['SINV-TAX']['acc_code'] && $amount_tax) //如果是0税，不生成税的分录
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-TAX']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-TAX']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_tax);
				$data_vou_item[$itmp]['cr']=get_round($amount_tax);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($amount_diff && $facc['SINV-STK']['acc_code']) //如果有退货差异的话，生成分录
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-STK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-STK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['dr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['cr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($amount_diff && $facc['SINV-DIFF']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-DIFF']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-DIFF']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['dr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['cr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($total_curr_dr != $total_curr_cr || $total_dr != $total_cr)
			{
				$model_mas->rollback();
				$this->error('借贷不平衡，请检查您的默认科目设置是否正确');
				exit;
			}
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}			
			
			if($spmasid && $delid && $item_flg && $detail_flg && $vouid && $vou_item_flg)
			{
				$model_mas->commit();
				$this->assign("jumpUrl",U('Purchasing/rns_edit?id='.$spmasid));
				$this->success("操作已完成!");
			}
			else
			{
				$model_mas->rollback();
				$this->error('操作时发生错误。');
			}
						
		}
		else
		{
			$model_mas->rollback();
			$this->error($model_mas->getError());
		}
	}
	
	public function rns_post()
	{		
		if(!$this->doc_check_status($_REQUEST['id']))
		{
			$this->error('单据状态要求为【活动中】');
			exit;
		}
		
		$model_mas = D("Spmas");
		$model_item = D("Spitem");
		$model_mas->startTrans();
		
		$ori_id = $_REQUEST['id'];
		$ori_mas = $model_mas->find($ori_id);
		$ori_item = $model_item->where('masid='.$ori_id)->select();
		
		if(!fiscalyp_check_common($ori_mas['fadate'],2))
		{
			$this->error('会计期间已关闭');
			exit;
		}
		
		$data['id']=$_REQUEST['id'];
		$data['status_flg']='P';
		$data['posttime']=time();
		$data['poster']=$_SESSION[C('USER_AUTH_KEY')];
		
		$spmasid=$model_mas->save($data);		
		
		$itmp = 0;
			$amount_ar =0;
			$amount_tax=0;
			$amount_stk=0;
			$amount_fee=0;
			$amount_check      = (double)$ori_mas['check_amount'] < 0 ? 0 : (double)$ori_mas['check_amount']; //本次退款金额
			$amount_check_over = 0; //付款溢出金额
			
			//是否为管控物料，如果不是，则不生成实际的库存数量和金额
			$stk_ids = 0;
			foreach($ori_item as $item)
			{
				$stk_ids .= ','.$item['stk_id'];
			}
			$stk_flg1 = M('Stkmas')->field('id,flag1')->where('id in ('.$stk_ids.')')->select();

			if($stk_flg1)
			{
				foreach($stk_flg1 as $stk)
				{
					$stk_flag[$stk['id']]['stk_id']=$stk['id'];
					$stk_flag[$stk['id']]['flag1']=$stk['flag1'];
				}
				unset($stk_flg1);
			}
			
			foreach($ori_item as $item)
			{
				if($item['stk_id'] && $item['wh_id'] && $item['qty'])
				{
					$data_detail[$itmp]['fadate']=$ori_mas['fadate'];
					$data_detail[$itmp]['refcode']=$ori_mas['mas_code'];
					$data_detail[$itmp]['refid']=$ori_id;
					$data_detail[$itmp]['stk_id']=$item['stk_id'];
					$data_detail[$itmp]['wh_id']=$item['wh_id'];
					$data_detail[$itmp]['cr_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['cr_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : (double)$item['amount1']*(float)$ori_mas['curr_rate'];
					$data_detail[$itmp]['open_qty']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $item['qty'];
					$data_detail[$itmp]['open_amount']=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : $data_detail[$itmp]['cr_amount'];
                    
                    $data_detail[$itmp]['batch_no']=$item['batch_no'];
                    $data_detail[$itmp]['expiry_date']=$item['expiry_date'];
                    $data_detail[$itmp]['approval_no']=$item['approval_no'];
                    $data_detail[$itmp]['spec']=$item['spec'];
                    $data_detail[$itmp]['madein']=$item['madein'];
					
					$amount_ar +=$item['amount2'];
					$amount_tax+=(double)$item['amount1']*(float)$ori_mas['curr_rate']*(float)$item['tax_rate']/100;
					$amount_stk+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? 0 : (double)$item['amount1']*(float)$ori_mas['curr_rate'];
					$amount_fee+=$stk_flag[$item['stk_id']]['flag1']=='Y' ? (double)$item['amount1']*(float)$ori_mas['curr_rate'] : 0;
					
					$itmp += 1;
				}
			}
			
			//生成存货明细账
			$detail_flg = deduct_stk($data_detail);
			//var_dump($detail_flg);$model_mas->rollback();exit;
			if($detail_flg['flg']===false)
			{
				$model_mas->rollback();
				$this->error($detail_flg['msg']);
				exit;
			}
			$amount_diff = (double)$detail_flg['amount_stk'] - $amount_stk; //退货成本差异
			
			//生成总账
			$model_vou_mas = M('Voumas');
			$model_vou_item= M('Vouitem');
			
			$data_vou_mas['fadate']=$ori_mas['fadate'];
			$data_vou_mas['refcode']=$ori_mas['mas_code'];
			$data_vou_mas['refid']=$ori_id;
			$data_vou_mas['vouno']=vouno_create($ori_mas['fadate']);
			$data_vou_mas['remark']='从'.$ori_mas['mas_code'].'#'.$ori_mas['mas_no'].'#'.$ori_mas['remark'];
			$data_vou_mas['createtime']=time();
			$data_vou_mas['creater']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['posttime']=time();
			$data_vou_mas['poster']=$_SESSION[C('USER_AUTH_KEY')];
			$data_vou_mas['status_flg']='P';
			$vouid=$model_vou_mas->data($data_vou_mas)->add();
			
			$faccdef = D('Faccdefault')->relation(true)->where("code in ('SINV-AP','SINV-TAX','SINV-STK','SINV-FEE','BANK','PREPAID','SINV-DIFF')")->select();
			foreach($faccdef as $faccrow)
			{
				$facc[$faccrow['code']]['acc_code']=$faccrow['acc_code'];
				$facc[$faccrow['code']]['acc_title']=$faccrow['acc_title'];
			}
			//var_dump($facc);var_dump($facc['SINV-AP']['acc_code']);$model_mas->rollback();exit;			
			$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
			$itmp = 0;
			$amount_check_over = get_round($amount_ar) - get_round($amount_check); //如果为负数，表示有预付
			$total_curr_dr = 0;
			$total_dr      = 0;
			$total_curr_cr = 0;
			$total_cr      = 0;	
			if($amount_check && $facc['BANK']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['BANK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['BANK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$ori_mas['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$ori_mas['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_check);
				$data_vou_item[$itmp]['dr']=get_round($amount_check*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
			
				$itmp+=1;
			}
			if($amount_check && $facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$ori_mas['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$ori_mas['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$ori_mas['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_check);
				$data_vou_item[$itmp]['cr']=get_round($amount_check*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1 : 0);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? $amount_check_over*-1*(float)$ori_mas['curr_rate'] : 0);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp+=1;
			}
			if($facc['SINV-AP']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-AP']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-AP']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=$ori_mas['acc_id'];
				$data_vou_item[$itmp]['curr_code']=$ori_mas['curr_code'];
				$data_vou_item[$itmp]['curr_rate']=$ori_mas['curr_rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_ar);
				$data_vou_item[$itmp]['dr']=get_round($amount_ar*(float)$ori_mas['curr_rate']);
				$data_vou_item[$itmp]['curr_cr']=0;
				$data_vou_item[$itmp]['cr']=0;
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_check_over < 0 ? 0 : $amount_check_over*(float)$_POST['curr_rate']);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($facc['SINV-STK']['acc_code'] && $amount_stk)
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-STK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-STK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_stk);
				$data_vou_item[$itmp]['cr']=get_round($amount_stk);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_stk);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($facc['SINV-FEE']['acc_code'] && $amount_fee) //如果有非管控商品，则生成费用分录，而不是存货科目
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-FEE']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-FEE']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_fee);
				$data_vou_item[$itmp]['cr']=get_round($amount_fee);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_fee);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($facc['SINV-TAX']['acc_code'] && $amount_tax) //如果是0税，不生成税的分录
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-TAX']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-TAX']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=0;
				$data_vou_item[$itmp]['dr']=0;
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_tax);
				$data_vou_item[$itmp]['cr']=get_round($amount_tax);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_tax);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($amount_diff && $facc['SINV-STK']['acc_code']) //如果有退货差异的话，生成分录
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-STK']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-STK']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['dr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['cr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($amount_diff && $facc['SINV-DIFF']['acc_code'])
			{
				$data_vou_item[$itmp]['masid']=$vouid;
				$data_vou_item[$itmp]['acc_code']=$facc['SINV-DIFF']['acc_code'];
				$data_vou_item[$itmp]['acc_title']=$facc['SINV-DIFF']['acc_title'];
				$data_vou_item[$itmp]['customer_id']=0;
				$data_vou_item[$itmp]['supplier_id']=0;
				$data_vou_item[$itmp]['curr_code']=$currmas['code'];
				$data_vou_item[$itmp]['curr_rate']=$currmas['rate'];
				$data_vou_item[$itmp]['curr_dr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['dr']=get_round($amount_diff > 0 ? $amount_diff : 0);
				$data_vou_item[$itmp]['curr_cr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['cr']=get_round($amount_diff < 0 ? $amount_diff*-1 : 0);
				$data_vou_item[$itmp]['curr_open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['open_amount']=get_round($amount_diff);
				$data_vou_item[$itmp]['refid']=0;
				
				$total_curr_dr += (double)$data_vou_item[$itmp]['curr_dr'];
				$total_dr      += (double)$data_vou_item[$itmp]['dr'];
				$total_curr_cr += (double)$data_vou_item[$itmp]['curr_cr'];
				$total_cr      += (double)$data_vou_item[$itmp]['cr'];
				
				$itmp += 1;
			}
			if($total_curr_dr != $total_curr_cr || $total_dr != $total_cr)
			{
				$model_mas->rollback();
				$this->error('借贷不平衡，请检查您的默认科目设置是否正确');
				exit;
			}
			if($data_vou_item)
			{
				$vou_item_flg=$model_vou_item->addAll($data_vou_item);
			}
		
		if ($spmasid && $detail_flg && $vouid && $vou_item_flg)
		{
			$model_mas->commit();
			$this->assign("jumpUrl",U('Purchasing/rns_edit?id='.$_REQUEST['id']));
			$this->success("操作已完成!");				
		}
		else
		{
			$model_mas->rollback();
			$this->error("操作时发生错误!");
		}
    }
    
    public function rns_print()
	{		
		$map = array();
        $map["where"]["id"] = $_GET["id"];
		
		M('Spmas')->where('id='.(int)$_GET["id"])->setInc('print_num');

        $model= D("SpmasView");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$model_item= D("SpitemView");
        $mydata_item = $model_item->where("masid = ".$_GET["id"])->select();
		$this->assign("mydata_item",$mydata_item);

		$item_num = count($mydata_item);
		$this->assign("item_num",$item_num);
		
		$currmas = M('Currmas')->where("flag='Y' and status_flg='Y'")->find();
		$this->assign("currmas",$currmas);
		
		$this->assign("web_title",C('WEB_TITLE'));
		
		$this->display();
    }
	
	//公用
	
	public function doc_check_status($spmasid,$doc_status="'A'")
	{
		$model_mas = M("Spmas");
		$where = "id in (".$spmasid.") and status_flg in (".$doc_status.")";
		$chk = $model_mas->where($where)->find();
		if($chk)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function doc_status_to_a()
	{
		if($this->doc_check_status($_GET["id"],"'P'"))
		{
			$this->error('单据已过账');
			exit;
		}
		
		$model = M("Spmas");
		//$this->assign("jumpUrl",U('Purchasing/po_list'));
		$data = array('status_flg'=>'A');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function doc_status_to_c()
	{
		if($this->doc_check_status($_GET["id"],"'P'"))
		{
			$this->error('单据已过账');
			exit;
		}
		
		$model = M("Spmas");
		//$this->assign("jumpUrl",U('Purchasing/po_list'));
		$data = array('status_flg'=>'C');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("取消成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	public function pur_history()
	{
	
		$mas_code = $_REQUEST['mas_code'];
		$mas_no = $_REQUEST['mas_no'];
		$mas_fadate1 = $_REQUEST['mas_fadate1'];
		$mas_fadate2 = $_REQUEST['mas_fadate2'];
		$acc_title = $_REQUEST['acc_title'];
		$stk_title = $_REQUEST['stk_title'];
		$wh_title = $_REQUEST['wh_title'];
		
		//if(!$mas_code) $mas_code = 'PO';
		if(!$mas_fadate1) $mas_fadate1 = date('Y-m-d');
		if(!$mas_fadate2) $mas_fadate2 = date('Y-m-d');
		
		$this->assign('mas_code',$mas_code);
		$this->assign('mas_no',$mas_no);
		$this->assign('mas_fadate1',$mas_fadate1);
		$this->assign('mas_fadate2',$mas_fadate2);
		$this->assign('acc_title',$acc_title);
		$this->assign('stk_title',$stk_title);
		$this->assign('wh_title',$wh_title);

		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("SpdetailView");
		$strwhere="Spmas.mas_code in ('PO','RNS')";
		if($mas_code){$strwhere.=" and Spmas.mas_code = '$mas_code'";}
		if($mas_no){$strwhere.=" and Spmas.mas_no like '%$mas_no%'";}
		//if($mas_fadate1){$strwhere.=" and PERIOD_DIFF(FROM_UNIXTIME(Spmas.fadate, '%Y%m%d'),'".date('Ymd',strtotime($mas_fadate1))."') >= 0";}
		//if($mas_fadate2){$strwhere.=" and PERIOD_DIFF(FROM_UNIXTIME(Spmas.fadate, '%Y%m%d'),'".date('Ymd',strtotime($mas_fadate2))."') <= 0";}
        if($mas_fadate1){$strwhere.=" and Spmas.fadate >= ".strtotime($mas_fadate1)." ";}
		if($mas_fadate2){$strwhere.=" and Spmas.fadate <= ".strtotime($mas_fadate2)." ";}
		if($acc_title){$strwhere.=" and (Supplier.title like '%$acc_title%' or Supplier.stk_c like '%$acc_title%')";}
		if($stk_title){$strwhere.=" and (Stkmas.title like '%$stk_title%' or Stkmas.stk_c like '%$stk_title%')";}
		if($wh_title){$strwhere.=" and Whmas.title like '%$wh_title%'";}
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$Page->parameter   .=   "&mas_code=".urlencode($mas_code);   //传递查询条件
		$Page->parameter   .=   "&mas_no=".urlencode($mas_no);
		$Page->parameter   .=   "&mas_fadate1=".urlencode($mas_fadate1);
		$Page->parameter   .=   "&mas_fadate2=".urlencode($mas_fadate2);
		$Page->parameter   .=   "&acc_title=".urlencode($acc_title);
		$Page->parameter   .=   "&stk_title=".urlencode($stk_title);
		$Page->parameter   .=   "&wh_title=".urlencode($wh_title);
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("Spmas.fadate desc,Spmas.mas_no desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出

		$datasum = $model->where($strwhere) -> field("sum(Spitem.qty*case when Spmas.mas_code='RNS' then -1 else 1 end) as qty,sum(Spmas.curr_rate*Spitem.amount1*case when Spmas.mas_code='RNS' then -1 else 1 end) as amount1,sum(Spmas.curr_rate*Spitem.amount2*case when Spmas.mas_code='RNS' then -1 else 1 end) as amount2")->find();
		$this->assign("datasum",$datasum);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	
	public function pur_report()
	{
	
		$mas_fadate1 = $_POST['mas_fadate1'];
		$mas_fadate2 = $_POST['mas_fadate2'];
		if(!$mas_fadate1) $mas_fadate1 = date('Y-m').'-01';
		if(!$mas_fadate2) $mas_fadate2 = date('Y-m-d');
		
		$titles = array('供应商','仓库','商品分类','商品品牌','采购员');
		$fields = array('supplier_title','wh_title','class_title','brand_title','manager_title'); //因TP的ViewModel在【查询字段】里要用转换后的列名，
		$fields2 = array('Supplier.title','Whmas.title','Stkclass.classname','Stkbrand.classname','User3.username');//而【查询条件】里却要用原表名.原字段名
		for($itmp=0;$itmp<=4;$itmp++)
		{
			$filters[$itmp]['flg'] = $_POST['report'][$itmp]['flg']=='Y'?'Y':'N';
			$filters[$itmp]['keywords'] = $_POST['report'][$itmp]['keywords'];
			$filters[$itmp]['title'] = $titles[$itmp];
			$filters[$itmp]['field'] = $fields[$itmp];
			$filters[$itmp]['field2'] = $fields2[$itmp];
		}

		$this->assign('mas_fadate1',$mas_fadate1);
		$this->assign('mas_fadate2',$mas_fadate2);
		$this->assign('filters',$filters);
		$this->assign('col_title',$filters);
		$this->assign('col_value',$filters);
		
		$model=D("SpdetailView");
		$strwhere="Spmas.mas_code in ('PO','RNS')";
		//if($mas_fadate1){$strwhere.=" and PERIOD_DIFF(FROM_UNIXTIME(Spmas.fadate, '%Y%m%d'),'".date('Ymd',strtotime($mas_fadate1))."') >= 0";}
		//if($mas_fadate2){$strwhere.=" and PERIOD_DIFF(FROM_UNIXTIME(Spmas.fadate, '%Y%m%d'),'".date('Ymd',strtotime($mas_fadate2))."') <= 0";}
        if($mas_fadate1){$strwhere.=" and Spmas.fadate >= ".strtotime($mas_fadate1)." ";}
		if($mas_fadate2){$strwhere.=" and Spmas.fadate <= ".strtotime($mas_fadate2)." ";}
		$strfield="";
		foreach($filters as $row)
		{
			if($row['flg']=='Y')
			{
				if($row['keywords']) $strwhere.=" and ".$row['field2']." like '%".$row['keywords']."%'";
				$strfield.=$row['field'].",";
			}
		}
		$strgroup=substr($strfield,0,strlen($strfield)-1);
		$strfield=$strfield."sum(Spitem.qty*case when Spmas.mas_code='RNS' then -1 else 1 end) as qty,sum(Spmas.curr_rate*Spitem.amount1*case when Spmas.mas_code='RNS' then -1 else 1 end) as amount1,sum(Spmas.curr_rate*Spitem.amount2*case when Spmas.mas_code='RNS' then -1 else 1 end) as amount2";
		
		$datalist = $model->where($strwhere) ->field($strfield) -> group($strgroup)-> order($strgroup)->select();
		$this->assign("datalist",$datalist);

		$datasum = $model->where($strwhere) -> field("sum(Spitem.qty*case when Spmas.mas_code='RNS' then -1 else 1 end) as qty,sum(Spmas.curr_rate*Spitem.amount1*case when Spmas.mas_code='RNS' then -1 else 1 end) as amount1,sum(Spmas.curr_rate*Spitem.amount2*case when Spmas.mas_code='RNS' then -1 else 1 end) as amount2")->find();
		$this->assign("datasum",$datasum);
		
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	





}
?>