<?php

class MemberAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 数据处理 */
	
	public function data_list()
	{
		
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("Member");
		$strwhere="status_flg in ('Y','N') and (username like '%$keywords%' or cardnum like '%$keywords%' or telephone like '%$keywords%')";
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,(int)C('LIST_PAGESIZE')==''?'10':C('LIST_PAGESIZE')); // 实例化分页类传入总记录数和每页显示的记录数 
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->where($strwhere) -> order("cardnum desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('keywords',$keywords);
		
		$this->display();
    }

	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Member");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->display();
    }
	public function data_edit_save()
	{
		$model = M("Member");
		$this->assign("jumpUrl",U('Member/data_list'));
		if ($model->create())
		{			
			$model->save(); // 保存数据	
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del()
	{
		$model = M("Member");
		$this->assign("jumpUrl",U('Member/data_list'));
		$data = array('status_flg'=>'D');
		$model->where("id in (".$_GET["id"].")")->setField($data); // 仅标记为删除
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_todo()
	{
		$model = M("Member");
		$this->assign("jumpUrl",U('Member/data_list_rec'));
		$model->where("id in (".$_GET["id"].")")->delete();
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_undo()
	{
		$model = M("Member");
		$this->assign("jumpUrl",U('Member/data_list_rec'));
		$data = array('status_flg'=>'Y');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("还原成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_y()
	{
		$model = M("Member");
		$this->assign("jumpUrl",U('Member/data_list'));
		$data = array('status_flg'=>'Y');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_n()
	{
		//var_dump($_GET);exit;
		$model = M("Member");
		$this->assign("jumpUrl",U('Member/data_list'));
		$data = array('status_flg'=>'N');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }


}
?>