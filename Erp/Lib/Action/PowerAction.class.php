<?php

class PowerAction extends CommonAction
{
    public function index()
	{
		$this->node_list();
    }
	
	/* node数据处理 */
	
	public function node_list()
	{
		$datalist = M("Node")->field("id,name,title,status,remark,sort,pid,level,concat(remark,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['remark']));
		}
		$this->assign("datalist",$datalist);
		//C('SHOW_PAGE_TRACE',true);
		$this->display();
    }
	public function node_add()
	{		
		$cid=$_GET['cid'];
		if(!$cid) $cid=0;
		
		$this->get_node_list();
		$this->assign("cid",$cid);
		$orderid=M("Node")->getField("max(sort)+1");
		$this->assign("orderid",$orderid==null?1:$orderid);
		$this->display();
    }
	public function node_add_save()
	{
		$model = D("Node");
		$this->assign("jumpUrl",U('Power/node_list'));
		if ($model->create())
		{			
			$lv= '0';
			if($_POST['pid'])
			{
				$lv=M("Node")->where('id='.$_POST['pid'])->getField("level");
			}
			//var_dump($lv);exit;
			$model->level=(int)$lv+1;
			$nodeid=$model->add();
			
			$data = array('remark'=>$this->get_parentstr($nodeid,$_POST['pid']));
			$model->where("id=$nodeid")->setField($data);
			//var_dump($nodeid);exit;
			$this->success("创建成功!");			
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function node_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Node");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		
		$this->get_node_list();
		
		$this->display();
    }
	public function node_edit_save()
	{
		$model = D("Node");
		$this->assign("jumpUrl",U('Power/node_list'));
		if ($model->create())
		{			
			$lv= '0';
			if($_POST['pid'])
			{
				$lv=M("Node")->where('id='.$_POST['pid'])->getField("level");
			}
			//var_dump($lv);exit;
			$model->level=(int)$lv+1;
			$model->save(); // 保存数据
			if($_POST['status']=="0")
			{
				//当禁用类别时，自动禁用下级所有类别
				$data = array('status'=>'0');
				$model->where("pid=".$_POST['id']." or remark like '%,".$_POST['id'].",%'")->setField($data);
			}
			$this->set_parentstr($_POST['id'],$_POST['pid']);
			$this->success("保存成功!");
		}
		else
		{
			// 创建数据对象失败
			$this->error($model->getError());
		}
    }
	public function node_del()
	{
		$model = M("Node");
		$this->assign("jumpUrl",U('Power/node_list'));
		
		$data = array('pid'=>0,'remark'=>',0,'); //将下级类别的父类设置为0
		foreach (explode(',',$_GET["id"]) as $s)
		{
			if($s)
			{
				$model->where("pid=$s or remark like '%,$s,%'")->setField($data);
			}
		}		
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			M("Access")->where("node_id in (".$_GET["id"].")")->delete();
			$this->success("删除成功: 此类别的所有下级类别已转为顶级（未归类）!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	

	
	public function get_parentstr($cid,$pid=0)
	{
		if($pid==0)
		{
			return ',0,';
		}
		else
		{
			return M("Node")->where("id=$pid")->getField("remark").$pid.",";
		}
	}
	
	public function set_parentstr($cid,$pid=0,$pstr='')
	{
		
		//获取当前parentstr
		if($pstr == '')
		{
			$parstr = $this->get_parentstr($cid,$pid);
		}
		else
		{
			$parstr = $pstr.$pid.',';
		}
		
		//更新类别表parentstr
		M("Node")->where("id=$cid")->setField("remark",$parstr);
			
		$arrNav = M("Node")->where("pid=$cid")->select();
		foreach($arrNav as $row)
		{
			//传递下级参数,继续更新
			$this->set_parentstr($row['id'], $row['pid'], $parstr);
		}

	}
	
	public function get_node_list()
	{
		$datalist = M("Node")->field("id,pid,remark,name,title,status,concat(remark,'',id) as bpath") ->where("status='1'") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['remark']));
		}
		$this->assign("datalist",$datalist);
	}
	
	
	
	
	/* role(group)数据处理 */
	
	public function role_list()
	{
		$datalist = M("Role")->order("name asc")->select();
		$this->assign("datalist",$datalist);
		$this->display();
    }
	public function role_add()
	{		
		$this->display();
    }
	public function role_add_save()
	{
		$model = D("Role");
		$this->assign("jumpUrl",U('Power/role_list'));
		if ($model->create())
		{			

			$nodeid=$model->add();
			$this->success("创建成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function role_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Role");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->display();
    }
	public function role_edit_save()
	{
		$model = D("Role");
		$this->assign("jumpUrl",U('Power/role_list'));
		if ($model->create())
		{
			$model->save();
			$this->success("保存成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function role_del()
	{
		$model = M("Role");
		$this->assign("jumpUrl",U('Power/role_list'));
		
		$model->where("id in (".$_GET["id"].")")->delete(); // 删除
		if ($model)
		{
			M("Role_user")->where("rule_id in (".$_GET["id"].")")->delete();
			M("Access")->where("rule_id in (".$_GET["id"].")")->delete();
			$this->success("删除成功");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	
	/* access 数据处理 */
	
	public function access()
	{
		$role_id = $_GET['id'];
		if(!$role_id)
		{
			$this->error('没有选择要设置权限的用户组');
			exit;
		}
		$role = M('role')->find($role_id);
		if(!$role)
		{
			$this->error('用户组信息读取失败');
			exit;
		}
		$this->assign("role",$role);
		
		$datalist = M("Node")->field("id,name,title,status,remark,sort,pid,level,concat(remark,'',id) as bpath") -> order("bpath asc")->select();
		foreach($datalist as $key=>$value)
		{
			$datalist[$key]['count'] = count(explode(',',$value['remark']));
		}
		$this->assign("datalist",$datalist);
		
		$access = M('access')->field('node_id')->where('role_id='.$role_id)->select();
		
		$access_str = '0';
		foreach($access as $acs)
		{
			$access_str .= ','.implode(',',$acs);
		}
		$access_str .= ',0';
		$this->assign("access",$access_str);
		$this->display();
    }
	
	public function access_save()
	{
		$role_id = $_POST['role_id'];
		$chk_list = $_POST['chk_list'];
		if($role_id && $chk_list)
		{			
			$node = M('Node')->getField('id,name,title,pid,level');  //->where('id in('.explode(',',$chk_list).')')
			$itmp = 0;
			foreach($chk_list as $chk)
			{
				$data[$itmp]['role_id'] = $role_id;
				$data[$itmp]['node_id'] = $chk;
				$data[$itmp]['module'] = '';
				$data[$itmp]['level'] = $node[$chk]['level'];
				$data[$itmp]['pid'] = $node[$chk]['pid'];
				$itmp += 1;
			}
			$model = M('Access');
			$model->startTrans();
		
			$result1 = $model->where('role_id='.$role_id)->delete();
			$result2 = $model ->addAll($data);
			if($result2)
			{
				$model->commit();
				$this->success('操作成功');
			}
			else
			{
				$model->rollback();
				$this->error('操作失败');
			}
		}
		else
		{
			$this->error('没有用户组名，或是权限列表值');
		}
	}
	
	public function access_clear()
	{
		$role_id = $_GET['role_id'];
		$this->assign("jumpUrl",U('Power/role_list'));
		if($role_id)
		{
			$model = M('Access');
			$model->where('role_id='.$role_id)->delete();
			$this->success('操作成功');
		}
		else
		{			
			$this->error('没有用户组名');
		}
	}
	
	
	/* role_user 数据处理 */
	
	public function role_user()
	{
		$role_id = $_GET['id'];
		if(!$role_id)
		{
			$this->error('没有选择要设置权限的用户组');
			exit;
		}
		$role = M('role')->find($role_id);
		if(!$role)
		{
			$this->error('用户组信息读取失败');
			exit;
		}
		$this->assign("role",$role);
		
		$datalist = M("User")->select();
		$this->assign("datalist",$datalist);
		
		$access = M('Role_user')->field('user_id')->where('role_id='.$role_id)->select();
		
		$access_str = '0';
		foreach($access as $acs)
		{
			$access_str .= ','.implode(',',$acs);
		}
		$access_str .= ',0';
		$this->assign("access",$access_str);
		$this->display();
    }
	
	public function role_user_save()
	{
		$role_id = $_POST['role_id'];
		$chk_list = $_POST['chk_list'];
		if($role_id && $chk_list)
		{			
			$itmp = 0;
			foreach($chk_list as $chk)
			{
				$data[$itmp]['role_id'] = $role_id;
				$data[$itmp]['user_id'] = $chk;
				$itmp += 1;
			}
			$model = M('Role_user');
			$model->startTrans();
		
			$result1 = $model->where('role_id='.$role_id)->delete();
			$result2 = $model ->addAll($data);
			if($result2)
			{
				$model->commit();
				$this->success('操作成功');
			}
			else
			{
				$model->rollback();
				$this->error('操作失败');
			}
		}
		else
		{
			$this->error('没有用户组名，或是没有选择用户');
		}
	}
	
	public function user_clear()
	{
		$role_id = $_GET['role_id'];
		$this->assign("jumpUrl",U('Power/role_list'));
		if($role_id)
		{
			$model = M('Role_user');
			$model->where('role_id='.$role_id)->delete();
			$this->success('操作成功');
		}
		else
		{			
			$this->error('没有用户组名');
		}
	}
	
	public function test()
	{
				
	}
	

	


}
?>