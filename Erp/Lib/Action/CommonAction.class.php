<?php

class  CommonAction  extends  Action
{

    function  _initialize()
	{		
		$web_config_diy = S('web_config_diy');
        if(!$web_config_diy)
		{
            $config_list = M("Webconfig") -> order("orderid asc")->select();
			$web_config_diy = array();
			foreach($config_list as $row)
			{					
				if($row['vartype'] == 'number')
				{
					$web_config_diy[trim($row['varname'])] = (double)$row['varvalue'];
				}
				else
				{
					$web_config_diy[trim($row['varname'])] = (string)$row['varvalue'];
				}
			}
			S('web_config_diy',$web_config_diy);
        }
        C($web_config_diy); //添加配置
		
		//var_dump(MODULE_NAME);
		if (C ( 'USER_AUTH_ON' ) && !in_array(MODULE_NAME,explode(',',C('NOT_AUTH_MODULE')))) 
		{
			import ( 'ORG.Util.RBAC' );       //载入rabc类
			if (! RBAC::AccessDecision ()) 
			{     //获取用户项 权限访问信息  
			
				if (! $_SESSION [C ( 'USER_AUTH_KEY' )]) 
				{	//检查认证识别号  读取session 如果不存在
					
					//redirect ( PHP_FILE . C ( 'USER_AUTH_GATEWAY' ) );//跳转到认证网关
					echo '<script type="text/javascript">top.window.location.href="'.PHP_FILE . C ( 'USER_AUTH_GATEWAY' ).'";</script>';
					exit;
				}
				// 没有权限 抛出错误
				if (C ( 'RBAC_ERROR_PAGE' )) 
				{
					// 定义权限错误页面
					//redirect ( C ( 'RBAC_ERROR_PAGE' ) );
					echo '<script type="text/javascript">window.location.href="'.PHP_FILE . C ( 'RBAC_ERROR_PAGE' ).'";</script>';					
					exit;
				} 
				else 
				{
					if (C ( 'GUEST_AUTH_ON' )) {
						//$this->assign ( 'jumpUrl', PHP_FILE . C ( 'USER_AUTH_GATEWAY' ) );
						echo '<script type="text/javascript">top.window.location.href="'.PHP_FILE . C ( 'USER_AUTH_GATEWAY' ).'";</script>';
						exit;
					}
					// 提示错误信息
					$this->error ('权限不足！');
				}
			}
		}
		$this->assign("username",$_SESSION[C('USER_AUTH_TIT')]);
	
	
	
	}

}



?>