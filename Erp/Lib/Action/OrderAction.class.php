<?php

class OrderAction extends CommonAction
{
    public function index()
	{
		$this->data_list();
    }
	
	/* 数据处理 */
	
	public function data_list()
	{
		$stat = $_REQUEST['stat'];
		$keywords = $_REQUEST['keywords'];
		import("ORG.Util.Page"); // 导入分页类
		
		$model=D("Order");
		$strwhere="(status_flg in ('0','1','2') and (order_id like '%$keywords%' or remark like '%$keywords%'))";
		if($stat!=''){$strwhere.=" and status_flg in ('$stat')";}
		
		$count      = $model->where($strwhere)->count(); // 查询满足要求的总记录数
		$Page       = new Page($count,10); // 实例化分页类传入总记录数和每页显示的记录数

		$Page->parameter   .=   "&stat=".urlencode($stat);   //传递查询条件
		$Page->parameter   .=   "&keywords=".urlencode($keywords);
		
		$show       = $Page->show(); // 分页显示输出
		
		$datalist = $model->relation(true)->where($strwhere) -> order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign("datalist",$datalist);
		$this->assign('page',$show); // 赋值分页输出
		
		$this->assign('keywords',$keywords);
		$this->assign('stat',$stat);
		
		$this->display();
    }

	public function data_edit()
	{
		$map = array();
        $map["where"]["id"] = $_GET["id"];

        $model= M("Order");
        $mydata = $model->find($map);
		$this->assign("mydata",$mydata);
		$this->display();
    }
	public function data_edit_save()
	{
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list'));
		if ($model->create())
		{
			$model->save(); // 保存数据	
			$this->success("保存成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del()
	{
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list'));
		$data = array('status_flg'=>'9');
		$model->where("id in (".$_GET["id"].")")->setField($data); // 仅标记为删除
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_todo()
	{
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list_rec'));
		$model->where("id in (".$_GET["id"].")")->delete();
		if ($model)
		{
			$this->success("删除成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_del_undo()
	{
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list_rec'));
		$data = array('status_flg'=>'1');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("还原成功!");
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_y()
	{
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list'));
		$data = array('status_flg'=>'1');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_y2()
	{
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list'));
		$data = array('status_flg'=>'2');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("启用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }
	public function data_status_to_n()
	{
		//var_dump($_GET);exit;
		$model = M("Order");
		$this->assign("jumpUrl",U('Order/data_list'));
		$data = array('status_flg'=>'0');
		$model->where("id in (".$_GET["id"].")")->setField($data);
		if ($model)
		{
			$this->success("禁用成功!");			
		}
		else
		{
			$this->error($model->getError());
		}
    }


}
?>