<?php
class SpdetailViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Spitem'=>array('*'),
		'Spmas'=>array('fadate'=>'mas_fadate','mas_code'=>'mas_code','mas_no'=>'mas_no','curr_code'=>'curr_code','curr_rate'=>'curr_rate','remark'=>'mas_remark',
						'_on'=>'Spmas.id=Spitem.masid AND Spmas.status_flg in(\'P\',\'N\')',
						),
		'Stkmas'=>array('stk_c'=>'stk_code','title'=>'stk_title','uom'=>'stk_uom', '_on'=>'Spitem.stk_id=Stkmas.id'),
		'Whmas'=>array('title'=>'wh_title', '_on'=>'Spitem.wh_id=Whmas.id','_type'=>'LEFT'),
		'Stkbrand'=>array('classname'=>'brand_title', '_on'=>'Stkmas.brandid=Stkbrand.id','_type'=>'LEFT'),
		'Stkclass'=>array('classname'=>'class_title', '_on'=>'Stkmas.classid=Stkclass.id','_type'=>'LEFT'),
		'Supplier'=>array('stk_c'=>'supplier_code','title'=>'supplier_title', '_on'=>'Spmas.acc_id=Supplier.id AND Spmas.mas_code in(\'PO\',\'RNS\')','_type'=>'LEFT'),
		'Customer'=>array('stk_c'=>'customer_code','title'=>'customer_title', '_on'=>'Spmas.acc_id=Customer.id AND Spmas.mas_code in(\'SO\',\'RNC\')','_type'=>'LEFT'),
		'User'=>array('_as'=>'User1','username'=>'creater_username', '_on'=>'Spmas.creater=User1.id','_type'=>'LEFT'),
		'User2'=>array('_table'=>'misone_user','_as'=>'User2','username'=>'poster_username', '_on'=>'Spmas.poster=User2.id','_type'=>'LEFT'),
		'User3'=>array('_table'=>'misone_user','_as'=>'User3','username'=>'manager_title', '_on'=>'Spmas.manager=User3.id'),
	);
}
?>