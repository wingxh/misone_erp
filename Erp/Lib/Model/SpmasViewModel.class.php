<?php
class SpmasViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Spmas'=>array('*','_type'=>'LEFT'),
		'Supplier'=>array('title'=>'supplier_title', '_on'=>'Spmas.acc_id=Supplier.id AND Spmas.mas_code in(\'PO\',\'RNS\')','_type'=>'LEFT'),
		'Customer'=>array('title'=>'customer_title', '_on'=>'Spmas.acc_id=Customer.id AND Spmas.mas_code in(\'SO\',\'RNC\')','_type'=>'LEFT'),
		'User'=>array('_as'=>'User1','username'=>'creater_username', '_on'=>'Spmas.creater=User1.id','_type'=>'LEFT'),
		'User2'=>array('_table'=>'misone_user','_as'=>'User2','username'=>'poster_username', '_on'=>'Spmas.poster=User2.id','_type'=>'LEFT'),
		'User3'=>array('_table'=>'misone_user','_as'=>'User3','username'=>'manager_title', '_on'=>'Spmas.manager=User3.id'),
	);
}
?>