<?php
class MemberModel extends RelationModel
{
	// 自动验证设置
	protected $_validate = array
	( 
		array('username','require','名称必填!',1),
        array('cardnum','require','卡号必填!',1),
		array('cardnum','','卡号已存在',0,'unique',1),
	);



	protected $_link = array(
		array(  
			'mapping_type'=>BELONGS_TO,
			'classname'=>'Memberclass',
			'foreign_key'=>'classid',
			'mapping_name'=>'memberclass',
			'as_fields'=>'classname:classname',
			// 定义更多的关联属性
		),
   );
	
}
?>