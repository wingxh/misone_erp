<?php
class StkiomasViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Stkiomas'=>array('*','_type'=>'LEFT'),
		'Iotype'=>array('title'=>'iotype_title', '_on'=>'Stkiomas.iotype_id=Iotype.id','_type'=>'LEFT'),
		'Whmas'=>array('_as'=>'Whmas1','title'=>'wh1_title', '_on'=>'Stkiomas.wh1_id=Whmas1.id','_type'=>'LEFT'),
		'Whmas2'=>array('_table'=>'misone_whmas','_as'=>'Whmas2','title'=>'wh2_title', '_on'=>'Stkiomas.wh2_id=Whmas2.id','_type'=>'LEFT'),
		'User'=>array('_as'=>'User1','username'=>'creater_username', '_on'=>'Stkiomas.creater=User1.id','_type'=>'LEFT'),
		'User2'=>array('_table'=>'misone_user','_as'=>'User2','username'=>'poster_username', '_on'=>'Stkiomas.poster=User2.id'),
	);
}
?>