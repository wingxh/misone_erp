<?php
class VoudetailViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Voumas'=>array('fadate'=>'mas_fadate','refcode'=>'mas_refcode','refid'=>'mas_refid','vouno'=>'vou_no'),
		'Vouitem'=>array('*','_type'=>'LEFT','_on'=>'Voumas.id=Vouitem.masid and Voumas.status_flg=\'P\''),
		'Faccmas'=>array('title'=>'faccmas_title','diyinfo'=>'faccmas_diyinfo', '_on'=>'Faccmas.code=Vouitem.acc_code','_type'=>'LEFT'),
		'Customer'=>array('title'=>'customer_title', '_on'=>'Vouitem.customer_id=Customer.id','_type'=>'LEFT'),
		'Supplier'=>array('title'=>'supplier_title', '_on'=>'Vouitem.supplier_id=Supplier.id'),
	);
}
?>