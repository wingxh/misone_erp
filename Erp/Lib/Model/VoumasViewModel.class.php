<?php
class VoumasViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Voumas'=>array('*','_type'=>'LEFT'),
		'User'=>array('_as'=>'User1','username'=>'creater_username', '_on'=>'Voumas.creater=User1.id','_type'=>'LEFT'),
		'User2'=>array('_table'=>'misone_user','_as'=>'User2','username'=>'poster_username', '_on'=>'Voumas.poster=User2.id'),
	);
}
?>