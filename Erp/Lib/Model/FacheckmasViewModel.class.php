<?php
class FacheckmasViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Facheckmas'=>array('*','_type'=>'LEFT'),
		'Supplier'=>array('title'=>'supplier_title', '_on'=>'Facheckmas.acc_id=Supplier.id AND Facheckmas.mas_code in(\'APCHKOUT\')','_type'=>'LEFT'),
		'Customer'=>array('title'=>'customer_title', '_on'=>'Facheckmas.acc_id=Customer.id AND Facheckmas.mas_code in(\'ARCHKIN\')','_type'=>'LEFT'),
		'User'=>array('_as'=>'User1','username'=>'creater_username', '_on'=>'Facheckmas.creater=User1.id','_type'=>'LEFT'),
		'User2'=>array('_table'=>'misone_user','_as'=>'User2','username'=>'poster_username', '_on'=>'Facheckmas.poster=User2.id','_type'=>'LEFT'),
		'User3'=>array('_table'=>'misone_user','_as'=>'User3','username'=>'manager_title', '_on'=>'Facheckmas.manager=User3.id'),
	);
}
?>