<?php
class StkioitemViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Stkioitem'=>array('*'), //,'_type'=>'LEFT'
		'Stkmas'=>array('stk_c'=>'stk_code','title'=>'stk_title','uom'=>'stk_uom', '_on'=>'Stkioitem.stk_id=Stkmas.id'),
		'Whmas'=>array('title'=>'wh_title', '_on'=>'Stkioitem.wh_id=Whmas.id'),
	);
}
?>