<?php
class VouitemViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Vouitem'=>array('*'),
		'Voumas'=>array('fadate'=>'mas_fadate','remark'=>'mas_remark', '_on'=>'Vouitem.masid=Voumas.id','_type'=>'LEFT'),
		'Customer'=>array('title'=>'customer_title', '_on'=>'Vouitem.customer_id=Customer.id','_type'=>'LEFT'),
		'Supplier'=>array('title'=>'supplier_title', '_on'=>'Vouitem.supplier_id=Supplier.id'),
	);
}
?>