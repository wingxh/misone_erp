<?php
class CustomerModel extends RelationModel
{
	// 自动验证设置
	protected $_validate = array
	( 
		array('title','require','名称必填!',1),
		array('title','','名称已存在',0,'unique',1), 
		array('classid','number','所属类别,必须是数字!',2),
		array('orderid','number','排序号,必须是数字!',2),
	);

	protected $_link = array(
		array(  
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'Customerclass',
			'foreign_key'=>'classid',
			'mapping_name'=>'customerclass',
			'as_fields'=>'classname,orderid:brandorderid',
			// 定义更多的关联属性
		),
   );
	
}
?>