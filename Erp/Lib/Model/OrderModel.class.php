<?php
class OrderModel extends RelationModel
{

	protected $_link = array(
		array(  
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'Member',
			'foreign_key'=>'user_id',
			'mapping_name'=>'memberorder',
			'as_fields'=>'username,truename',
			// 定义更多的关联属性
		),
   );
	
}
?>