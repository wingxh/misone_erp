<?php
class StkmasViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Stkmas'=>array('*','_type'=>'LEFT'),
		'Stkdetail'=>array('open_qty'=>'open_qty','open_amount'=>'open_amount', '_on'=>'Stkmas.id=Stkdetail.stk_id and Stkdetail.open_qty>0','_type'=>'LEFT'),
		'Whmas'=>array('title'=>'wh_title', '_on'=>'Stkdetail.wh_id=Whmas.id'),
	);
}
?>