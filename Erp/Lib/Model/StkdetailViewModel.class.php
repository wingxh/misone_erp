<?php
class StkdetailViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Stkdetail'=>array('*'),
		'Stkmas'=>array('stk_c'=>'stk_code','title'=>'stk_title','uom'=>'stk_uom','price_sales'=>'stk_price_sales', '_on'=>'Stkdetail.stk_id=Stkmas.id'),
		'Whmas'=>array('title'=>'wh_title', '_on'=>'Stkdetail.wh_id=Whmas.id','_type'=>'LEFT'),
		'Stkbrand'=>array('classname'=>'brand_title', '_on'=>'Stkmas.brandid=Stkbrand.id','_type'=>'LEFT'),
		'Stkclass'=>array('classname'=>'class_title', '_on'=>'Stkmas.classid=Stkclass.id','_type'=>'LEFT'),
	);
}
?>