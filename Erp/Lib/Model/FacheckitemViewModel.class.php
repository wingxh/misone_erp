<?php
class FacheckitemViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Facheckitem'=>array('*'),
		'Facheckmas'=>array('fadate'=>'mas_fadate', '_on'=>'Facheckmas.id=Facheckitem.masid','_type'=>'LEFT'),
		'Customer'=>array('title'=>'customer_title', '_on'=>'Facheckitem.customer_id=Customer.id','_type'=>'LEFT'),
		'Supplier'=>array('title'=>'supplier_title', '_on'=>'Facheckitem.supplier_id=Supplier.id'),
	);
}
?>