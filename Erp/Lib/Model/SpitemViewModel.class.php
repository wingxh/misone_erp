<?php
class SpitemViewModel extends ViewModel
{
	
	public $viewFields = array(
		'Spitem'=>array('*'), //,'_type'=>'LEFT'
		'Stkmas'=>array('stk_c'=>'stk_code','title'=>'stk_title','uom'=>'stk_uom', '_on'=>'Spitem.stk_id=Stkmas.id'),
		'Whmas'=>array('title'=>'wh_title', '_on'=>'Spitem.wh_id=Whmas.id'),
	);
}
?>