<?php
class FaccdefaultModel extends RelationModel
{
	// 自动验证设置
	protected $_validate = array
	( 
		array('code','require','代码必填!',1),
		array('code','','代码已存在',0,'unique',1), 
		array('facc_id','number','会计科目ID,必须是数字!',2),
		array('orderid','number','排序号,必须是数字!',2),
	);

	protected $_link = array(
		array(  
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'Faccmas',
			'foreign_key'=>'facc_id',
			'mapping_name'=>'faccmas',
			'as_fields'=>'code:acc_code,title:acc_title',
			// 定义更多的关联属性
		),
   );

	
}
?>