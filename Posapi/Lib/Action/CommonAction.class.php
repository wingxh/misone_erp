<?php

class  CommonAction  extends  Action
{

    function  _initialize()
	{
		$web_config_diy = S('web_config_diy');
        if(!$web_config_diy)
		{
            $config_list = M("Webconfig") -> order("orderid asc")->select();
			$web_config_diy = array();
			foreach($config_list as $row)
			{					
				if($row['vartype'] == 'number')
				{
					$web_config_diy[trim($row['varname'])] = (double)$row['varvalue'];
				}
				else
				{
					$web_config_diy[trim($row['varname'])] = (string)$row['varvalue'];
				}
			}
			S('web_config_diy',$web_config_diy);
        }
        C($web_config_diy); //添加配置
	}
	//返回服务器处理结果
	public function ret($code,$msg,$data = null)
	{
		$_ret['status'] = $code;
		$_ret['msg'] = $msg;
		$_ret['data'] = $data;
		
		if($code != 1)
		{
			//file_put_contents("debug.txt",date('Y-m-d H:i:s')."--------".serialize($_ret)."----------\r\n",FILE_APPEND);
		}
		
		echo(json_encode($_ret));
		exit;
	}
	
	public function object2array($object)
	{
		$_ret = json_decode( json_decode(json_encode( $object )),true );
		return  $_ret;
    }
	
	//按KV结构，解析POS客户端上传的参数
	public function getObjData()
	{		
		$param = $_POST;
		$objectData = $param['paramaters'];
		$validate = $param['validate'];
		$sign = $param['sign'];
		
		$objectData = str_replace(PHP_EOL, '', $objectData);
		
		//file_put_contents("debug.txt",date('Y-m-d H:i:s')."--------paramaters=".urlencode($objectData).C('POS_API_KEY')."----------\r\n",FILE_APPEND);
		
		//if(strtolower(md5(strtolower(urlencode($objectData)).C('POS_API_KEY'))) != strtolower($validate))
		if(strtolower(md5(strtolower(md5($sign)).C('POS_API_KEY'))) != strtolower($validate))
		{
			//file_put_contents("debug.txt",date('Y-m-d H:i:s')."--------数据验证失败，非法数据！----".strtolower(md5($objectData.C('POS_API_KEY'))).'***'.strtolower($validate)."----------\r\n",FILE_APPEND);
			$this->ret('0','数据验证失败，非法数据！');
		}
		
		$arr = $this->object2array($objectData);
		$_ret = array();
		foreach($arr as $v)
		{
			$_ret[$v['K']] = $v['V'];
		}
		return $_ret;
	}

}



?>