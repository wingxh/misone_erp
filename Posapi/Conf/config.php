<?php
if (!defined('THINK_PATH')) exit();
//载入网站配置
$web_config	=	require 'web_config.php';

//载入网站配置
$web_config_diy	=	require 'web_config_diy.php';

//载入数据库配置
$db_config = require 'db_config.php';

//RBAC
$array	=	array(
	'POS_API_KEY'=>'MISONE-POS',
);

//return $array;

//合并输出配置
return array_merge($db_config,$web_config,$web_config_diy,$array);
?>