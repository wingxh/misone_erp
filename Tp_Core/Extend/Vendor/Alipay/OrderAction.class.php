<?php
class OrderAction extends Action {
	public function _initialize() {
		Vendor('Alipay.Corefunction');
		Vendor('Alipay.Notify');
		Vendor('Alipay.Service');
		Vendor('Alipay.Submit');
		Vendor('Alipay.Alipayconfig');
	}

	function alipayto() {
		C('TOKEN_ON',false);
		$aliapy_config = alicofings();
		$out_trade_no = 'SO' . date('Ymdhis', time()) . round(0, 100);

		$subject = $_POST['subject'];

		$body = $_POST['body'];

		$total_fee = $_POST['total_fee'];

		$paymethod = '';

		$defaultbank = '';

		$anti_phishing_key = '';

		$exter_invoke_ip = '';

		$show_url = 'http://www.xxxxxx.com';

		$extra_common_param = '';

		$royalty_type = "";
		$royalty_parameters = "";

		$parameter = array (
			"service" => "create_direct_pay_by_user",
			"payment_type" => "1",

			"partner" => trim($aliapy_config['partner']), 
			"_input_charset" => trim(strtolower($aliapy_config['input_charset'])), 
			"seller_email" => trim($aliapy_config['seller_email']), 
			"return_url" => trim($aliapy_config['return_url']), 
			"notify_url" => trim($aliapy_config['notify_url']), 
			"out_trade_no" => $out_trade_no, 
			"subject" => $subject, 
			"body" => $body, 
			"total_fee" => $total_fee, 
			"paymethod" => $paymethod, 
			"defaultbank" => $defaultbank, 
			"anti_phishing_key" => $anti_phishing_key, 
			"exter_invoke_ip" => $exter_invoke_ip, 
			"show_url" => $show_url, 
			"extra_common_param" => $extra_common_param, 
			"royalty_type" => $royalty_type, 
			"royalty_parameters" => $royalty_parameters);
		$d = M('Order');
		$d->create();
		$d->user_id = $_SESSION[C('USER_AUTH_KEY')];
		$d->total_fee = $total_fee;
		$d->order_id = $out_trade_no;
		$d->create_time=time();
		$d->status_flg = '0';
		$d->remark = $subject.'--'.$body;
		if (false !== $d->add()) {

			$alipayService = new AlipayService($aliapy_config);
			$html_text = $alipayService->create_direct_pay_by_user($parameter);
			$this->assign('alipay', $html_text);
			$this->assign('total_fee', $total_fee);
			$this->display();

		} else {
			$this->error('系统错误暂时不能充值，请联系在线客服!');

		}

	}
	public function returnurl() {
		$aliapy_config = alicofings();
		$alipayNotify = new AlipayNotify($aliapy_config);
		$verify_result = $alipayNotify->verifyReturn();
		if ($verify_result) { //验证成功

			$out_trade_no = $_GET['out_trade_no']; //获取订单号
			$trade_no = $_GET['trade_no']; //获取支付宝交易号
			$total_fee = $_GET['total_fee']; //获取总价格
			$d = M('Order');
			$result = $d->where("order_id='.$trade_no.'")->find();
			$status = $result['status_flg'];

			if ($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {

				if ($status == '0') {
					$d->create();
					$d->id = $result['id'];
					$d->status_flg = '1';
					$d->save();
				}

			} else {
				//echo "trade_status=" . $_GET['trade_status'];
				$this->assign('msg','充值失败!');
			}

			echo "验证成功<br />";
			echo "trade_no=" . $trade_no;

		} else {


			$this->assign('msg','验证失败');
		}

		$this->display();

	}
	public function notifyurl() {
		$aliapy_config = alicofings();
		$alipayNotify = new AlipayNotify($aliapy_config);
		$verify_result = $alipayNotify->verifyNotify();

		if ($verify_result) { //验证成功

			$out_trade_no = $_POST['out_trade_no']; //获取订单号
			$trade_no = $_POST['trade_no']; //获取支付宝交易号
			$total_fee = $_POST['total_fee']; //获取总价格

			if ($_POST['trade_status'] == 'TRADE_FINISHED' || $_POST['trade_status'] == 'TRADE_SUCCESS') { //交易成功结束

				echo "success"; //请不要修改或删除
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				$d = M('Order');
				$result = $d->where("order_id='.$trade_no.'")->find();
				$status = $result['status_flg'];
				if ($status == '0') {
					$d->create();
					$d->id = $result['id'];
					$d->status_flg = '1';
					$d->save();
				}
			} else {
				echo "success"; //其他状态判断。普通即时到帐中，其他状态不用判断，直接打印success。
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				$d = M('Order');
				$result = $d->where("order_id='.$trade_no.'")->find();
				$status = $result['status_flg'];
				if ($status == '0') {
					$d->create();
					$d->id = $result['id'];
					$d->status_flg = '1';
					$d->save();
				}
			}

			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} else {
			//验证失败
			echo "fail";

			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		}

	}
}
?>