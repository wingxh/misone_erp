<?php

class Myinitdb
{
	/*
	$dbinfo array{'dbhost'=>'','dbuser'=>'','dbpw'=>'','dbname'=>'','tablepre'=>'','dbinitfile'=>'','dbinitadminsql'=>''}
	*/
	function todo($dbinfo)
	{

		$conn = @mysql_connect($dbinfo['dbhost'], $dbinfo['dbuser'], $dbinfo['dbpw']);
		//var_dump($conn);
		//var_dump($dbinfo);
		if($conn)
		{
			if(mysql_get_server_info() < '4.0')
			{
				$outinfo['error']=true;
				$outinfo['errorMsg']='检测到您的数据库版本过低，请更新！';
				return $outinfo;exit;
			}

			$res = mysql_query('show Databases', $conn);

			//遍历所有数据库，存入数组
			while(@$row = mysql_fetch_array($res))
			{
				$dbname_arr[] = $row['Database'];
			}

			//检查数据库是否存在，没有则创建数据库
			if(@!in_array(trim($dbinfo['dbname']), $dbname_arr))
			{
				if(!@mysql_query("CREATE DATABASE `".$dbinfo['dbname']."`", $conn))
				{
					$outinfo['error']=true;
					$outinfo['errorMsg']='创建数据库失败，请检查权限或联系管理员！';
					return $outinfo;exit;
				}
			}
			mysql_select_db($dbinfo['dbname'], $conn);		


			//设置数据库状态
			mysql_query("SET NAMES 'utf8', character_set_client=binary, sql_mode='', interactive_timeout=3600;", $conn);


			//创建表结构
			$tbdata = '';
			$tbdata = $this->Readf($dbinfo['dbinitfile']);
			//var_dump($tbdata);
			
			$querys = explode(';', $tbdata);
			foreach($querys as $q)
			{
				if(trim($q) == '') continue;
				//mysql_query(str_replace('#@__', $dbinfo['tablepre'], trim($q)).';', $conn);
				mysql_query(trim($q).';', $conn);
				//var_dump(trim($q));echo '<br><br><br><br>';
			}

			//创建管理员
			if($dbinfo['dbinitadminsql'])
			{
				mysql_query($dbinfo['dbinitadminsql'], $conn);
			}

		
			//安装完成
			mysql_close($conn);
			return true;
		}
		else
		{
			$outinfo['error']=true;
			$outinfo['errorMsg']='数据库连接错误，请检查！';
			return $outinfo;exit;
		}
	}
	
	//读取文件内容
	function Readf($file)
	{
		if(file_exists($file) and is_readable($file))
		{
			if(function_exists('file_get_contents'))
			{
				$string = file_get_contents($file);
			}
			else
			{
				$fp = fopen($file, 'r');
				while(!feof($fp))
				{
					$string = fgets($fp, 1024);
				}
				fclose($fp);
			}
			return $string;
		}
	}
	
	//写入文件内容
	function Writef($file,$string,$mode='w')
	{
		if(function_exists('file_put_contents'))
		{
			file_put_contents($file, $string);
		}
		else
		{
			$fp = fopen($file,$mode);
			fwrite($fp, $string);
			fclose($fp);
		}
		return true;
	}



}
?>