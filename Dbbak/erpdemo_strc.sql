SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `misone_access`;
CREATE TABLE IF NOT EXISTS `misone_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `module` varchar(50) default NULL,
  `pid` int(11) NOT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `misone_currmas`;
CREATE TABLE IF NOT EXISTS `misone_currmas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(16) NOT NULL,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `rate` float NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

DROP TABLE IF EXISTS `misone_customer`;
CREATE TABLE IF NOT EXISTS `misone_customer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `stk_c` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `diyinfo` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

DROP TABLE IF EXISTS `misone_customerclass`;
CREATE TABLE IF NOT EXISTS `misone_customerclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

DROP TABLE IF EXISTS `misone_faccdefault`;
CREATE TABLE IF NOT EXISTS `misone_faccdefault` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(64) NOT NULL,
  `title` varchar(255) NOT NULL,
  `facc_id` int(11) NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `code_2` (`code`),
  KEY `code_3` (`code`),
  KEY `acc_code` (`facc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

DROP TABLE IF EXISTS `misone_faccmas`;
CREATE TABLE IF NOT EXISTS `misone_faccmas` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `code` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `note` varchar(256) NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  `posttime` int(10) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=173 ;

DROP TABLE IF EXISTS `misone_facheckitem`;
CREATE TABLE IF NOT EXISTS `misone_facheckitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `acc_code` varchar(128) NOT NULL,
  `acc_title` varchar(256) NOT NULL,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `supplier_id` int(11) NOT NULL COMMENT '供应商ID',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `curr_dr` decimal(18,6) NOT NULL,
  `dr` decimal(18,6) NOT NULL,
  `curr_cr` decimal(18,6) NOT NULL,
  `cr` decimal(18,6) NOT NULL,
  `curr_open_amount` decimal(18,6) NOT NULL,
  `open_amount` decimal(18,6) NOT NULL,
  `refid` bigint(20) unsigned NOT NULL COMMENT '对冲账时引用的ID',
  `ref_remark` varchar(512) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

DROP TABLE IF EXISTS `misone_facheckmas`;
CREATE TABLE IF NOT EXISTS `misone_facheckmas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `mas_code` varchar(32) NOT NULL COMMENT '单据类型，如：ARCHKIN,APCHKOUT,FEEPAY',
  `mas_no` varchar(16) NOT NULL COMMENT '单号',
  `acc_id` int(10) NOT NULL COMMENT '客户或供应商',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `manager` int(11) NOT NULL,
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mas_code_no` (`mas_code`,`mas_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

DROP TABLE IF EXISTS `misone_fiscalyp`;
CREATE TABLE IF NOT EXISTS `misone_fiscalyp` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `f_year` smallint(6) NOT NULL,
  `f_period` smallint(6) NOT NULL,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `diyinfo` text NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('0','1','2') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

DROP TABLE IF EXISTS `misone_infoclass`;
CREATE TABLE IF NOT EXISTS `misone_infoclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(100) NOT NULL,
  `keywords` varchar(100) default NULL,
  `picurl` varchar(100) default NULL,
  `classtype` varchar(10) default NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10003 ;

DROP TABLE IF EXISTS `misone_infolist`;
CREATE TABLE IF NOT EXISTS `misone_infolist` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `price` char(10) NOT NULL,
  `infofrom` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `linkurl` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

DROP TABLE IF EXISTS `misone_iotype`;
CREATE TABLE IF NOT EXISTS `misone_iotype` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

DROP TABLE IF EXISTS `misone_linkclass`;
CREATE TABLE IF NOT EXISTS `misone_linkclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

DROP TABLE IF EXISTS `misone_linklist`;
CREATE TABLE IF NOT EXISTS `misone_linklist` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `title` varchar(30) NOT NULL,
  `note` varchar(200) NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `linkurl` varchar(255) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  `posttime` int(10) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

DROP TABLE IF EXISTS `misone_member`;
CREATE TABLE IF NOT EXISTS `misone_member` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `password` char(32) NOT NULL,
  `truename` varchar(30) NOT NULL,
  `sex` enum('0','1') NOT NULL,
  `birthdate` date default NULL,
  `province` varchar(30) NOT NULL,
  `diyinfo` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `domain` varchar(30) NOT NULL,
  `cardtype` enum('idcard','other') NOT NULL,
  `cardnum` varchar(30) NOT NULL,
  `cardpicurl` varchar(100) NOT NULL,
  `showinfo` varchar(100) default NULL,
  `picurl` varchar(100) NOT NULL,
  `mlevel` smallint(5) unsigned NOT NULL,
  `integral` smallint(5) unsigned NOT NULL,
  `regtime` int(10) unsigned NOT NULL,
  `regip` char(20) NOT NULL,
  `logintime` int(10) unsigned NOT NULL,
  `loginip` char(20) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

DROP TABLE IF EXISTS `misone_navclass`;
CREATE TABLE IF NOT EXISTS `misone_navclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

DROP TABLE IF EXISTS `misone_navlist`;
CREATE TABLE IF NOT EXISTS `misone_navlist` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `title` varchar(30) NOT NULL,
  `note` varchar(200) NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `linkurl` varchar(255) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  `posttime` int(10) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

DROP TABLE IF EXISTS `misone_node`;
CREATE TABLE IF NOT EXISTS `misone_node` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) default NULL,
  `status` tinyint(1) default '0',
  `remark` varchar(1024) default NULL,
  `sort` smallint(6) unsigned default NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10299 ;

DROP TABLE IF EXISTS `misone_order`;
CREATE TABLE IF NOT EXISTS `misone_order` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `total_fee` decimal(10,0) NOT NULL,
  `order_id` varchar(50) collate utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `remark` text collate utf8_unicode_ci NOT NULL,
  `status_flg` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

DROP TABLE IF EXISTS `misone_role`;
CREATE TABLE IF NOT EXISTS `misone_role` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) default NULL,
  `status` tinyint(1) unsigned default NULL,
  `remark` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

DROP TABLE IF EXISTS `misone_role_user`;
CREATE TABLE IF NOT EXISTS `misone_role_user` (
  `role_id` mediumint(9) unsigned default NULL,
  `user_id` char(32) default NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `misone_spitem`;
CREATE TABLE IF NOT EXISTS `misone_spitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `stk_id` int(11) NOT NULL COMMENT '商品ID',
  `wh_id` int(11) NOT NULL COMMENT '仓库ID',
  `qty` decimal(18,6) NOT NULL,
  `price` decimal(18,6) NOT NULL,
  `tax_rate` decimal(5,2) NOT NULL,
  `disc_rate` decimal(5,2) NOT NULL,
  `amount1` decimal(18,6) NOT NULL,
  `amount2` decimal(18,6) NOT NULL,
  `qty_processed` decimal(18,6) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL COMMENT '状态',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `batch_no` varchar(50) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `approval_no` varchar(100) NOT NULL,
  `spec` varchar(100) NOT NULL,
  `madein` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `misone_spmas`;
CREATE TABLE IF NOT EXISTS `misone_spmas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `mas_code` varchar(32) NOT NULL COMMENT '单据类型，如：PO,SO,RNS,RNC',
  `mas_no` varchar(16) NOT NULL COMMENT '单号',
  `acc_id` int(10) NOT NULL COMMENT '客户或供应商',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `manager` int(11) NOT NULL,
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  `check_amount` decimal(18,6) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mas_code_no` (`mas_code`,`mas_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

DROP TABLE IF EXISTS `misone_stkbrand`;
CREATE TABLE IF NOT EXISTS `misone_stkbrand` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

DROP TABLE IF EXISTS `misone_stkclass`;
CREATE TABLE IF NOT EXISTS `misone_stkclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

DROP TABLE IF EXISTS `misone_stkdetail`;
CREATE TABLE IF NOT EXISTS `misone_stkdetail` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL,
  `refcode` varchar(32) NOT NULL,
  `refid` bigint(20) NOT NULL,
  `stk_id` int(11) NOT NULL,
  `wh_id` smallint(6) NOT NULL,
  `dr_qty` decimal(18,6) NOT NULL,
  `dr_amount` decimal(18,6) NOT NULL,
  `cr_qty` decimal(18,6) NOT NULL,
  `cr_amount` decimal(18,6) NOT NULL,
  `open_qty` decimal(18,6) NOT NULL,
  `open_amount` decimal(18,6) NOT NULL,
  `detail_id` bigint(20) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `approval_no` varchar(100) NOT NULL,
  `spec` varchar(100) NOT NULL,
  `madein` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `misone_stkioitem`;
CREATE TABLE IF NOT EXISTS `misone_stkioitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `stk_id` int(11) NOT NULL COMMENT '商品ID',
  `wh_id` int(11) NOT NULL COMMENT '仓库ID',
  `qty` decimal(18,6) NOT NULL,
  `price` decimal(18,6) NOT NULL,
  `amount` decimal(18,6) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL COMMENT '状态',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `batch_no` varchar(50) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `approval_no` varchar(100) NOT NULL,
  `spec` varchar(100) NOT NULL,
  `madein` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `misone_stkiomas`;
CREATE TABLE IF NOT EXISTS `misone_stkiomas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `mas_code` varchar(32) NOT NULL COMMENT '单据类型，如：STKIN,STKOUT,WHSWITCH',
  `mas_no` varchar(16) NOT NULL COMMENT '单号',
  `iotype_id` int(10) NOT NULL COMMENT '出入库类型',
  `wh1_id` int(11) NOT NULL COMMENT 'WHSWITCH-仓库ID',
  `wh2_id` int(11) NOT NULL COMMENT 'WHSWITCH-仓库ID',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mas_code_no` (`mas_code`,`mas_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

DROP TABLE IF EXISTS `misone_stkmas`;
CREATE TABLE IF NOT EXISTS `misone_stkmas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `brandid` smallint(5) unsigned NOT NULL,
  `stk_c` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `uom` varchar(10) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `price_sales` decimal(18,6) NOT NULL,
  `price_pur` decimal(18,6) NOT NULL,
  `qty_max` int(10) NOT NULL,
  `qty_min` int(10) NOT NULL,
  `diyinfo` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  `spec` text NOT NULL,
  `madein` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `misone_supplier`;
CREATE TABLE IF NOT EXISTS `misone_supplier` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `stk_c` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `diyinfo` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

DROP TABLE IF EXISTS `misone_supplierclass`;
CREATE TABLE IF NOT EXISTS `misone_supplierclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

DROP TABLE IF EXISTS `misone_test`;
CREATE TABLE IF NOT EXISTS `misone_test` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(64) collate utf8_unicode_ci NOT NULL,
  `html` varchar(256) collate utf8_unicode_ci NOT NULL,
  `php` varchar(256) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

DROP TABLE IF EXISTS `misone_tst`;
CREATE TABLE IF NOT EXISTS `misone_tst` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(512) collate utf8_unicode_ci NOT NULL,
  `content` varchar(1024) collate utf8_unicode_ci NOT NULL,
  `remark` text collate utf8_unicode_ci NOT NULL,
  `orderid` int(11) NOT NULL,
  `status_flg` enum('Y','N') collate utf8_unicode_ci NOT NULL,
  `tst1` varchar(32) collate utf8_unicode_ci NOT NULL,
  `tst2` varchar(32) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `orderid` (`orderid`),
  KEY `title` (`title`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

DROP TABLE IF EXISTS `misone_user`;
CREATE TABLE IF NOT EXISTS `misone_user` (
  `id` int(20) NOT NULL auto_increment,
  `username` varchar(202) character set utf8 NOT NULL,
  `password` varchar(200) character set utf8 NOT NULL,
  `role` varchar(10) collate utf8_unicode_ci NOT NULL default 'admin',
  `logintime` int(11) NOT NULL default '0',
  `loginip` varchar(20) collate utf8_unicode_ci NOT NULL default '127.0.0.1',
  `status_flg` varchar(1) collate utf8_unicode_ci NOT NULL default 'Y',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

DROP TABLE IF EXISTS `misone_vouitem`;
CREATE TABLE IF NOT EXISTS `misone_vouitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `acc_code` varchar(128) NOT NULL,
  `acc_title` varchar(256) NOT NULL,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `supplier_id` int(11) NOT NULL COMMENT '供应商ID',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `curr_dr` decimal(18,6) NOT NULL,
  `dr` decimal(18,6) NOT NULL,
  `curr_cr` decimal(18,6) NOT NULL,
  `cr` decimal(18,6) NOT NULL,
  `curr_open_amount` decimal(18,6) NOT NULL,
  `open_amount` decimal(18,6) NOT NULL,
  `refid` bigint(20) unsigned NOT NULL COMMENT '对冲账时引用的ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=175 ;

DROP TABLE IF EXISTS `misone_voumas`;
CREATE TABLE IF NOT EXISTS `misone_voumas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `refcode` varchar(32) NOT NULL COMMENT '从...生成',
  `refid` bigint(20) NOT NULL COMMENT '从...生成',
  `vouno` varchar(16) NOT NULL COMMENT '凭证号',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `vouno` (`vouno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

DROP TABLE IF EXISTS `misone_webconfig`;
CREATE TABLE IF NOT EXISTS `misone_webconfig` (
  `varname` varchar(50) NOT NULL default '',
  `varinfo` varchar(80) NOT NULL default '',
  `vargroup` varchar(50) NOT NULL default 'basic',
  `vartype` char(10) NOT NULL default 'string',
  `varvalue` text,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`varname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `misone_whmas`;
CREATE TABLE IF NOT EXISTS `misone_whmas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
