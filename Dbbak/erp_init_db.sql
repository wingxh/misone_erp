-- phpMyAdmin SQL Dump
-- version 3.5.0-rc2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 04 月 11 日 12:08
-- 服务器版本: 5.0.27-community-nt
-- PHP 版本: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `erpdemo`
--

-- --------------------------------------------------------

--
-- 表的结构 `misone_access`
--

DROP TABLE IF EXISTS `misone_access`;
CREATE TABLE IF NOT EXISTS `misone_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `module` varchar(50) default NULL,
  `pid` int(11) NOT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `misone_access`
--

INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 1, 1, '', 0);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10001, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10012, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10013, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10014, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10017, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10021, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10026, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10027, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10028, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10029, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10030, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10002, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10031, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10032, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10033, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10035, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10038, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10039, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10040, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10041, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10044, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10047, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10050, 3, '', 10002);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10003, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10053, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10054, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10055, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10057, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10060, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10061, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10063, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10066, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10067, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10068, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10070, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10079, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10080, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10081, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10082, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10083, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10004, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10084, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10085, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10086, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10088, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10093, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10094, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10095, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10097, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10102, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10104, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10106, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10107, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10005, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10108, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10109, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10110, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10112, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10115, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10116, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10117, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10119, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10128, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10129, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10130, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10131, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10006, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10132, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10133, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10134, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10136, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10139, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10140, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10141, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10143, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10152, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10153, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10154, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10155, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10007, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10156, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10008, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10177, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10009, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10198, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10010, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10224, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10225, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10226, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10228, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10234, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10239, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10240, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10241, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10243, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10246, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10247, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10248, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10249, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10250, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10251, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10253, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10265, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10269, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10277, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(1, 10011, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10003, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10053, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10054, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10055, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10056, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10057, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10058, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10060, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10061, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10062, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10063, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10064, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10066, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10067, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10068, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10069, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10070, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10071, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10072, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10074, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10075, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10076, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10077, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10078, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10079, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10080, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10081, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10082, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10083, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10004, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10084, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10085, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10086, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10087, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10088, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10089, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10091, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10092, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10093, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10094, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10095, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10096, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10097, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10098, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10100, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10101, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10102, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10103, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10104, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10105, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10106, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10107, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10005, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10108, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10109, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10110, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10111, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10112, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10113, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10115, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10116, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10117, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10118, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10119, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10120, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10122, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10123, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10124, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10125, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10126, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10127, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10128, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10129, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10130, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10131, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10006, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10132, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10133, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10134, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10135, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10136, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10137, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10139, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10140, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10141, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10142, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10143, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10144, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10146, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10147, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10148, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10149, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10150, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10151, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10152, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10153, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10154, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10155, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10007, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10156, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10157, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10158, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10159, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10160, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10161, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10162, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10163, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10164, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10165, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10166, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10167, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10168, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10169, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10170, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10171, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10172, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10173, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10174, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10175, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10176, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10008, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10177, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10178, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10179, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10180, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10181, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10182, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10183, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10184, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10185, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10186, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10187, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10188, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10189, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10190, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10191, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10192, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10193, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10194, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10195, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10196, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10197, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10009, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10198, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10199, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10200, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10201, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10202, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10203, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10204, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10205, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10206, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10207, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10208, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10209, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10210, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10211, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10212, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10213, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10214, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10215, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10216, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10217, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10218, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10219, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10220, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10221, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10222, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10223, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10010, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10224, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10225, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10226, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10227, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10228, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10229, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10231, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10232, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10233, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10234, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10235, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10236, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10237, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10238, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10239, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10240, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10241, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10242, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10243, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10244, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10246, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10247, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10248, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10249, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10250, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10251, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10252, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10253, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10254, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10256, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10257, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10258, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10259, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10260, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10261, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10262, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10263, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10264, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10265, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10266, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10267, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10268, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10269, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10270, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10271, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10272, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10273, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10274, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10275, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10276, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10277, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10278, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10279, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10280, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10281, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10282, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10283, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10284, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10285, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10286, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10287, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10288, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10289, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10290, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10291, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10292, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(5, 10011, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 1, 1, '', 0);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10001, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10012, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10013, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10014, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10028, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10029, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10030, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10003, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10053, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10054, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10055, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10056, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10057, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10058, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10060, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10061, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10062, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10063, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10064, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10066, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10067, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10068, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10069, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10070, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10071, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10079, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10080, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10081, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10082, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10083, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10004, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10084, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10085, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10093, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10094, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10102, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10104, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10105, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10005, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10108, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10109, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10110, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10111, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10112, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10113, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10115, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10116, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10117, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10118, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10119, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10120, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10121, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10123, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10124, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10125, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10126, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10127, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10128, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10129, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10130, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10131, 3, '', 10005);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10007, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10156, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10157, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10158, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10159, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10160, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10161, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10162, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10163, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10164, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10165, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10166, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10167, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10168, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10169, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10170, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10171, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10172, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10173, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10174, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10175, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10176, 3, '', 10007);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10009, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10198, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10220, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10221, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10222, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10223, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10010, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10224, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10225, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10233, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10234, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10246, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10247, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10248, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10249, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10269, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10270, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10271, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10277, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10288, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(4, 10291, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 1, 1, '', 0);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10001, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10012, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10013, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10014, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10029, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10030, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10003, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10053, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10054, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10055, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10056, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10057, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10058, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10060, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10061, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10062, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10063, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10064, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10066, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10067, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10068, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10069, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10070, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10071, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10079, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10080, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10081, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10082, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10083, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10004, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10084, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10085, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10093, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10094, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10102, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10104, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10105, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10106, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10107, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10006, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10132, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10133, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10134, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10135, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10136, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10137, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10139, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10140, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10141, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10142, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10143, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10144, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10146, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10147, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10148, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10149, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10150, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10151, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10152, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10153, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10154, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10155, 3, '', 10006);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10008, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10177, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10178, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10179, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10180, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10181, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10182, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10183, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10184, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10185, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10186, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10187, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10188, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10189, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10190, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10191, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10192, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10193, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10194, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10195, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10196, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10197, 3, '', 10008);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10009, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10198, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10220, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10223, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10010, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10224, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10225, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10233, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10234, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10239, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10240, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10246, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10247, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10248, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10249, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10250, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10265, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10269, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10280, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10281, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10287, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(3, 10292, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 1, 1, '', 0);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10001, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10012, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10013, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10014, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10026, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10027, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10028, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10029, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10030, 3, '', 10001);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10003, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10053, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10054, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10055, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10056, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10057, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10058, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10060, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10061, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10062, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10063, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10064, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10066, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10067, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10068, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10069, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10070, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10071, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10072, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10074, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10075, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10076, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10077, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10078, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10079, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10080, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10081, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10082, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10083, 3, '', 10003);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10004, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10084, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10085, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10086, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10087, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10088, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10089, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10091, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10092, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10093, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10094, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10095, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10096, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10097, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10098, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10100, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10101, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10102, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10103, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10104, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10105, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10106, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10107, 3, '', 10004);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10009, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10198, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10199, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10200, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10201, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10202, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10203, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10204, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10205, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10206, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10207, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10208, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10209, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10210, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10211, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10212, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10213, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10214, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10215, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10216, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10217, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10218, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10219, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10220, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10221, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10222, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10223, 3, '', 10009);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10010, 2, '', 1);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10224, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10225, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10233, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10234, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10239, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10246, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10247, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10248, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10249, 3, '', 10010);
INSERT INTO `misone_access` (`role_id`, `node_id`, `level`, `module`, `pid`) VALUES(2, 10250, 3, '', 10010);

-- --------------------------------------------------------

--
-- 表的结构 `misone_currmas`
--

DROP TABLE IF EXISTS `misone_currmas`;
CREATE TABLE IF NOT EXISTS `misone_currmas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(16) NOT NULL,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `rate` float NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `misone_currmas`
--

INSERT INTO `misone_currmas` (`id`, `code`, `title`, `flag`, `rate`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(1, 'CNY', '人民币', 'Y', 1, '', 1, 1348908529, 'Y');

-- --------------------------------------------------------

--
-- 表的结构 `misone_customer`
--

DROP TABLE IF EXISTS `misone_customer`;
CREATE TABLE IF NOT EXISTS `misone_customer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `stk_c` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `diyinfo` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_customerclass`
--

DROP TABLE IF EXISTS `misone_customerclass`;
CREATE TABLE IF NOT EXISTS `misone_customerclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_faccdefault`
--

DROP TABLE IF EXISTS `misone_faccdefault`;
CREATE TABLE IF NOT EXISTS `misone_faccdefault` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `code` varchar(64) NOT NULL,
  `title` varchar(255) NOT NULL,
  `facc_id` int(11) NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `code_2` (`code`),
  KEY `code_3` (`code`),
  KEY `acc_code` (`facc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- 转存表中的数据 `misone_faccdefault`
--

INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(1, 'INV-AR', '销售-借方应收账款科目', 24, 1, 1349085089, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(2, 'INV-TAX', '销售-销项税', 86, 2, 1349085810, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(3, 'INV-PROFIT', '销售-收入', 159, 3, 1349085842, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(4, 'SINV-AP', '采购-应付账款', 74, 4, 1349102726, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(5, 'SINV-TAX', '采购-进项税', 82, 5, 1349102757, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(6, 'SINV-STK', '采购-库存商品', 35, 6, 1349102793, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(7, 'STKIO-FEE', '其他出入库-损益类科目', 167, 8, 1349708471, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(8, 'STKIO-STK', '其他出入库-存货科目', 35, 9, 1349708514, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(9, 'SINV-FEE', '采购-费用类', 167, 7, 1349839598, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(10, 'INV-STK', '销售-库存商品', 35, 3, 1350009596, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(11, 'BANK', '现金或银行默认科目', 6, 11, 1350009651, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(12, 'PRECOLLECTED', '预收科目', 75, 12, 1350009806, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(13, 'PREPAID', '预付科目', 27, 13, 1350009854, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(14, 'EGOL', '汇兑损益(Exchange Gains Or Losses)', 169, 14, 1350010478, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(15, 'INV-FEE', '销售成本', 164, 3, 1350446101, 'Y');
INSERT INTO `misone_faccdefault` (`id`, `code`, `title`, `facc_id`, `orderid`, `posttime`, `status_flg`) VALUES(16, 'SINV-DIFF', '采购退货成本差异', 164, 7, 1350453749, 'Y');

-- --------------------------------------------------------

--
-- 表的结构 `misone_faccmas`
--

DROP TABLE IF EXISTS `misone_faccmas`;
CREATE TABLE IF NOT EXISTS `misone_faccmas` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `code` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `note` varchar(256) NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  `posttime` int(10) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=173 ;

--
-- 转存表中的数据 `misone_faccmas`
--

INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(1, 0, ',0,', '1000', '资产', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 1, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(2, 0, ',0,', '2000', '负债', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 2, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(3, 0, ',0,', '3000', '所有者权益', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 3, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(4, 0, ',0,', '4000', '成本', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 4, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(5, 0, ',0,', '5000', '损益', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 5, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(6, 1, ',0,1,', '1001', '现金', '', 'a:6:{s:4:"bank";s:1:"Y";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 6, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(7, 1, ',0,1,', '1002', '银行存款', '', 'a:6:{s:4:"bank";s:1:"Y";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 7, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(8, 1, ',0,1,', '1009', '其他货币资金', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 8, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(9, 8, ',0,1,8,', '100901', '外埠存款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 9, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(10, 8, ',0,1,8,', '100902', '银行本票存款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 10, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(11, 8, ',0,1,8,', '100903', '银行汇票存款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 11, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(12, 8, ',0,1,8,', '100904', '信用卡', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 12, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(13, 8, ',0,1,8,', '100905', '信用证保证金', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 13, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(14, 8, ',0,1,8,', '100906', '存出投资款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 14, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(15, 1, ',0,1,', '1101', '短期投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 15, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(16, 15, ',0,1,15,', '110101', '股票', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 16, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(17, 15, ',0,1,15,', '110102', '债券', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 17, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(18, 15, ',0,1,15,', '110103', '基金', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 18, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(19, 15, ',0,1,15,', '110110', '其他', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 19, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(20, 1, ',0,1,', '1102', '短期投资跌价准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 20, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(21, 1, ',0,1,', '1111', '应收票据', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 21, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(22, 1, ',0,1,', '1121', '应收股利', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 22, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(23, 1, ',0,1,', '1122', '应收利息', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 23, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(24, 1, ',0,1,', '1131', '应收账款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"Y";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 24, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(25, 1, ',0,1,', '1133', '其他应收款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 25, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(26, 1, ',0,1,', '1141', '坏账准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 26, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(27, 1, ',0,1,', '1151', '预付账款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"Y";s:6:"profit";s:1:"N";}', 27, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(28, 1, ',0,1,', '1161', '应收补贴款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 28, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(29, 1, ',0,1,', '1201', '物资采购', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 29, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(30, 1, ',0,1,', '1211', '原材料', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 30, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(31, 1, ',0,1,', '1221', '包装物', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 31, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(32, 1, ',0,1,', '1231', '低值易耗品', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 32, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(33, 1, ',0,1,', '1232', '材料成本差异', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 33, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(34, 1, ',0,1,', '1241', '自制半成品', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 34, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(35, 1, ',0,1,', '1243', '库存商品', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 35, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(36, 1, ',0,1,', '1244', '商品进销差价', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 36, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(37, 1, ',0,1,', '1251', '委托加工物资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 37, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(38, 1, ',0,1,', '1261', '委托代销商品', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 38, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(39, 1, ',0,1,', '1271', '受托代销商品', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 39, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(40, 1, ',0,1,', '1281', '存货跌价准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 40, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(41, 1, ',0,1,', '1291', '分期收款发出商品', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 41, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(42, 1, ',0,1,', '1301', '待摊费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 42, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(43, 1, ',0,1,', '1401', '长期股权投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 43, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(44, 43, ',0,1,43,', '140101', '股票投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 44, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(45, 43, ',0,1,43,', '140102', '其他股权投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 45, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(46, 1, ',0,1,', '1402', '长期债权投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 46, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(47, 46, ',0,1,46,', '140201', '债券投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 47, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(48, 46, ',0,1,46,', '140202', '其他债权投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 48, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(49, 1, ',0,1,', '1421', '长期投资减值准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 49, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(50, 1, ',0,1,', '1431', '委托贷款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 50, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(51, 50, ',0,1,50,', '143101', '本金', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 51, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(52, 50, ',0,1,50,', '143102', '利息', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 52, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(53, 50, ',0,1,50,', '143103', '减值准备', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 53, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(54, 1, ',0,1,', '1501', '固定资产', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 54, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(55, 1, ',0,1,', '1502', '累计折旧', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 55, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(56, 1, ',0,1,', '1505', '固定资产减值准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 56, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(57, 1, ',0,1,', '1601', '工程物资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 57, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(58, 57, ',0,1,57,', '160101', '专用材料', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 58, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(59, 57, ',0,1,57,', '160102', '专用设备', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 59, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(60, 57, ',0,1,57,', '160103', '预付大型设备款', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 60, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(61, 57, ',0,1,57,', '160104', '为生产准备的工具及器具', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 61, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(62, 1, ',0,1,', '1603', '在建工程', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 62, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(63, 1, ',0,1,', '1605', '在建工程减值准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 63, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(64, 1, ',0,1,', '1701', '固定资产清理', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 64, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(65, 1, ',0,1,', '1801', '无形资产', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 65, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(66, 1, ',0,1,', '1805', '无形资产减值准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 66, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(67, 1, ',0,1,', '1815', '未确认融资费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 67, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(68, 1, ',0,1,', '1901', '长期待摊费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 68, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(69, 1, ',0,1,', '1911', '待处理财产损溢', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 69, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(70, 69, ',0,1,69,', '191101', '待处理流动资产损溢', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 70, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(71, 69, ',0,1,69,', '191102', '待处理固定资产损溢', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 71, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(72, 2, ',0,2,', '2101', '短期借款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 72, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(73, 2, ',0,2,', '2111', '应付票据', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 73, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(74, 2, ',0,2,', '2121', '应付账款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"Y";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 74, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(75, 2, ',0,2,', '2131', '预收账款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"Y";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 75, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(76, 2, ',0,2,', '2141', '代销商品款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 76, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(77, 2, ',0,2,', '2151', '应付工资', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 77, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(78, 2, ',0,2,', '2153', '应付福利费', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 78, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(79, 2, ',0,2,', '2161', '应付股利', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 79, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(80, 2, ',0,2,', '2171', '应交税金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 80, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(81, 80, ',0,2,80,', '217101', '应交增值税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 81, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(82, 81, ',0,2,80,81,', '21710101', '进项税额', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 82, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(83, 81, ',0,2,80,81,', '21710102', '已交税金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 83, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(84, 81, ',0,2,80,81,', '21710103', '转出未交增值税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 84, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(85, 81, ',0,2,80,81,', '21710104', '减免税款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 85, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(86, 81, ',0,2,80,81,', '21710105', '销项税额', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 86, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(87, 81, ',0,2,80,81,', '21710106', '出口退税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 87, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(88, 81, ',0,2,80,81,', '21710107', '进项税额转出', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 88, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(89, 81, ',0,2,80,81,', '21710108', '出口抵减内销产品应纳税额', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 89, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(90, 81, ',0,2,80,81,', '21710109', '转出多交增值税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 90, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(91, 80, ',0,2,80,', '217102', '未交增值税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 91, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(92, 80, ',0,2,80,', '217103', '应交营业税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 92, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(93, 80, ',0,2,80,', '217104', '应交消费税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 93, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(94, 80, ',0,2,80,', '217105', '应交资源税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 94, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(95, 80, ',0,2,80,', '217106', '应交所得税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 95, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(96, 80, ',0,2,80,', '217107', '应交土地增值税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 96, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(97, 80, ',0,2,80,', '217108', '应交城市维护建设税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 97, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(98, 80, ',0,2,80,', '217109', '应交房产税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 98, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(99, 80, ',0,2,80,', '217110', '应交土地使用税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 99, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(100, 80, ',0,2,80,', '217111', '应交车船使用税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 100, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(101, 80, ',0,2,80,', '217112', '应交个人所得税', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 101, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(102, 80, ',0,2,80,', '217113', '教育附加', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 102, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(103, 80, ',0,2,80,', '217114', '地方教育发展费', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 103, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(104, 2, ',0,2,', '2176', '其他应交款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 104, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(105, 2, ',0,2,', '2181', '其他应付款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 105, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(106, 2, ',0,2,', '2191', '预提费用', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 106, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(107, 2, ',0,2,', '2201', '待转资产价值', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 107, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(108, 2, ',0,2,', '2211', '预计负债', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 108, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(109, 2, ',0,2,', '2301', '长期借款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 109, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(110, 109, ',0,2,109,', '230101', '借款本金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 110, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(111, 109, ',0,2,109,', '230102', '借款利息', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 111, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(112, 2, ',0,2,', '2311', '应付债券', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 112, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(113, 112, ',0,2,112,', '231101', '债券面值', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 113, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(114, 112, ',0,2,112,', '231102', '债券溢价', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 114, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(115, 112, ',0,2,112,', '231103', '债券折价', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 115, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(116, 112, ',0,2,112,', '231104', '应计利息', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 116, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(117, 2, ',0,2,', '2321', '长期应付款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 117, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(118, 2, ',0,2,', '2331', '专项应付款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 118, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(119, 2, ',0,2,', '2341', '递延税款', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 119, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(120, 3, ',0,3,', '3101', '实收资本（或股本）', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 120, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(121, 3, ',0,3,', '3103', '已归还投资', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 121, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(122, 3, ',0,3,', '3111', '资本公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 122, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(123, 122, ',0,3,122,', '311101', '资本（或股本）溢价', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 123, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(124, 122, ',0,3,122,', '311102', '接受捐赠非现金资产准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 124, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(125, 122, ',0,3,122,', '311103', '接受现金捐赠', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 125, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(126, 122, ',0,3,122,', '311104', '股权投资准备', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 126, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(127, 122, ',0,3,122,', '311105', '拨款转入', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 127, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(128, 122, ',0,3,122,', '311106', '外币资本折算差额', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 128, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(129, 122, ',0,3,122,', '311107', '其他资本公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 129, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(130, 3, ',0,3,', '3121', '盈余公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 130, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(131, 130, ',0,3,130,', '312101', '法定盈余公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 131, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(132, 130, ',0,3,130,', '312102', '任意盈余公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 132, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(133, 130, ',0,3,130,', '312103', '法定公益金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 133, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(134, 130, ',0,3,130,', '312104', '储备基金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 134, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(135, 130, ',0,3,130,', '312105', '企业发展基金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 135, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(136, 130, ',0,3,130,', '312106', '利润归还投资', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 136, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(137, 3, ',0,3,', '3131', '本年利润', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"Y";s:6:"credit";s:1:"Y";}', 137, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(138, 3, ',0,3,', '3141', '利润分配', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 138, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(139, 138, ',0,3,138,', '314101', '其他转入', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 139, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(140, 138, ',0,3,138,', '314102', '提取法定盈余公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 140, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(141, 138, ',0,3,138,', '314103', '提取法定公益金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 141, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(142, 138, ',0,3,138,', '314104', '提取储备基金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 142, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(143, 138, ',0,3,138,', '314105', '提取企业发展基金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 143, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(144, 138, ',0,3,138,', '314106', '提取职工奖励及福利基金', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 144, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(145, 138, ',0,3,138,', '314107', '利润归还投资', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 145, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(146, 138, ',0,3,138,', '314108', '应付优先股股利', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 146, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(147, 138, ',0,3,138,', '314109', '提取任意盈余公积', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 147, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(148, 138, ',0,3,138,', '314110', '应付普通股股利', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 148, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(149, 138, ',0,3,138,', '314111', '转作资本（或股本）的普通股股利', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 149, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(150, 138, ',0,3,138,', '314115', '未分配利润', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 150, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(151, 4, ',0,4,', '4101', '生产成本', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 151, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(152, 151, ',0,4,148,', '410101', '基本生产成本', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 152, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(153, 152, ',0,4,148,152,', '41010101', '直接材料费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 153, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(154, 152, ',0,4,148,152,', '41010102', '直接人工费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 154, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(155, 151, ',0,4,148,', '410102', '辅助生产成本', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 155, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(156, 4, ',0,4,', '4105', '制造费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 156, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(157, 4, ',0,4,', '4107', '劳务成本', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 157, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(158, 4, ',0,4,', '4108', '研发支出', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 158, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(159, 5, ',0,5,', '5101', '主营业务收入', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 159, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(160, 5, ',0,5,', '5102', '其他业务收入', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 160, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(161, 5, ',0,5,', '5201', '投资收益', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 161, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(162, 5, ',0,5,', '5203', '补贴收入', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 162, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(163, 5, ',0,5,', '5301', '营业外收入', '', 'a:7:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";s:6:"credit";s:1:"Y";}', 163, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(164, 5, ',0,5,', '5401', '主营业务成本', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 164, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(165, 5, ',0,5,', '5402', '主营业务税金及附加', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 165, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(166, 5, ',0,5,', '5405', '其他业务支出', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 166, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(167, 5, ',0,5,', '5501', '营业费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 167, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(168, 5, ',0,5,', '5502', '管理费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 168, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(169, 5, ',0,5,', '5503', '财务费用', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 169, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(170, 5, ',0,5,', '5601', '营业外支出', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 170, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(171, 5, ',0,5,', '5701', '所得税', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 171, 0, 'Y');
INSERT INTO `misone_faccmas` (`id`, `parentid`, `parentstr`, `code`, `title`, `note`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(172, 5, ',0,5,', '5801', '以前年度损益调整', '', 'a:6:{s:4:"bank";s:1:"N";s:6:"arctrl";s:1:"N";s:6:"apctrl";s:1:"N";s:12:"precollected";s:1:"N";s:7:"prepaid";s:1:"N";s:6:"profit";s:1:"N";}', 172, 0, 'Y');

-- --------------------------------------------------------

--
-- 表的结构 `misone_facheckitem`
--

DROP TABLE IF EXISTS `misone_facheckitem`;
CREATE TABLE IF NOT EXISTS `misone_facheckitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `acc_code` varchar(128) NOT NULL,
  `acc_title` varchar(256) NOT NULL,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `supplier_id` int(11) NOT NULL COMMENT '供应商ID',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `curr_dr` decimal(18,6) NOT NULL,
  `dr` decimal(18,6) NOT NULL,
  `curr_cr` decimal(18,6) NOT NULL,
  `cr` decimal(18,6) NOT NULL,
  `curr_open_amount` decimal(18,6) NOT NULL,
  `open_amount` decimal(18,6) NOT NULL,
  `refid` bigint(20) unsigned NOT NULL COMMENT '对冲账时引用的ID',
  `ref_remark` varchar(512) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_facheckmas`
--

DROP TABLE IF EXISTS `misone_facheckmas`;
CREATE TABLE IF NOT EXISTS `misone_facheckmas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `mas_code` varchar(32) NOT NULL COMMENT '单据类型，如：ARCHKIN,APCHKOUT,FEEPAY',
  `mas_no` varchar(16) NOT NULL COMMENT '单号',
  `acc_id` int(10) NOT NULL COMMENT '客户或供应商',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `manager` int(11) NOT NULL,
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mas_code_no` (`mas_code`,`mas_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_fiscalyp`
--

DROP TABLE IF EXISTS `misone_fiscalyp`;
CREATE TABLE IF NOT EXISTS `misone_fiscalyp` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `f_year` smallint(6) NOT NULL,
  `f_period` smallint(6) NOT NULL,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `diyinfo` text NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('0','1','2') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- 转存表中的数据 `misone_fiscalyp`
--

INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(1, 2011, 1, '2011年1月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(2, 2011, 2, '2011年2月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(3, 2011, 3, '2011年3月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(4, 2011, 4, '2011年4月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(5, 2011, 5, '2011年5月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(6, 2011, 6, '2011年6月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(7, 2011, 7, '2011年7月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(8, 2011, 8, '2011年8月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(9, 2011, 9, '2011年9月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(10, 2011, 10, '2011年10月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(11, 2011, 11, '2011年11月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(12, 2011, 12, '2011年12月', '', '', 1348911983, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(13, 2012, 1, '2012年1月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(14, 2012, 2, '2012年2月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(15, 2012, 3, '2012年3月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(16, 2012, 4, '2012年4月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(17, 2012, 5, '2012年5月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(18, 2012, 6, '2012年6月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(19, 2012, 7, '2012年7月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(20, 2012, 8, '2012年8月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(21, 2012, 9, '2012年9月', '', '', 1348912048, '1');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(22, 2012, 10, '2012年10月', '', '', 1348912048, '2');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(23, 2012, 11, '2012年11月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(24, 2012, 12, '2012年12月', '', '', 1348912048, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(25, 2013, 1, '2013年1月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(26, 2013, 2, '2013年2月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(27, 2013, 3, '2013年3月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(28, 2013, 4, '2013年4月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(29, 2013, 5, '2013年5月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(30, 2013, 6, '2013年6月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(31, 2013, 7, '2013年7月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(32, 2013, 8, '2013年8月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(33, 2013, 9, '2013年9月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(34, 2013, 10, '2013年10月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(35, 2013, 11, '2013年11月', '', '', 1361865545, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(36, 2013, 12, '2013年12月', '', '', 1361865545, '2');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(37, 2014, 1, '2014年1月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(38, 2014, 2, '2014年2月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(39, 2014, 3, '2014年3月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(40, 2014, 4, '2014年4月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(41, 2014, 5, '2014年5月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(42, 2014, 6, '2014年6月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(43, 2014, 7, '2014年7月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(44, 2014, 8, '2014年8月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(45, 2014, 9, '2014年9月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(46, 2014, 10, '2014年10月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(47, 2014, 11, '2014年11月', '', '', 1362548384, '0');
INSERT INTO `misone_fiscalyp` (`id`, `f_year`, `f_period`, `title`, `flag`, `diyinfo`, `posttime`, `status_flg`) VALUES(48, 2014, 12, '2014年12月', '', '', 1362548384, '0');

-- --------------------------------------------------------

--
-- 表的结构 `misone_infoclass`
--

DROP TABLE IF EXISTS `misone_infoclass`;
CREATE TABLE IF NOT EXISTS `misone_infoclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(100) NOT NULL,
  `keywords` varchar(100) default NULL,
  `picurl` varchar(100) default NULL,
  `classtype` varchar(10) default NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10003 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_infolist`
--

DROP TABLE IF EXISTS `misone_infolist`;
CREATE TABLE IF NOT EXISTS `misone_infolist` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `price` char(10) NOT NULL,
  `infofrom` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `linkurl` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_iotype`
--

DROP TABLE IF EXISTS `misone_iotype`;
CREATE TABLE IF NOT EXISTS `misone_iotype` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `misone_iotype`
--

INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(1, '系统初始化入库', 'IN', '', 'N', 1, 1348893393, 'Y');
INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(2, '客商赠送入库', 'IN', '', 'N', 2, 1348893482, 'Y');
INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(3, '客商赠送出库', 'OUT', '', 'N', 3, 1348893524, 'Y');
INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(4, '盘盈', 'IN', '', 'N', 4, 1348893543, 'Y');
INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(5, '盘亏', 'OUT', '', 'N', 5, 1348893554, 'Y');
INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(6, '客户返修入库', 'IN', '', 'N', 6, 1350548880, 'Y');
INSERT INTO `misone_iotype` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(7, '客户返修归还出库', 'OUT', '', 'N', 7, 1350548899, 'Y');

-- --------------------------------------------------------

--
-- 表的结构 `misone_linkclass`
--

DROP TABLE IF EXISTS `misone_linkclass`;
CREATE TABLE IF NOT EXISTS `misone_linkclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_linklist`
--

DROP TABLE IF EXISTS `misone_linklist`;
CREATE TABLE IF NOT EXISTS `misone_linklist` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `title` varchar(30) NOT NULL,
  `note` varchar(200) NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `linkurl` varchar(255) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  `posttime` int(10) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_member`
--

DROP TABLE IF EXISTS `misone_member`;
CREATE TABLE IF NOT EXISTS `misone_member` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `password` char(32) NOT NULL,
  `truename` varchar(30) NOT NULL,
  `sex` enum('0','1') NOT NULL,
  `birthdate` date default NULL,
  `province` varchar(30) NOT NULL,
  `diyinfo` text NOT NULL,
  `address` varchar(50) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `domain` varchar(30) NOT NULL,
  `cardtype` enum('idcard','other') NOT NULL,
  `cardnum` varchar(30) NOT NULL,
  `cardpicurl` varchar(100) NOT NULL,
  `showinfo` varchar(100) default NULL,
  `picurl` varchar(100) NOT NULL,
  `mlevel` smallint(5) unsigned NOT NULL,
  `integral` smallint(5) unsigned NOT NULL,
  `regtime` int(10) unsigned NOT NULL,
  `regip` char(20) NOT NULL,
  `logintime` int(10) unsigned NOT NULL,
  `loginip` char(20) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_navclass`
--

DROP TABLE IF EXISTS `misone_navclass`;
CREATE TABLE IF NOT EXISTS `misone_navclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_navlist`
--

DROP TABLE IF EXISTS `misone_navlist`;
CREATE TABLE IF NOT EXISTS `misone_navlist` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `title` varchar(30) NOT NULL,
  `note` varchar(200) NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `linkurl` varchar(255) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  `posttime` int(10) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_node`
--

DROP TABLE IF EXISTS `misone_node`;
CREATE TABLE IF NOT EXISTS `misone_node` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) default NULL,
  `status` tinyint(1) default '0',
  `remark` varchar(1024) default NULL,
  `sort` smallint(6) unsigned default NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10299 ;

--
-- 转存表中的数据 `misone_node`
--

INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(1, 'Erp', 'ERP主系统', 1, ',0,', 0, 0, 1);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10001, 'Index', '系统管理', 1, ',0,1,', 1, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10002, 'Power', '用户组及权限', 1, ',0,1,', 2, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10003, 'Stk', '商品管理', 1, ',0,1,', 3, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10004, 'Warehouse', '仓库管理', 1, ',0,1,', 4, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10005, 'Customer', '客户管理', 1, ',0,1,', 5, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10006, 'Supplier', '供应商管理', 1, ',0,1,', 6, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10007, 'Sales', '销售模块', 1, ',0,1,', 7, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10008, 'Purchasing', '采购模块', 1, ',0,1,', 8, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10009, 'Stkio', '其他库存进出', 1, ',0,1,', 9, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10010, 'Finance', '财务模块', 1, ',0,1,', 10, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10011, 'Info', '其他信息管理', 0, ',0,1,', 11, 1, 2);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10012, 'index', '系统欢迎界面', 1, ',0,1,10001,', 101, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10013, 'edit_password', '修改密码界面', 1, ',0,1,10001,', 102, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10014, 'edit_password_save', '修改密码保存', 1, ',0,1,10001,', 103, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10015, 'account_add', '增加用户界面', 1, ',0,1,10001,', 104, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10016, 'account_add_save', '增加用户保存', 1, ',0,1,10001,', 105, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10017, 'account_list', '查看用户列表', 1, ',0,1,10001,', 106, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10018, 'account_edit', '编辑用户界面', 1, ',0,1,10001,', 107, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10019, 'account_edit_save', '编辑用户保存', 1, ',0,1,10001,', 108, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10020, 'account_del', '删除用户', 1, ',0,1,10001,', 109, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10021, 'account_popup', '打开用户弹窗', 1, ',0,1,10001,', 110, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10022, 'web_config', '系统配置界面', 1, ',0,1,10001,', 111, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10023, 'web_config_add_save', '新增系统配置项', 1, ',0,1,10001,', 112, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10024, 'web_config_save', '保存系统配置修改', 1, ',0,1,10001,', 113, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10025, 'web_config_del', '删除系统配置项', 1, ',0,1,10001,', 114, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10026, 'uploadbox', '上传界面', 1, ',0,1,10001,', 115, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10027, 'upload_save', '上传保存', 1, ',0,1,10001,', 116, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10028, 'today', '今日看板', 1, ',0,1,10001,', 117, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10029, 'help', '帮助界面', 1, ',0,1,10001,', 118, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10030, 'about', '关于界面', 1, ',0,1,10001,', 119, 10001, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10031, 'index', '用户组和权限首页', 1, ',0,1,10002,', 122, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10032, 'node_list', '查看模块列表', 1, ',0,1,10002,', 123, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10033, 'node_add', '新增模块界面', 1, ',0,1,10002,', 124, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10034, 'node_add_save', '新增模块保存', 1, ',0,1,10002,', 125, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10035, 'node_edit', '编辑模块界面', 1, ',0,1,10002,', 126, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10036, 'node_edit_save', '编辑模块保存', 1, ',0,1,10002,', 127, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10037, 'node_del', '删除模块项', 1, ',0,1,10002,', 128, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10038, 'get_parentstr', '公用-读取级层关系', 1, ',0,1,10002,', 130, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10039, 'set_parentstr', '公用-设置级层关系', 1, ',0,1,10002,', 131, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10040, 'get_node_list', '公用-读取模块到下拉框', 1, ',0,1,10002,', 132, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10041, 'role_list', '查看用户组列表', 1, ',0,1,10002,', 134, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10042, 'role_add', '新增用户组界面', 1, ',0,1,10002,', 135, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10043, 'role_add_save', '新增用户组保存', 1, ',0,1,10002,', 136, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10044, 'role_edit', '编辑用户组界面', 1, ',0,1,10002,', 137, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10045, 'role_edit_save', '编辑用户组保存', 1, ',0,1,10002,', 138, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10046, 'role_del', '删除用户组', 1, ',0,1,10002,', 139, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10047, 'access', '配置用户组权限界面', 1, ',0,1,10002,', 141, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10048, 'access_save', '配置用户组权限保存', 1, ',0,1,10002,', 142, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10049, 'access_clear', '清除用户组权限', 1, ',0,1,10002,', 143, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10050, 'role_user', '设置用户组成员界面', 1, ',0,1,10002,', 145, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10051, 'role_user_save', '设置用户组成员保存', 1, ',0,1,10002,', 146, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10052, 'user_clear', '清除用户组成员', 1, ',0,1,10002,', 147, 10002, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10053, 'index', '商品管理首页', 1, ',0,1,10003,', 150, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10054, 'class_list', '查看类别列表', 1, ',0,1,10003,', 151, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10055, 'class_add', '新增类别界面', 1, ',0,1,10003,', 152, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10056, 'class_add_save', '新增类别保存', 1, ',0,1,10003,', 153, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10057, 'class_edit', '编辑类别界面', 1, ',0,1,10003,', 154, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10058, 'class_edit_save', '编辑类别保存', 1, ',0,1,10003,', 155, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10059, 'class_del', '删除类别', 1, ',0,1,10003,', 156, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10060, 'brand_list', '查看品牌列表', 1, ',0,1,10003,', 158, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10061, 'brand_add', '新增品牌界面', 1, ',0,1,10003,', 159, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10062, 'brand_add_save', '新增品牌保存', 1, ',0,1,10003,', 160, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10063, 'brand_edit', '编辑品牌界面', 1, ',0,1,10003,', 161, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10064, 'brand_edit_save', '编辑品牌保存', 1, ',0,1,10003,', 162, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10065, 'brand_del', '删除品牌', 1, ',0,1,10003,', 163, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10066, 'data_list', '查看商品列表', 1, ',0,1,10003,', 165, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10067, 'data_list_rec', '查看商品回收站', 1, ',0,1,10003,', 166, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10068, 'data_add', '新增商品界面', 1, ',0,1,10003,', 167, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10069, 'data_add_save', '新增商品保存', 1, ',0,1,10003,', 168, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10070, 'data_edit', '编辑商品界面', 1, ',0,1,10003,', 169, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10071, 'data_edit_save', '编辑商品保存', 1, ',0,1,10003,', 170, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10072, 'data_del', '删除商品到回收站', 1, ',0,1,10003,', 171, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10073, 'data_del_todo', '彻底删除商品', 1, ',0,1,10003,', 172, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10074, 'data_del_undo', '从回收站恢复商品', 1, ',0,1,10003,', 173, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10075, 'data_status_to_y', '批量启用商品', 1, ',0,1,10003,', 174, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10076, 'data_status_to_n', '批量禁用商品', 1, ',0,1,10003,', 175, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10077, 'data_move_byid', '批量移动选中的商品到新类别', 1, ',0,1,10003,', 176, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10078, 'data_move_byclass', '批量按类别移动商品', 1, ',0,1,10003,', 177, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10079, 'get_parentstr', '公用读取层级关系', 1, ',0,1,10003,', 179, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10080, 'set_parentstr', '公用设置层级关系', 1, ',0,1,10003,', 180, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10081, 'get_class_list', '读取类别下拉数据', 1, ',0,1,10003,', 181, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10082, 'get_brand_list', '读取品牌下拉数据', 1, ',0,1,10003,', 182, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10083, 'stkmas_popup', '打开商品弹窗', 1, ',0,1,10003,', 183, 10003, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10084, 'index', '仓库管理首页', 1, ',0,1,10004,', 186, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10085, 'whmas_list', '查看仓库列表', 1, ',0,1,10004,', 187, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10086, 'whmas_add', '新增仓库界面', 1, ',0,1,10004,', 188, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10087, 'whmas_add_save', '新增仓库保存', 1, ',0,1,10004,', 189, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10088, 'whmas_edit', '编辑仓库界面', 1, ',0,1,10004,', 190, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10089, 'whmas_edit_save', '编辑仓库保存', 1, ',0,1,10004,', 191, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10090, 'whmas_del', '删除仓库', 1, ',0,1,10004,', 192, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10091, 'whmas_status_to_y', '批量启用仓库', 1, ',0,1,10004,', 193, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10092, 'whmas_status_to_n', '批量禁用仓库', 1, ',0,1,10004,', 194, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10093, 'whmas_popup', '打开仓库弹窗', 1, ',0,1,10004,', 195, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10094, 'iotype_list', '查看出入库类型列表', 1, ',0,1,10004,', 197, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10095, 'iotype_add', '新增出入库类型界面', 1, ',0,1,10004,', 198, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10096, 'iotype_add_save', '新增出入库类型保存', 1, ',0,1,10004,', 199, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10097, 'iotype_edit', '编辑出入库类型界面', 1, ',0,1,10004,', 200, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10098, 'iotype_edit_save', '编辑出入库类型保存', 1, ',0,1,10004,', 201, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10099, 'iotype_del', '删除出入库类型', 1, ',0,1,10004,', 202, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10100, 'iotype_status_to_y', '批量启用出入库类型', 1, ',0,1,10004,', 203, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10101, 'iotype_status_to_n', '批量禁用出入库类型', 1, ',0,1,10004,', 204, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10102, 'iotype_popup', '打开出入库类型弹窗', 1, ',0,1,10004,', 205, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10103, 'stk_detail', '查看库存分布明细', 1, ',0,1,10004,', 207, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10104, 'stk_search', '查看实时库存', 1, ',0,1,10004,', 208, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10105, 'stk_track', '查看货物凭证', 1, ',0,1,10004,', 209, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10106, 'stk_qtylow', '查看库存下限报警', 1, ',0,1,10004,', 210, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10107, 'stk_qtyhigh', '查看库存上限报警', 1, ',0,1,10004,', 211, 10004, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10108, 'index', '客户管理首页', 1, ',0,1,10005,', 214, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10109, 'class_list', '查看客户分类', 1, ',0,1,10005,', 215, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10110, 'class_add', '新增客户分类界面', 1, ',0,1,10005,', 216, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10111, 'class_add_save', '新增客户分类保存', 1, ',0,1,10005,', 217, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10112, 'class_edit', '编辑客户分类界面', 1, ',0,1,10005,', 218, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10113, 'class_edit_save', '编辑客户分类保存', 1, ',0,1,10005,', 219, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10114, 'class_del', '删除客户分类', 1, ',0,1,10005,', 220, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10115, 'data_list', '查看客户列表', 1, ',0,1,10005,', 222, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10116, 'data_list_rec', '查看客户回收站', 1, ',0,1,10005,', 223, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10117, 'data_add', '新增客户界面', 1, ',0,1,10005,', 224, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10118, 'data_add_save', '新增客户保存', 1, ',0,1,10005,', 225, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10119, 'data_edit', '编辑客户界面', 1, ',0,1,10005,', 226, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10120, 'data_edit_save', '编辑客户保存', 1, ',0,1,10005,', 227, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10121, 'data_del', '删除客户到回收站', 1, ',0,1,10005,', 228, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10122, 'data_del_todo', '从回收站删除客户', 1, ',0,1,10005,', 229, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10123, 'data_del_undo', '从回收站恢复客户', 1, ',0,1,10005,', 230, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10124, 'data_status_to_y', '批量启用客户', 1, ',0,1,10005,', 231, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10125, 'data_status_to_n', '批量禁用客户', 1, ',0,1,10005,', 232, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10126, 'data_move_byid', '批量转移选中客户的类别', 1, ',0,1,10005,', 233, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10127, 'data_move_byclass', '批量按类别转移到新类别', 1, ',0,1,10005,', 234, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10128, 'get_parentstr', '公用读取层级关系', 1, ',0,1,10005,', 236, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10129, 'set_parentstr', '公用设置层级关系', 1, ',0,1,10005,', 237, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10130, 'get_class_list', '读取类别下拉数据', 1, ',0,1,10005,', 238, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10131, 'customer_popup', '打开客户弹窗', 1, ',0,1,10005,', 239, 10005, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10132, 'index', '供应商管理首页', 1, ',0,1,10006,', 242, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10133, 'class_list', '查看供应商分类', 1, ',0,1,10006,', 243, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10134, 'class_add', '新增供应商分类界面', 1, ',0,1,10006,', 244, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10135, 'class_add_save', '新增供应商分类保存', 1, ',0,1,10006,', 245, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10136, 'class_edit', '编辑供应商分类界面', 1, ',0,1,10006,', 246, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10137, 'class_edit_save', '编辑供应商分类保存', 1, ',0,1,10006,', 247, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10138, 'class_del', '删除供应商分类', 1, ',0,1,10006,', 248, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10139, 'data_list', '查看供应商列表', 1, ',0,1,10006,', 250, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10140, 'data_list_rec', '查看供应商回收站', 1, ',0,1,10006,', 251, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10141, 'data_add', '新增供应商界面', 1, ',0,1,10006,', 252, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10142, 'data_add_save', '新增供应商保存', 1, ',0,1,10006,', 253, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10143, 'data_edit', '编辑供应商界面', 1, ',0,1,10006,', 254, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10144, 'data_edit_save', '编辑供应商保存', 1, ',0,1,10006,', 255, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10145, 'data_del', '删除供应商到回收站', 1, ',0,1,10006,', 256, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10146, 'data_del_todo', '从回收站删除供应商', 1, ',0,1,10006,', 257, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10147, 'data_del_undo', '从回收站恢复供应商', 1, ',0,1,10006,', 258, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10148, 'data_status_to_y', '批量启用供应商', 1, ',0,1,10006,', 259, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10149, 'data_status_to_n', '批量禁用供应商', 1, ',0,1,10006,', 260, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10150, 'data_move_byid', '批量转移选中供应商的类别', 1, ',0,1,10006,', 261, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10151, 'data_move_byclass', '批量按类别转移到新类别', 1, ',0,1,10006,', 262, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10152, 'get_parentstr', '公用读取层级关系', 1, ',0,1,10006,', 264, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10153, 'set_parentstr', '公用设置层级关系', 1, ',0,1,10006,', 265, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10154, 'get_class_list', '读取类别下拉数据', 1, ',0,1,10006,', 266, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10155, 'supplier_popup', '打开供应商弹窗', 1, ',0,1,10006,', 267, 10006, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10156, 'index', '销售模块首页', 1, ',0,1,10007,', 270, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10157, 'so_list', '查看销售单列表', 1, ',0,1,10007,', 271, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10158, 'so_add', '新增销售单界面', 1, ',0,1,10007,', 272, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10159, 'so_add_save', '新增销售单保存', 1, ',0,1,10007,', 273, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10160, 'so_edit', '编辑销售单界面', 1, ',0,1,10007,', 274, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10161, 'so_edit_save', '编辑销售单保存', 1, ',0,1,10007,', 275, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10162, 'so_save_post', '销售单保存并过账', 1, ',0,1,10007,', 276, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10163, 'so_post', '销售单过账', 1, ',0,1,10007,', 277, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10164, 'so_write_off', '从销售单生成退货单', 1, ',0,1,10007,', 278, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10165, 'rnc_list', '查看退货单列表', 1, ',0,1,10007,', 279, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10166, 'rnc_add', '新增退货单界面', 1, ',0,1,10007,', 280, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10167, 'rnc_add_save', '新增退货单保存', 1, ',0,1,10007,', 281, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10168, 'rnc_edit', '编辑退货单界面', 1, ',0,1,10007,', 282, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10169, 'rnc_edit_save', '编辑退货单保存', 1, ',0,1,10007,', 283, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10170, 'rnc_save_post', '退货单保存并过账', 1, ',0,1,10007,', 284, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10171, 'rnc_post', '退货单过账', 1, ',0,1,10007,', 285, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10172, 'doc_check_status', '公用检查单据状态', 1, ',0,1,10007,', 287, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10173, 'doc_status_to_a', '批量恢复单据', 1, ',0,1,10007,', 288, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10174, 'doc_status_to_c', '批量取消单据', 1, ',0,1,10007,', 289, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10175, 'sales_history', '查看销售历史', 1, ',0,1,10007,', 290, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10176, 'sales_report', '销售分类汇总', 1, ',0,1,10007,', 291, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10177, 'index', '采购模块首页', 1, ',0,1,10008,', 294, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10178, 'po_list', '查看采购单列表', 1, ',0,1,10008,', 295, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10179, 'po_add', '新增采购单界面', 1, ',0,1,10008,', 296, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10180, 'po_add_save', '新增采购单保存', 1, ',0,1,10008,', 297, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10181, 'po_edit', '编辑采购单界面', 1, ',0,1,10008,', 298, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10182, 'po_edit_save', '编辑采购单保存', 1, ',0,1,10008,', 299, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10183, 'po_save_post', '采购单保存并过账', 1, ',0,1,10008,', 300, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10184, 'po_post', '采购单过账', 1, ',0,1,10008,', 301, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10185, 'po_write_off', '从采购单生成退货单', 1, ',0,1,10008,', 302, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10186, 'rns_list', '查看退货单列表', 1, ',0,1,10008,', 303, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10187, 'rns_add', '新增退货单界面', 1, ',0,1,10008,', 304, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10188, 'rns_add_save', '新增退货单保存', 1, ',0,1,10008,', 305, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10189, 'rns_edit', '编辑退货单界面', 1, ',0,1,10008,', 306, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10190, 'rns_edit_save', '编辑退货单保存', 1, ',0,1,10008,', 307, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10191, 'rns_save_post', '退货单保存并过账', 1, ',0,1,10008,', 308, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10192, 'rns_post', '退货单过账', 1, ',0,1,10008,', 309, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10193, 'doc_check_status', '公用检查单据状态', 1, ',0,1,10008,', 311, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10194, 'doc_status_to_a', '批量恢复单据', 1, ',0,1,10008,', 312, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10195, 'doc_status_to_c', '批量取消单据', 1, ',0,1,10008,', 313, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10196, 'pur_history', '查看采购历史', 1, ',0,1,10008,', 314, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10197, 'pur_report', '采购分类汇总', 1, ',0,1,10008,', 315, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10198, 'index', '其他出入库首页', 1, ',0,1,10009,', 318, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10199, 'stkin_list', '查看其他入库单列表', 1, ',0,1,10009,', 319, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10200, 'stkin_add', '新增其他入库单界面', 1, ',0,1,10009,', 320, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10201, 'stkin_add_save', '新增其他入库单保存', 1, ',0,1,10009,', 321, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10202, 'stkin_edit', '编辑其他入库单界面', 1, ',0,1,10009,', 322, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10203, 'stkin_edit_save', '编辑其他入库单保存', 1, ',0,1,10009,', 323, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10204, 'stkin_save_post', '保存并过账', 1, ',0,1,10009,', 324, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10205, 'stkin_post', '过账', 1, ',0,1,10009,', 325, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10206, 'stkout_list', '查看其他出库单列表', 1, ',0,1,10009,', 326, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10207, 'stkout_add', '新增其他出库单界面', 1, ',0,1,10009,', 327, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10208, 'stkout_add_save', '新增其他出库单保存', 1, ',0,1,10009,', 328, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10209, 'stkout_edit', '编辑其他出库单界面', 1, ',0,1,10009,', 329, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10210, 'stkout_edit_save', '编辑其他出库单保存', 1, ',0,1,10009,', 330, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10211, 'stkout_save_post', '保存并过账', 1, ',0,1,10009,', 331, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10212, 'stkout_post', '过账', 1, ',0,1,10009,', 332, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10213, 'whswitch_list', '查看仓库调拨单列表', 1, ',0,1,10009,', 333, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10214, 'whswitch_add', '新增仓库调拨单界面', 1, ',0,1,10009,', 334, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10215, 'whswitch_add_save', '新增仓库调拨单保存', 1, ',0,1,10009,', 335, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10216, 'whswitch_edit', '编辑仓库调拨单界面', 1, ',0,1,10009,', 336, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10217, 'whswitch_edit_save', '编辑仓库调拨单保存', 1, ',0,1,10009,', 337, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10218, 'whswitch_save_post', '保存并过账', 1, ',0,1,10009,', 338, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10219, 'whswitch_post', '过账', 1, ',0,1,10009,', 339, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10220, 'doc_check_status', '公用检查单据状态', 1, ',0,1,10009,', 341, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10221, 'doc_status_to_a', '批量恢复单据', 1, ',0,1,10009,', 342, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10222, 'doc_status_to_c', '批量取消单据', 1, ',0,1,10009,', 343, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10223, 'stkio_history', '查看出入库历史', 1, ',0,1,10009,', 344, 10009, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10224, 'index', '财务模块首页', 1, ',0,1,10010,', 347, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10225, 'currmas_list', '查看货币列表', 1, ',0,1,10010,', 348, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10226, 'currmas_add', '新增货币界面', 1, ',0,1,10010,', 349, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10227, 'currmas_add_save', '新增货币保存', 1, ',0,1,10010,', 350, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10228, 'currmas_edit', '编辑货币界面', 1, ',0,1,10010,', 351, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10229, 'currmas_edit_save', '编辑货币保存', 1, ',0,1,10010,', 352, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10230, 'currmas_del', '删除货币', 1, ',0,1,10010,', 353, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10231, 'currmas_status_to_y', '启用货币', 1, ',0,1,10010,', 354, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10232, 'currmas_status_to_n', '禁用货币', 1, ',0,1,10010,', 355, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10233, 'currmas_popup', '打开货币弹窗', 1, ',0,1,10010,', 356, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10234, 'fiscalyp_list', '查看会计期间列表', 1, ',0,1,10010,', 357, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10235, 'fiscalyp_add', '新增会计期间', 1, ',0,1,10010,', 358, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10236, 'fiscalyp_status_to_0', '关闭会计期间', 1, ',0,1,10010,', 359, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10237, 'fiscalyp_status_to_1', '半关会计期间', 1, ',0,1,10010,', 360, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10238, 'fiscalyp_status_to_2', '全开会计期间', 1, ',0,1,10010,', 361, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10239, 'fiscalyp_check', '检查期间状态', 1, ',0,1,10010,', 362, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10240, 'faccmas_list', '查看会计科目列表', 1, ',0,1,10010,', 364, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10241, 'faccmas_add', '新增会计科目界面', 1, ',0,1,10010,', 365, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10242, 'faccmas_add_save', '新增会计科目保存', 1, ',0,1,10010,', 366, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10243, 'faccmas_edit', '编辑会计科目界面', 1, ',0,1,10010,', 367, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10244, 'faccmas_edit_save', '编辑会计科目保存', 1, ',0,1,10010,', 368, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10245, 'faccmas_del', '删除会计科目', 1, ',0,1,10010,', 369, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10246, 'get_faccmas_parentstr', '公用读取层级关系', 1, ',0,1,10010,', 370, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10247, 'set_faccmas_parentstr', '公用设置层级关系', 1, ',0,1,10010,', 371, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10248, 'get_faccmas_list', '读取科目下拉数据', 1, ',0,1,10010,', 372, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10249, 'faccmas_popup', '打开科目弹窗', 1, ',0,1,10010,', 373, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10250, 'faccdefault_list', '查看默认科目列表', 1, ',0,1,10010,', 375, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10251, 'faccdefault_add', '新增默认科目界面', 1, ',0,1,10010,', 376, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10252, 'faccdefault_add_save', '新增默认科目保存', 1, ',0,1,10010,', 377, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10253, 'faccdefault_edit', '编辑默认科目界面', 1, ',0,1,10010,', 378, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10254, 'faccdefault_edit_save', '编辑默认科目保存', 1, ',0,1,10010,', 379, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10255, 'faccdefault_del', '删除默认会计科目', 1, ',0,1,10010,', 380, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10256, 'faccdefault_status_to_y', '启用默认科目', 1, ',0,1,10010,', 381, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10257, 'faccdefault_status_to_n', '禁用默认科目', 1, ',0,1,10010,', 382, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10258, 'vou_list', '查看凭证列表', 1, ',0,1,10010,', 384, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10259, 'vou_add', '新增凭证界面', 1, ',0,1,10010,', 385, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10260, 'vou_add_save', '新增凭证保存', 1, ',0,1,10010,', 386, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10261, 'vou_edit', '编辑凭证界面', 1, ',0,1,10010,', 387, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10262, 'vou_edit_save', '编辑凭证保存', 1, ',0,1,10010,', 388, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10263, 'vou_save_post', '保存并过账', 1, ',0,1,10010,', 389, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10264, 'vou_post', '过账', 1, ',0,1,10010,', 390, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10265, 'vou_check_status', '检查凭证状态', 1, ',0,1,10010,', 391, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10266, 'vou_status_to_a', '启用凭证', 1, ',0,1,10010,', 392, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10267, 'vou_status_to_c', '禁用凭证', 1, ',0,1,10010,', 393, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10268, 'vou_write_off', '生成红字凭证', 1, ',0,1,10010,', 394, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10269, 'vou_track', '凭证追踪', 1, ',0,1,10010,', 395, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10270, 'fa_checkin_list', '查看收款列表', 1, ',0,1,10010,', 396, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10271, 'fa_checkin_add', '新增收款界面', 1, ',0,1,10010,', 397, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10272, 'fa_checkin_add_save', '新增收款保存', 1, ',0,1,10010,', 398, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10273, 'fa_checkin_edit', '编辑收款界面', 1, ',0,1,10010,', 399, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10274, 'fa_checkin_edit_save', '编辑收款保存', 1, ',0,1,10010,', 400, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10275, 'fa_checkin_save_post', '保存并过账', 1, ',0,1,10010,', 401, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10276, 'fa_checkin_post', '过账', 1, ',0,1,10010,', 402, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10277, 'facheck_check_status', '检查单据状态', 1, ',0,1,10010,', 403, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10278, 'facheck_status_to_a', '启用单据', 1, ',0,1,10010,', 404, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10279, 'facheck_status_to_c', '禁用单据', 1, ',0,1,10010,', 405, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10280, 'fa_checkout_list', '查看付款列表', 1, ',0,1,10010,', 406, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10281, 'fa_checkout_add', '新增付款界面', 1, ',0,1,10010,', 407, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10282, 'fa_checkout_add_save', '新增付款保存', 1, ',0,1,10010,', 408, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10283, 'fa_checkout_edit', '编辑付款界面', 1, ',0,1,10010,', 409, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10284, 'fa_checkout_edit_save', '编辑付款保存', 1, ',0,1,10010,', 410, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10285, 'fa_checkout_save_post', '保存并过账', 1, ',0,1,10010,', 411, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10286, 'fa_checkout_post', '过账', 1, ',0,1,10010,', 412, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10287, 'ap_list', '应付账款列表', 1, ',0,1,10010,', 413, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10288, 'ar_list', '应收账款列表', 1, ',0,1,10010,', 414, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10289, 'trial_balance', '试算平衡', 1, ',0,1,10010,', 415, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10290, 'vou_detail', '科目明细', 1, ',0,1,10010,', 416, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10291, 'ar_statement', '客户对账单', 1, ',0,1,10010,', 417, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10292, 'ap_statement', '供应商对账单', 1, ',0,1,10010,', 418, 10010, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10293, 'so_print', '销售单打印', 1, ',0,1,10007,', 419, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10294, 'po_print', '采购单打印', 1, ',0,1,10008,', 420, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10295, 'rnc_print', '销售退货单打印', 1, ',0,1,10007,', 421, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10296, 'rns_print', '采购退货单打印', 1, ',0,1,10008,', 422, 10008, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10297, 'profit_report', '销售利润报表', 1, ',0,1,10007,', 423, 10007, 3);
INSERT INTO `misone_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES(10298, 'profit_report_export', '销售利润报表打印', 1, ',0,1,10007,', 424, 10007, 3);

-- --------------------------------------------------------

--
-- 表的结构 `misone_order`
--

DROP TABLE IF EXISTS `misone_order`;
CREATE TABLE IF NOT EXISTS `misone_order` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `total_fee` decimal(10,0) NOT NULL,
  `order_id` varchar(50) collate utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `remark` text collate utf8_unicode_ci NOT NULL,
  `status_flg` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_role`
--

DROP TABLE IF EXISTS `misone_role`;
CREATE TABLE IF NOT EXISTS `misone_role` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) default NULL,
  `status` tinyint(1) unsigned default NULL,
  `remark` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `misone_role`
--

INSERT INTO `misone_role` (`id`, `name`, `pid`, `status`, `remark`) VALUES(1, 'base-group', NULL, 1, '基础操作组');
INSERT INTO `misone_role` (`id`, `name`, `pid`, `status`, `remark`) VALUES(2, 'whman', NULL, 1, '仓管');
INSERT INTO `misone_role` (`id`, `name`, `pid`, `status`, `remark`) VALUES(3, 'pur', NULL, 1, '采购');
INSERT INTO `misone_role` (`id`, `name`, `pid`, `status`, `remark`) VALUES(4, 'sales', NULL, 1, '销售');
INSERT INTO `misone_role` (`id`, `name`, `pid`, `status`, `remark`) VALUES(5, 'faman', NULL, 1, '财务');

-- --------------------------------------------------------

--
-- 表的结构 `misone_role_user`
--

DROP TABLE IF EXISTS `misone_role_user`;
CREATE TABLE IF NOT EXISTS `misone_role_user` (
  `role_id` mediumint(9) unsigned default NULL,
  `user_id` char(32) default NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `misone_role_user`
--

INSERT INTO `misone_role_user` (`role_id`, `user_id`) VALUES(1, '8');
INSERT INTO `misone_role_user` (`role_id`, `user_id`) VALUES(1, '9');
INSERT INTO `misone_role_user` (`role_id`, `user_id`) VALUES(5, '9');
INSERT INTO `misone_role_user` (`role_id`, `user_id`) VALUES(3, '9');
INSERT INTO `misone_role_user` (`role_id`, `user_id`) VALUES(4, '9');
INSERT INTO `misone_role_user` (`role_id`, `user_id`) VALUES(2, '9');

-- --------------------------------------------------------

--
-- 表的结构 `misone_spitem`
--

DROP TABLE IF EXISTS `misone_spitem`;
CREATE TABLE IF NOT EXISTS `misone_spitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `stk_id` int(11) NOT NULL COMMENT '商品ID',
  `wh_id` int(11) NOT NULL COMMENT '仓库ID',
  `qty` decimal(18,6) NOT NULL,
  `price` decimal(18,6) NOT NULL,
  `tax_rate` decimal(5,2) NOT NULL,
  `disc_rate` decimal(5,2) NOT NULL,
  `amount1` decimal(18,6) NOT NULL,
  `amount2` decimal(18,6) NOT NULL,
  `qty_processed` decimal(18,6) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL COMMENT '状态',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `batch_no` varchar(50) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `approval_no` varchar(100) NOT NULL,
  `spec` varchar(100) NOT NULL,
  `madein` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_spmas`
--

DROP TABLE IF EXISTS `misone_spmas`;
CREATE TABLE IF NOT EXISTS `misone_spmas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `mas_code` varchar(32) NOT NULL COMMENT '单据类型，如：PO,SO,RNS,RNC',
  `mas_no` varchar(16) NOT NULL COMMENT '单号',
  `acc_id` int(10) NOT NULL COMMENT '客户或供应商',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `manager` int(11) NOT NULL,
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  `check_amount` decimal(18,6) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mas_code_no` (`mas_code`,`mas_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_stkbrand`
--

DROP TABLE IF EXISTS `misone_stkbrand`;
CREATE TABLE IF NOT EXISTS `misone_stkbrand` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_stkclass`
--

DROP TABLE IF EXISTS `misone_stkclass`;
CREATE TABLE IF NOT EXISTS `misone_stkclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_stkdetail`
--

DROP TABLE IF EXISTS `misone_stkdetail`;
CREATE TABLE IF NOT EXISTS `misone_stkdetail` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL,
  `refcode` varchar(32) NOT NULL,
  `refid` bigint(20) NOT NULL,
  `stk_id` int(11) NOT NULL,
  `wh_id` smallint(6) NOT NULL,
  `dr_qty` decimal(18,6) NOT NULL,
  `dr_amount` decimal(18,6) NOT NULL,
  `cr_qty` decimal(18,6) NOT NULL,
  `cr_amount` decimal(18,6) NOT NULL,
  `open_qty` decimal(18,6) NOT NULL,
  `open_amount` decimal(18,6) NOT NULL,
  `detail_id` bigint(20) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `approval_no` varchar(100) NOT NULL,
  `spec` varchar(100) NOT NULL,
  `madein` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_stkioitem`
--

DROP TABLE IF EXISTS `misone_stkioitem`;
CREATE TABLE IF NOT EXISTS `misone_stkioitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `stk_id` int(11) NOT NULL COMMENT '商品ID',
  `wh_id` int(11) NOT NULL COMMENT '仓库ID',
  `qty` decimal(18,6) NOT NULL,
  `price` decimal(18,6) NOT NULL,
  `amount` decimal(18,6) NOT NULL,
  `status_flg` enum('Y','N') NOT NULL COMMENT '状态',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `batch_no` varchar(50) NOT NULL,
  `expiry_date` int(11) NOT NULL,
  `approval_no` varchar(100) NOT NULL,
  `spec` varchar(100) NOT NULL,
  `madein` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_stkiomas`
--

DROP TABLE IF EXISTS `misone_stkiomas`;
CREATE TABLE IF NOT EXISTS `misone_stkiomas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `mas_code` varchar(32) NOT NULL COMMENT '单据类型，如：STKIN,STKOUT,WHSWITCH',
  `mas_no` varchar(16) NOT NULL COMMENT '单号',
  `iotype_id` int(10) NOT NULL COMMENT '出入库类型',
  `wh1_id` int(11) NOT NULL COMMENT 'WHSWITCH-仓库ID',
  `wh2_id` int(11) NOT NULL COMMENT 'WHSWITCH-仓库ID',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mas_code_no` (`mas_code`,`mas_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_stkmas`
--

DROP TABLE IF EXISTS `misone_stkmas`;
CREATE TABLE IF NOT EXISTS `misone_stkmas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `brandid` smallint(5) unsigned NOT NULL,
  `stk_c` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `uom` varchar(10) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `price_sales` decimal(18,6) NOT NULL,
  `price_pur` decimal(18,6) NOT NULL,
  `qty_max` int(10) NOT NULL,
  `qty_min` int(10) NOT NULL,
  `diyinfo` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  `spec` text NOT NULL,
  `madein` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_supplier`
--

DROP TABLE IF EXISTS `misone_supplier`;
CREATE TABLE IF NOT EXISTS `misone_supplier` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `classid` smallint(5) unsigned NOT NULL,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(80) NOT NULL,
  `stk_c` varchar(80) NOT NULL,
  `title` varchar(80) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `flag1` enum('Y','N') NOT NULL,
  `flag2` enum('Y','N') NOT NULL,
  `flag3` enum('Y','N') NOT NULL,
  `flag4` enum('Y','N') NOT NULL,
  `flag5` enum('Y','N') NOT NULL,
  `diyinfo` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext NOT NULL,
  `picurl` varchar(100) NOT NULL,
  `picarr` text NOT NULL,
  `hits` mediumint(8) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_supplierclass`
--

DROP TABLE IF EXISTS `misone_supplierclass`;
CREATE TABLE IF NOT EXISTS `misone_supplierclass` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `parentid` smallint(5) unsigned NOT NULL,
  `parentstr` varchar(50) NOT NULL,
  `classname` varchar(50) NOT NULL,
  `orderid` smallint(5) unsigned NOT NULL,
  `status_flg` enum('Y','N') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_test`
--

DROP TABLE IF EXISTS `misone_test`;
CREATE TABLE IF NOT EXISTS `misone_test` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(64) collate utf8_unicode_ci NOT NULL,
  `html` varchar(256) collate utf8_unicode_ci NOT NULL,
  `php` varchar(256) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `misone_test`
--

INSERT INTO `misone_test` (`id`, `code`, `html`, `php`) VALUES(1, 'ca712264a5e19529e990ed8bf80f2a36', '844b39518aaa52e942a5ca3ed4d0e1dc', '');
INSERT INTO `misone_test` (`id`, `code`, `html`, `php`) VALUES(2, '339ce65236253906d6d0230394e33714', '00fc63dfe4bc3b70e3a4e4a367dea04f', 'web_config');
INSERT INTO `misone_test` (`id`, `code`, `html`, `php`) VALUES(3, '6a992d5529f459a44fee58c733255e86', 'e85a18c41406846b628f588b9c20ef02', 'index');
INSERT INTO `misone_test` (`id`, `code`, `html`, `php`) VALUES(4, 'd56b699830e77ba53855679cb1d252da', 'edba100a0254451feb794e2919132264', 'login');
INSERT INTO `misone_test` (`id`, `code`, `html`, `php`) VALUES(5, '66eca1473a1b2c01db5878bd04041f73', '7af9d8fa61b4accc2714e3c696e3da26', 'menubar');

-- --------------------------------------------------------

--
-- 表的结构 `misone_tst`
--

DROP TABLE IF EXISTS `misone_tst`;
CREATE TABLE IF NOT EXISTS `misone_tst` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(512) collate utf8_unicode_ci NOT NULL,
  `content` varchar(1024) collate utf8_unicode_ci NOT NULL,
  `remark` text collate utf8_unicode_ci NOT NULL,
  `orderid` int(11) NOT NULL,
  `status_flg` enum('Y','N') collate utf8_unicode_ci NOT NULL,
  `tst1` varchar(32) collate utf8_unicode_ci NOT NULL,
  `tst2` varchar(32) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `orderid` (`orderid`),
  KEY `title` (`title`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `misone_tst`
--

INSERT INTO `misone_tst` (`id`, `title`, `content`, `remark`, `orderid`, `status_flg`, `tst1`, `tst2`) VALUES(1, 'TEST BY 事务1', 'TEST BY 事务1', 'TEST By 事务1', 3, 'Y', '', '');
INSERT INTO `misone_tst` (`id`, `title`, `content`, `remark`, `orderid`, `status_flg`, `tst1`, `tst2`) VALUES(2, 'TEST BY 事务2', 'TEST BY 事务2', 'TEST By 事务2', 3, 'Y', '', '');
INSERT INTO `misone_tst` (`id`, `title`, `content`, `remark`, `orderid`, `status_flg`, `tst1`, `tst2`) VALUES(3, '', '', '', 0, 'Y', '', 'tst2');
INSERT INTO `misone_tst` (`id`, `title`, `content`, `remark`, `orderid`, `status_flg`, `tst1`, `tst2`) VALUES(4, '', '', '', 0, 'Y', '', 'tst2');

-- --------------------------------------------------------

--
-- 表的结构 `misone_user`
--

DROP TABLE IF EXISTS `misone_user`;
CREATE TABLE IF NOT EXISTS `misone_user` (
  `id` int(20) NOT NULL auto_increment,
  `username` varchar(202) character set utf8 NOT NULL,
  `password` varchar(200) character set utf8 NOT NULL,
  `role` varchar(10) collate utf8_unicode_ci NOT NULL default 'admin',
  `logintime` int(11) NOT NULL default '0',
  `loginip` varchar(20) collate utf8_unicode_ci NOT NULL default '127.0.0.1',
  `status_flg` varchar(1) collate utf8_unicode_ci NOT NULL default 'Y',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `misone_user`
--

INSERT INTO `misone_user` (`id`, `username`, `password`, `role`, `logintime`, `loginip`, `status_flg`) VALUES(8, 'misone', '0c480cbf6ac891cdd433e2567909511f', 'admin', 1397189045, '10.1.8.38', 'Y');
INSERT INTO `misone_user` (`id`, `username`, `password`, `role`, `logintime`, `loginip`, `status_flg`) VALUES(9, 'erpdemo', 'b5fb9822006dd07da8a39e932ce39935', 'user', 1362675919, '180.159.217.138', 'Y');

-- --------------------------------------------------------

--
-- 表的结构 `misone_vouitem`
--

DROP TABLE IF EXISTS `misone_vouitem`;
CREATE TABLE IF NOT EXISTS `misone_vouitem` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `masid` bigint(20) unsigned NOT NULL,
  `acc_code` varchar(128) NOT NULL,
  `acc_title` varchar(256) NOT NULL,
  `customer_id` int(11) NOT NULL COMMENT '客户ID',
  `supplier_id` int(11) NOT NULL COMMENT '供应商ID',
  `curr_code` varchar(16) NOT NULL,
  `curr_rate` float NOT NULL,
  `curr_dr` decimal(18,6) NOT NULL,
  `dr` decimal(18,6) NOT NULL,
  `curr_cr` decimal(18,6) NOT NULL,
  `cr` decimal(18,6) NOT NULL,
  `curr_open_amount` decimal(18,6) NOT NULL,
  `open_amount` decimal(18,6) NOT NULL,
  `refid` bigint(20) unsigned NOT NULL COMMENT '对冲账时引用的ID',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_voumas`
--

DROP TABLE IF EXISTS `misone_voumas`;
CREATE TABLE IF NOT EXISTS `misone_voumas` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fadate` int(10) NOT NULL COMMENT '财务日期',
  `refcode` varchar(32) NOT NULL COMMENT '从...生成',
  `refid` bigint(20) NOT NULL COMMENT '从...生成',
  `vouno` varchar(16) NOT NULL COMMENT '凭证号',
  `remark` varchar(512) NOT NULL COMMENT '摘要',
  `createtime` int(10) NOT NULL COMMENT '创建日期',
  `creater` int(10) NOT NULL COMMENT '创建人',
  `posttime` int(10) NOT NULL COMMENT '过账日期',
  `poster` int(10) NOT NULL COMMENT '过账人',
  `status_flg` enum('A','C','E','P','N') NOT NULL COMMENT '状态',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `vouno` (`vouno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- 表的结构 `misone_webconfig`
--

DROP TABLE IF EXISTS `misone_webconfig`;
CREATE TABLE IF NOT EXISTS `misone_webconfig` (
  `varname` varchar(50) NOT NULL default '',
  `varinfo` varchar(80) NOT NULL default '',
  `vargroup` varchar(50) NOT NULL default 'basic',
  `vartype` char(10) NOT NULL default 'string',
  `varvalue` text,
  `orderid` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`varname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `misone_webconfig`
--

INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('AUTO_UPDATE_PRICE', '自动更新存货价格', 'other', 'bool', 'Y', 11);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('LIST_PAGESIZE', '列表每页显示行数', 'other', 'number', '50', 21);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('MAIL_PASSWORD', '发件人认证码', 'smtp', 'string', 'support1***', 3);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('MAIL_SMTP', 'SMTP服务器', 'smtp', 'string', 'smtp.exmail.sina.com', 1);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('MAIL_USER', '发件人', 'smtp', 'string', 'support@misone.org', 2);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('PO2WH_CTRL_FLG', '采购单直接收货', 'other', 'bool', 'Y', 1);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('PRINT_PAGESIZE', '打印每页显示行数', 'other', 'number', '5', 22);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('SO2WH_CTRL_FLG', '销售单直接出货', 'other', 'bool', 'Y', 2);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('TODAY_FA_ACC', '看板中要显示的科目(可用逗号分隔)', 'other', 'string', '1131,2121,2131,1151', 4);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('TODAY_REFRESH_TIME', '看板自动刷新时间(分)', 'other', 'string', '30', 3);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('UPLOAD_ALLOW_EXTS', '允许上传的类型', 'basic', 'string', 'jpg,gif,png,jpeg,bmp,rar,zip,7z,xls,doc,pdf,swf', 12);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('UPLOAD_ALLOW_SIZE', '允许的文件大小', 'basic', 'number', '1024000', 13);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('UPLOAD_PATH', '文件上传路径', 'basic', 'string', '/Public/Upload/', 11);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('UPLOAD_RULE', '文件重命名方式', 'basic', 'string', 'uniqid', 14);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('WEB_LOGO', '公司LOGO', 'basic', 'string', '/Public/Images/erplogo.png', 3);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('WEB_TITLE', '公司名称', 'basic', 'string', '金秘书-进销存财', 1);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('WEB_URL', '系统URL', 'basic', 'string', 'http://127.0.0.1/', 2);
INSERT INTO `misone_webconfig` (`varname`, `varinfo`, `vargroup`, `vartype`, `varvalue`, `orderid`) VALUES('WHSWITCH_USE_NEWDATE', '调拨入库的商品使用新日期', 'other', 'bool', 'Y', 5);

-- --------------------------------------------------------

--
-- 表的结构 `misone_whmas`
--

DROP TABLE IF EXISTS `misone_whmas`;
CREATE TABLE IF NOT EXISTS `misone_whmas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `diyinfo` text NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `posttime` int(10) NOT NULL,
  `status_flg` enum('Y','N','D') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `misone_whmas`
--

INSERT INTO `misone_whmas` (`id`, `title`, `flag`, `keywords`, `diyinfo`, `orderid`, `posttime`, `status_flg`) VALUES(1, '成品仓', 'default', '', 'N', 0, 1348891639, 'Y');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
